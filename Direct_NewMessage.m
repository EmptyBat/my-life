//
//  Direct_NewMessage.m
//  My Life
//
//  Created by Admin on 12.10.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "Direct_NewMessage.h"

@interface Direct_NewMessage ()
{
    UIColor*tableCellColor;
    UIColor*textColor;
    UIColor*imageColor;
    UIColor*textColor_main;
}
@end

@implementation Direct_NewMessage

- (void)viewDidLoad {
    [super viewDidLoad];
self.tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.tableView.bounds.size.width, 0.01f)];
    NSString *theme = [[NSUserDefaults standardUserDefaults] stringForKey:@"theme"];
    if ([theme isEqualToString:@"black_diamond"]){
        [self black_diamond_style];
    }
    else  if ([theme isEqualToString:@"sea_blue"]){
        [self sea_blue_style];
    }
    else  if ([theme isEqualToString:@"soft_pink"]){
        [self soft_pink_style];
    }
    else  if ([theme isEqualToString:@"green"]){
        [self green_style];
    }
    else {
        [self black_pearl_style];
    }
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(THEME:)
                                                 name:@"THEME" object:nil];
}
- (void)THEME:(NSNotification *)notice{
    NSString *theme = [[NSUserDefaults standardUserDefaults] stringForKey:@"theme"];
    if ([theme isEqualToString:@"black_diamond"]){
        [self black_diamond_style];
    }
    else  if ([theme isEqualToString:@"sea_blue"]){
        [self sea_blue_style];
    }
    else  if ([theme isEqualToString:@"soft_pink"]){
        [self soft_pink_style];
    }
    else  if ([theme isEqualToString:@"green"]){
        [self green_style];
    }
    else {
        [self black_pearl_style];
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#warning Incomplete implementation, return the number of sections
    return 6;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete implementation, return the number of rows
    return 1;
}
- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    if([view isKindOfClass:[UITableViewHeaderFooterView class]]){
        UITableViewHeaderFooterView *tableViewHeaderFooterView = (UITableViewHeaderFooterView *) view;
        tableViewHeaderFooterView.textLabel.text = [tableViewHeaderFooterView.textLabel.text capitalizedString];
    }
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    [cell setBackgroundColor:tableCellColor];
    [cell.contentView setBackgroundColor:tableCellColor];
    if (tableCellColor==nil){
        [cell setBackgroundColor:[UIColor whiteColor]];
        [cell.contentView setBackgroundColor:[UIColor whiteColor]];
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    UILabel*name=(UILabel*) [cell viewWithTag:1];
    UILabel*text=(UILabel*) [cell viewWithTag:3];
    name.textColor=textColor_main;
    UIImage *check_image = [[UIImage imageNamed:@"accessory_icon.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    
    UIImageView *checkmark = [[UIImageView alloc] initWithImage:check_image];
    checkmark.tintColor = textColor;
    cell.accessoryView = checkmark;
    NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:@"Хочешь расширить свой бизнес..6ч"];
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:170.0/255 green:170.0/255 blue:170.0/255 alpha:1.0] range:NSMakeRange(0,30)];
    [string addAttribute:NSForegroundColorAttributeName value:textColor range:NSMakeRange(30,2)];
    text.attributedText=string;

    
    return cell;
    
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 67;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section==0){
    NSString *sectionName=@"Это сообщения от людей, на обновления которых вы не подписаны. Они узнают о том, что вы посмотрели их запросы, только в этом случае, если вы выберите 'Разрешить'";
    return sectionName;
    }
    else {
        return nil;
    }
}
-(void)black_pearl_style{
    [_title_btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    tableCellColor=[UIColor whiteColor];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleDefault;
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1.0];;
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor blackColor]};
    self.tableView.backgroundColor =[UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1.0];
    UIImage *image = [[UIImage imageNamed:@"YellowBack"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.back setImage:image forState:UIControlStateNormal];
    self.back.tintColor = [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    self.add.tintColor = [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    self.tabBarController.tabBar.barTintColor = [UIColor whiteColor];
    self.tabBarController.tabBar.tintColor = [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    imageColor= [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    textColor = [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    textColor_main=[UIColor blackColor];
    [self.tableView reloadData];
}
-(void)black_diamond_style{
        [_title_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleBlackOpaque;
    tableCellColor=[UIColor blackColor];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor =[UIColor blackColor];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    self.tableView.backgroundColor =[UIColor colorWithRed:23.0/255 green:23.0/255 blue:23.0/255 alpha:1.0];
    UIImage *image = [[UIImage imageNamed:@"YellowBack"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.back setImage:image forState:UIControlStateNormal];
    self.back.tintColor = [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    self.add.tintColor = [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];    self.tabBarController.tabBar.barTintColor = [UIColor blackColor];
    self.tabBarController.tabBar.tintColor = [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    imageColor=  [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    textColor = [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    textColor_main=[UIColor whiteColor];
    [self.tableView reloadData];
}
-(void)sea_blue_style{
    [_title_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:2.0/255 green:35.0/255 blue:143.0/255 alpha:1.0];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    self.tableView.backgroundColor =[UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1.0];
    UIImage *image = [[UIImage imageNamed:@"YellowBack"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.back setImage:image forState:UIControlStateNormal];
    self.back.tintColor = [UIColor whiteColor];
    self.add.tintColor =[UIColor whiteColor];
    self.tabBarController.tabBar.barTintColor =[UIColor whiteColor];
    self.tabBarController.tabBar.tintColor = [UIColor colorWithRed:2.0/255 green:35.0/255 blue:143.0/255 alpha:1.0];
    imageColor= [UIColor colorWithRed:2.0/255 green:35.0/255 blue:143.0/255 alpha:1.0];
    textColor = [UIColor colorWithRed:2.0/255 green:35.0/255 blue:143.0/255 alpha:1.0];
    textColor_main=[UIColor blackColor];
    [self.tableView reloadData];
}
-(void)soft_pink_style{
    [_title_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:231.0/255 green:194.0/255 blue:202.0/255 alpha:1.0];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    self.tableView.backgroundColor =[UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1.0];
    UIImage *image = [[UIImage imageNamed:@"YellowBack"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.back setImage:image forState:UIControlStateNormal];
    self.back.tintColor = [UIColor whiteColor];
    self.add.tintColor = [UIColor whiteColor];
    tableCellColor=[UIColor whiteColor];
    self.tabBarController.tabBar.barTintColor =[UIColor whiteColor];
    self.tabBarController.tabBar.tintColor =[UIColor colorWithRed:231.0/255 green:194.0/255 blue:202.0/255 alpha:1.0];
    imageColor= [UIColor colorWithRed:231.0/255 green:194.0/255 blue:202.0/255 alpha:1.0];
    textColor = [UIColor colorWithRed:231.0/255 green:194.0/255 blue:202.0/255 alpha:1.0];
    textColor_main=[UIColor blackColor];
    [self.tableView reloadData];
}
-(void)green_style{
    [_title_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:23.0/255 green:125.0/255 blue:46.0/255 alpha:1.0];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    self.tableView.backgroundColor =[UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1.0];
    UIImage *image = [[UIImage imageNamed:@"YellowBack"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.back setImage:image forState:UIControlStateNormal];
    self.back.tintColor = [UIColor whiteColor];
    self.add.tintColor = [UIColor whiteColor];
    tableCellColor=[UIColor whiteColor];
    self.tabBarController.tabBar.barTintColor =[UIColor whiteColor];
    self.tabBarController.tabBar.tintColor = [UIColor colorWithRed:23.0/255 green:125.0/255 blue:46.0/255 alpha:1.0];
    imageColor= [UIColor colorWithRed:23.0/255 green:125.0/255 blue:46.0/255 alpha:1.0];
    textColor = [UIColor colorWithRed:23.0/255 green:125.0/255 blue:46.0/255 alpha:1.0];
    textColor_main=[UIColor blackColor];
    [self.tableView reloadData];
}

- (IBAction)back:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];

}
@end
