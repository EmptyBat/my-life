//
//  Accept_subscribers.h
//  My Life
//
//  Created by Admin on 28.09.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TextFieldCustom.h"
@interface Accept_subscribers : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)back:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *title_btn;
@property (strong, nonatomic) IBOutlet UIButton *back;
@property (strong, nonatomic) IBOutlet UIButton *search;
@property (strong, nonatomic) IBOutlet UIView *search_view;
@property (strong, nonatomic) IBOutlet TextFieldCustom *search_field;

@end
