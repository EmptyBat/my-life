//
//  editProfiles.m
//  My Life
//
//  Created by Admin on 05.10.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "editProfiles.h"
#import "ActionSheetStringPicker.h"
@interface editProfiles ()
{
    UIColor*tableCellColor;
    UIColor*textColor;
    UIColor*imageColor;
    UIColor*textColor_main;
}
@end

@implementation editProfiles

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.tableView.bounds.size.width, 10.01f)];
    NSString *theme = [[NSUserDefaults standardUserDefaults] stringForKey:@"theme"];
    if ([theme isEqualToString:@"black_diamond"]){
        [self black_diamond_style];
    }
    else  if ([theme isEqualToString:@"sea_blue"]){
        [self sea_blue_style];
    }
    else  if ([theme isEqualToString:@"soft_pink"]){
        [self soft_pink_style];
    }
    else  if ([theme isEqualToString:@"green"]){
        [self green_style];
    }
    else {
        [self black_pearl_style];
    }
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(THEME:)
                                                 name:@"THEME" object:nil];
    
}
- (void)THEME:(NSNotification *)notice{
    NSString *theme = [[NSUserDefaults standardUserDefaults] stringForKey:@"theme"];
    if ([theme isEqualToString:@"black_diamond"]){
        [self black_diamond_style];
    }
    else  if ([theme isEqualToString:@"sea_blue"]){
        [self sea_blue_style];
    }
    else  if ([theme isEqualToString:@"soft_pink"]){
        [self soft_pink_style];
    }
    else  if ([theme isEqualToString:@"green"]){
        [self green_style];
    }
    else {
        [self black_pearl_style];
    }
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    [cell setBackgroundColor:tableCellColor];
    [cell.contentView setBackgroundColor:tableCellColor];
    if (tableCellColor==nil){
        [cell setBackgroundColor:[UIColor whiteColor]];
        [cell.contentView setBackgroundColor:[UIColor whiteColor]];
    }
}
-(void)black_pearl_style{
    tableCellColor=[UIColor whiteColor];
    textColor = [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    textColor_main=[UIColor blackColor];
    
    tableCellColor=[UIColor whiteColor];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleDefault;
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1.0];;
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor blackColor]};
    self.tableView.backgroundColor =[UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1.0];
    [_title_btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    UIImage *image = [[UIImage imageNamed:@"YellowBack"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.back setImage:image forState:UIControlStateNormal];
    self.back.tintColor = textColor;
    [self.done setTitleColor:textColor forState:UIControlStateNormal];
    UIImage *image1 = [[UIImage imageNamed:@"Compose"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.compose.image=image1;
    self.compose.tintColor = textColor;
    self.websait_compose.image=image1;
    self.websait_compose.tintColor = textColor;
    self.about_me_compose.image=image1;
    self.about_me_compose.tintColor = textColor;
    UIImage *image2 = [[UIImage imageNamed:@"accept"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.accept.image=image2;
    self.accept.tintColor = textColor;
    self.name.textColor=textColor_main;
    [self.name setValue:textColor_main forKeyPath:@"_placeholderLabel.textColor"];
    self.nickname.textColor=textColor_main;
    [self.nickname setValue:textColor_main forKeyPath:@"_placeholderLabel.textColor"];
    self.websait.textColor=textColor_main;
    [self.websait setValue:textColor_main forKeyPath:@"_placeholderLabel.textColor"];
    self.about_me.textColor=textColor_main;
    [self.about_me setValue:textColor_main forKeyPath:@"_placeholderLabel.textColor"];
    self.email.textColor=textColor_main;
    self.phone.textColor=textColor_main;
    self.sex.textColor=textColor_main;
    [self.tableView reloadData];
}
-(void)black_diamond_style{
    tableCellColor=[UIColor blackColor];
    textColor = [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    textColor_main=[UIColor whiteColor];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleBlackOpaque;
    tableCellColor=[UIColor blackColor];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor =[UIColor blackColor];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    self.tableView.backgroundColor =[UIColor colorWithRed:23.0/255 green:23.0/255 blue:23.0/255 alpha:1.0];
    self.tabBarController.tabBar.barTintColor = [UIColor blackColor];
    self.tabBarController.tabBar.tintColor = [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    [_title_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    UIImage *image = [[UIImage imageNamed:@"YellowBack"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.back setImage:image forState:UIControlStateNormal];
    self.back.tintColor = textColor;
    [self.done setTitleColor:textColor forState:UIControlStateNormal];
    UIImage *image1 = [[UIImage imageNamed:@"Compose"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.compose.image=image1;
    self.compose.tintColor = textColor;
    self.websait_compose.image=image1;
    self.websait_compose.tintColor = textColor;
    self.about_me_compose.image=image1;
    self.about_me_compose.tintColor = textColor;
    UIImage *image2 = [[UIImage imageNamed:@"accept"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.accept.image=image2;
    self.accept.tintColor = textColor;
    self.name.textColor=textColor_main;
    [self.name setValue:textColor_main forKeyPath:@"_placeholderLabel.textColor"];
    self.nickname.textColor=textColor_main;
    [self.nickname setValue:textColor_main forKeyPath:@"_placeholderLabel.textColor"];
    self.websait.textColor=textColor_main;
    [self.websait setValue:textColor_main forKeyPath:@"_placeholderLabel.textColor"];
    self.about_me.textColor=textColor_main;
    [self.about_me setValue:textColor_main forKeyPath:@"_placeholderLabel.textColor"];
    self.email.textColor=textColor_main;
    self.phone.textColor=textColor_main;
    self.sex.textColor=textColor_main;
    [self.tableView reloadData];
    
}
-(void)sea_blue_style{
    tableCellColor=[UIColor whiteColor];
    textColor = [UIColor colorWithRed:2.0/255 green:35.0/255 blue:143.0/255 alpha:1.0];
    textColor_main=[UIColor blackColor];
    
    tableCellColor=[UIColor whiteColor];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:2.0/255 green:35.0/255 blue:143.0/255 alpha:1.0];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    self.tableView.backgroundColor =[UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1.0];
    tableCellColor=[UIColor whiteColor];
    self.tabBarController.tabBar.barTintColor =[UIColor whiteColor];
    self.tabBarController.tabBar.tintColor = [UIColor colorWithRed:2.0/255 green:35.0/255 blue:143.0/255 alpha:1.0];
    [_title_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    UIImage *image = [[UIImage imageNamed:@"YellowBack"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.back setImage:image forState:UIControlStateNormal];
    self.back.tintColor = tableCellColor;
    [self.done setTitleColor:tableCellColor forState:UIControlStateNormal];
    UIImage *image1 = [[UIImage imageNamed:@"Compose"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.compose.image=image1;
    self.compose.tintColor = textColor;
    self.websait_compose.image=image1;
    self.websait_compose.tintColor = textColor;
    self.about_me_compose.image=image1;
    self.about_me_compose.tintColor = textColor;
    UIImage *image2 = [[UIImage imageNamed:@"accept"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.accept.image=image2;
    self.accept.tintColor = textColor;
    self.name.textColor=textColor_main;
    [self.name setValue:textColor_main forKeyPath:@"_placeholderLabel.textColor"];
    self.nickname.textColor=textColor_main;
    [self.nickname setValue:textColor_main forKeyPath:@"_placeholderLabel.textColor"];
    self.websait.textColor=textColor_main;
    [self.websait setValue:textColor_main forKeyPath:@"_placeholderLabel.textColor"];
    self.about_me.textColor=textColor_main;
    [self.about_me setValue:textColor_main forKeyPath:@"_placeholderLabel.textColor"];
    self.email.textColor=textColor_main;
    self.phone.textColor=textColor_main;
    self.sex.textColor=textColor_main;
    [self.tableView reloadData];
}
-(void)soft_pink_style{
    tableCellColor=[UIColor whiteColor];
    textColor = [UIColor colorWithRed:231.0/255 green:194.0/255 blue:202.0/255 alpha:1.0];
    textColor_main=[UIColor blackColor];
    
    tableCellColor=[UIColor whiteColor];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:231.0/255 green:194.0/255 blue:202.0/255 alpha:1.0];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    self.tableView.backgroundColor =[UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1.0];
    tableCellColor=[UIColor whiteColor];
    self.tabBarController.tabBar.barTintColor =[UIColor whiteColor];
    self.tabBarController.tabBar.tintColor =[UIColor colorWithRed:231.0/255 green:194.0/255 blue:202.0/255 alpha:1.0];
    [_title_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    UIImage *image = [[UIImage imageNamed:@"YellowBack"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.back setImage:image forState:UIControlStateNormal];
    self.back.tintColor = tableCellColor;
    [self.done setTitleColor:tableCellColor forState:UIControlStateNormal];
    UIImage *image1 = [[UIImage imageNamed:@"Compose"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.compose.image=image1;
    self.compose.tintColor = textColor;
    self.websait_compose.image=image1;
    self.websait_compose.tintColor = textColor;
    self.about_me_compose.image=image1;
    self.about_me_compose.tintColor = textColor;
    UIImage *image2 = [[UIImage imageNamed:@"accept"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.accept.image=image2;
    self.accept.tintColor = textColor;
    self.name.textColor=textColor_main;
    [self.name setValue:textColor_main forKeyPath:@"_placeholderLabel.textColor"];
    self.nickname.textColor=textColor_main;
    [self.nickname setValue:textColor_main forKeyPath:@"_placeholderLabel.textColor"];
    self.websait.textColor=textColor_main;
    [self.websait setValue:textColor_main forKeyPath:@"_placeholderLabel.textColor"];
    self.about_me.textColor=textColor_main;
    [self.about_me setValue:textColor_main forKeyPath:@"_placeholderLabel.textColor"];
    self.email.textColor=textColor_main;
    self.phone.textColor=textColor_main;
    self.sex.textColor=textColor_main;
    [self.tableView reloadData];
    
}
-(void)green_style{
    tableCellColor=[UIColor whiteColor];
    textColor = [UIColor colorWithRed:23.0/255 green:125.0/255 blue:46.0/255 alpha:1.0];
    textColor_main=[UIColor blackColor];
    
    tableCellColor=[UIColor whiteColor];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:23.0/255 green:125.0/255 blue:46.0/255 alpha:1.0];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    self.tableView.backgroundColor =[UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1.0];
    tableCellColor=[UIColor whiteColor];
    self.tabBarController.tabBar.barTintColor =[UIColor whiteColor];
    self.tabBarController.tabBar.tintColor = [UIColor colorWithRed:23.0/255 green:125.0/255 blue:46.0/255 alpha:1.0];
    [_title_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    UIImage *image = [[UIImage imageNamed:@"YellowBack"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.back setImage:image forState:UIControlStateNormal];
    self.back.tintColor = tableCellColor;
    [self.done setTitleColor:tableCellColor forState:UIControlStateNormal];
    UIImage *image1 = [[UIImage imageNamed:@"Compose"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.compose.image=image1;
    self.compose.tintColor = textColor;
    self.websait_compose.image=image1;
    self.websait_compose.tintColor = textColor;
    self.about_me_compose.image=image1;
    self.about_me_compose.tintColor = textColor;
    UIImage *image2 = [[UIImage imageNamed:@"accept"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.accept.image=image2;
    self.accept.tintColor = textColor;
    self.name.textColor=textColor_main;
    [self.name setValue:textColor_main forKeyPath:@"_placeholderLabel.textColor"];
    self.nickname.textColor=textColor_main;
    [self.nickname setValue:textColor_main forKeyPath:@"_placeholderLabel.textColor"];
    self.websait.textColor=textColor_main;
    [self.websait setValue:textColor_main forKeyPath:@"_placeholderLabel.textColor"];
    self.about_me.textColor=textColor_main;
    [self.about_me setValue:textColor_main forKeyPath:@"_placeholderLabel.textColor"];
    self.email.textColor=textColor_main;
    self.phone.textColor=textColor_main;
    self.sex.textColor=textColor_main;
    [self.tableView reloadData];
}

- (IBAction)back:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)done:(id)sender {
}
- (IBAction)sex:(id)sender {
    ActionStringDoneBlock done = ^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        NSLog(@"Block picker sucess: selectedValue = %@", selectedValue);
        if ([sender respondsToSelector:@selector(setText:)]) {
            [sender performSelector:@selector(setText:) withObject:selectedValue];
        }
    };
    ActionStringCancelBlock cancel = ^(ActionSheetStringPicker *picker)
    {
        
        NSLog(@"Block Picker Canceled");
    };
    NSArray *colors = @[@"Мужской", @"Женский", @"Не указанно"];
    ActionSheetStringPicker* picker = [[ActionSheetStringPicker alloc] initWithTitle:nil rows:colors initialSelection:0 doneBlock:done cancelBlock:cancel origin:sender];
    picker.tapDismissAction = TapActionCancel;
    [picker showActionSheetPicker];
}
@end
