//
//  Search_Bests.h
//  My Life
//
//  Created by Admin on 16.10.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Search_Bests : UIViewController
@property (strong, nonatomic) IBOutlet UISearchBar *search;
@property (strong, nonatomic) IBOutlet UIView *panel;
@property (strong, nonatomic) IBOutlet UITableView *tableVIew;

@end
