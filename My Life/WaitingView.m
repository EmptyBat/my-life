//
//  WaitingView.m
//  Rolyal Car Wash
//
//  Created by Almas on 11.09.15.
//  Copyright (c) 2015 acelight. All rights reserved.
//

#import "WaitingView.h"

@implementation WaitingView  {
    UIView *trBlockView;
}
- (void)addWaitinViewToView:(UIView *)view {
    [self addBlockViewToView:view];
    if ( ![SVProgressHUD isVisible] )[SVProgressHUD show];
}
- (void) removeWaitActivity {
    [self removeBlock];
    if ( [SVProgressHUD isVisible] ) [SVProgressHUD dismiss];
}

- (void) addBlockViewToView:(UIView*)view {
    if ( trBlockView == nil ) trBlockView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, view.frame.size.width, view.frame.size.height)];
    [trBlockView setBackgroundColor:[UIColor colorWithWhite:0 alpha:.1]];
    [view addSubview:trBlockView];
}
- (void) removeBlock {
    [trBlockView removeFromSuperview];
}
@end
