//
//  TextFieldCustom.m
//  Baibazar
//
//  Created by bugingroup on 27.04.16.
//  Copyright © 2016 Almas. All rights reserved.
//

#import "TextFieldCustom.h"

@implementation TextFieldCustom

- (CGRect)textRectForBounds:(CGRect)bounds {
    return [self rectForBounds:bounds];
}

- (CGRect)editingRectForBounds:(CGRect)bounds {
    return [self rectForBounds:bounds];
}

- (CGRect)placeholderRectForBounds:(CGRect)bounds {
    return [self rectForBounds:bounds];
}

//here 40 - is your x offset
- (CGRect)rectForBounds:(CGRect)bounds {
    
    return CGRectInset(bounds, 14, 3);
    
}

@end
