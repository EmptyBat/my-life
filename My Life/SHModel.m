//
//  SHModel.m
//  My Life
//
//  Created by Admin on 05.12.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "SHModel.h"
#import "Constant.h"
@implementation SHModel
// Authorization
- (void) LoginRequest:(NSString*)email pwd:(NSString*)pwd type:(NSString*)type completion:(void (^)(BOOL success, NSString *error,NSString*expires,NSString*token))completionBlock{
    NSString*params=[NSString stringWithFormat:@"{'type':'%@','login' : '%@','password':'%@'}",type,email,pwd];
    NSLog(@"%@",params);
    NSString*urls=[NSString stringWithFormat:@"%@reg-server/services/login/auth",mainURL];
    
    NSURL* url = [NSURL URLWithString:urls];
    
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"POST";
    request.HTTPBody = [params dataUsingEncoding:NSUTF8StringEncoding];
    
    NSURLResponse *responce;
    NSData *GETReply = [NSURLConnection sendSynchronousRequest:request returningResponse:&responce error:nil];
    NSString *theReply = [[NSString alloc] initWithBytes:[GETReply bytes] length:[GETReply length] encoding:NSASCIIStringEncoding];
    NSError* error;
    NSLog(@"%@",theReply);
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:GETReply
                                                         options:kNilOptions
                                                           error:&error];
    NSNumber * isSuccessNumber = (NSNumber *)[json objectForKey: @"success"];
    NSString *expires = [json objectForKey:@"expires"];
    NSString *token = [json objectForKey:@"token"];
     if([isSuccessNumber boolValue] == YES){
         completionBlock(YES,nil,expires,token);
     }
     else {
              completionBlock(NO,nil,nil,nil);
     }
    /*
    NSDictionary *parameters = @{@"type": type,@"login":email,@"password":pwd};
   AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSString*url=[NSString stringWithFormat:@"%@reg-server/services/login/auth",mainURL];
    NSLog(@"%@",parameters);
    [manager POST:url parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSNumber * isSuccessNumber = (NSNumber *)[responseObject objectForKey: @"success"];
        
        if([isSuccessNumber boolValue] == YES){
        NSLog(@"**++%@",responseObject);
        
         NSString*token= [responseObject objectForKey:@"token"];
          NSString*expires= [responseObject objectForKey:@"expires"];
            completionBlock(YES,nil,expires,token);
        }
        else {
         NSLog(@"%@",responseObject);
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
          completionBlock(NO,nil,nil,nil);
        
    }];*/
}
- (void) registerWithEmail:(NSString*)email  completion:(void  (^)(BOOL success, NSString *error))completionBlock{

    NSDictionary *parameters = @{ @"email":email};
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSString*url=[NSString stringWithFormat:@"%@reg-server/services/eMailRegistration",mainURL];
    [manager POST:url parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSLog(@"**++%@",responseObject);
        
        completionBlock(YES,nil);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        
    }];
}
- (void) registerWithPhone:(NSString*)phone  completion:(void  (^)(BOOL success, NSString *error))completionBlock
{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSString*url=[NSString stringWithFormat:@"%@reg-server/services/phoneRegistration/%@",mainURL,phone];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        //NSLog(@"**++%@",responseObject);
            completionBlock(YES,nil);
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            NSLog(@"Error: %@", error);
            
        }];
}
- (void) frendsByPhoneList:(NSArray*)numbers completion:(void (^)(BOOL success, NSString *error))completionBlock{

    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSString*url=[NSString stringWithFormat:@"%@reg-server/services/frendsByPhoneList",mainURL];
    [manager POST:url parameters:numbers progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        //NSLog(@"**++%@",responseObject);
        completionBlock(YES,nil);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        
    }];
}
<<<<<<< HEAD
- (void) refreshToken:(NSString*)token completion:(void (^)(BOOL success, NSString *error,NSString*expires,NSString*token))completionBlock{
  NSLog(@"**++%@",token);
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSString*url=[NSString stringWithFormat:@"%@reg-server/services/login/refresh",mainURL];
      [manager.requestSerializer setValue:token forHTTPHeaderField:@"auth"];
    [manager POST:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSLog(@"**++%@",responseObject);
        NSNumber * isSuccessNumber = (NSNumber *)[responseObject objectForKey: @"success"];
        NSString *expires = [responseObject objectForKey:@"expires"];
        NSString *token = [responseObject objectForKey:@"token"];
          if([isSuccessNumber boolValue] == YES){
        completionBlock(YES,nil,expires,token);
          }
=======
- (void) refreshToken:(NSString*)token completion:(void (^)(BOOL success, NSString *error))completionBlock{
    NSDictionary *parameters = @{ @"auth":token};
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSString*url=[NSString stringWithFormat:@"%@reg-server/services/refresh",mainURL];
    [manager POST:url parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        //NSLog(@"**++%@",responseObject);
        
        completionBlock(YES,nil);
>>>>>>> 0480d4163c366406f7e51190add26214ae62c3b4
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        
    }];

    
    
}
- (void) resetPasswordWithEmail:(NSString*)email completion:(void (^)(BOOL success, NSString *message, NSString *error))completionBlock{

}
- (void) logoutWithCompletion:(void (^)(BOOL success, NSString *error))completionBlock{

}
- (void) checkEmail:(NSString*)email completion:(void (^)(BOOL success, NSString *error))completionBlock{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSString*url=[NSString stringWithFormat:@"%@reg-server/services/eMailRegistration/%@ ",mainURL,email];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        //NSLog(@"**++%@",responseObject);
        
        completionBlock(YES,nil);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        
    }];
}
- (void) checkPhone:(NSString*)phone completion:(void (^)(BOOL success, NSString *error))completionBlock{

    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSString*url=[NSString stringWithFormat:@"%@reg-server/services/phoneRegistration/%@",mainURL,phone];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        //NSLog(@"**++%@",responseObject);
        
        completionBlock(YES,nil);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        
    }];
}
- (void) authFB:(NSString*)token completion:(void (^)(BOOL success, NSString *error,NSString*expires,NSString*token))completionBlock{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSString*url=[NSString stringWithFormat:@"%@reg-server/services/login/authFB?accessToken=%@",mainURL,token];
    NSLog(@"%@",url);
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSNumber * isSuccessNumber = (NSNumber *)[responseObject objectForKey: @"success"];
        NSString *expires = [responseObject objectForKey:@"expires"];
        NSString *token = [responseObject objectForKey:@"token"];
        if([isSuccessNumber boolValue] == YES){
            completionBlock(YES,nil,expires,token);
        }
        else {
            completionBlock(NO,nil,nil,nil);
        }

     NSLog(@"**++%@",responseObject);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
<<<<<<< HEAD
    }];
}
- (void) authVK:(NSString*)token completion:(void (^)(BOOL success, NSString *error))completionBlock{

}
- (void) registrationDataPhone:(NSString*)phone password:(NSString*)password surname:(NSString*)surname name:(NSString*)name completion:(void (^)(BOOL success, NSString *error))completionBlock{
    
<<<<<<< HEAD
  NSString*urls=[NSString stringWithFormat:@"%@reg-server/services/registrationData",mainURL];
    NSString*params=[NSString stringWithFormat:@"{'phone':'%@','password' : '%@','surname':'%@','name':'%@','email':''}",phone,password,surname,name];
    NSLog(@"%@",params);
    
    NSURL* url = [NSURL URLWithString:urls];
    
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"POST";
    request.HTTPBody = [params dataUsingEncoding:NSUTF8StringEncoding];
    
    NSURLResponse *responce;
    NSData *GETReply = [NSURLConnection sendSynchronousRequest:request returningResponse:&responce error:nil];
    NSString *theReply = [[NSString alloc] initWithBytes:[GETReply bytes] length:[GETReply length] encoding:NSASCIIStringEncoding];
    NSError* error;
    NSLog(@"%@",theReply);
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:GETReply
                                                         options:kNilOptions
                                                           error:&error];
    NSNumber * isSuccessNumber = (NSNumber *)[json objectForKey: @"success"];
=======
    NSDictionary *parameters = @{ @"phone":phone,@"password":password,@"surname":surname,@"name":name};
    NSLog(@"4455 +++++   %@",parameters);
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSString*url=[NSString stringWithFormat:@"%@reg-server/services/registrationData",mainURL];
    [manager POST:url parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSLog(@"**++%@",responseObject);
        NSNumber * isSuccessNumber = (NSNumber *)[responseObject objectForKey: @"success"];
        if([isSuccessNumber boolValue] == YES){
            completionBlock(YES,nil);
        }
        else {
            completionBlock(NO,nil);
        }
        
>>>>>>> parent of f7c153f... AWS integration &&More requests&&UI updated.


    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        
    }];
}
- (void) registrationDataEmail:(NSString*)email password:(NSString*)password surname:(NSString*)surname name:(NSString*)name completion:(void (^)(BOOL success, NSString *error))completionBlock{
    
    NSString*urls=[NSString stringWithFormat:@"%@reg-server/services/registrationData",mainURL];
    NSString*params=[NSString stringWithFormat:@"{'email':'%@','password' : '%@','surname':'%@','name':'%@','phone':''}",email,password,surname,name];
    NSLog(@"%@",params);
    
    NSURL* url = [NSURL URLWithString:urls];
    
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"POST";
    request.HTTPBody = [params dataUsingEncoding:NSUTF8StringEncoding];
    
    NSURLResponse *responce;
    NSData *GETReply = [NSURLConnection sendSynchronousRequest:request returningResponse:&responce error:nil];
    NSString *theReply = [[NSString alloc] initWithBytes:[GETReply bytes] length:[GETReply length] encoding:NSASCIIStringEncoding];
    NSError* error;
    NSLog(@"%@",theReply);
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:GETReply
                                                         options:kNilOptions
                                                           error:&error];
    NSNumber * isSuccessNumber = (NSNumber *)[json objectForKey: @"success"];
    
    if([isSuccessNumber boolValue] == YES){
        completionBlock(YES,nil);
    }
    else {
        completionBlock(NO,nil);
    }
}

- (void) AwsToken:(NSString*)token contentHashString:(NSString*)contentHashString contentLength:(NSString*)contentLength  completion:(void (^)(BOOL success, NSString *error))completionBlock{

    NSDictionary *parameters = @{ @"contentHashString":contentHashString,@"contentLength":contentLength};
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
      [manager.requestSerializer setValue:token forHTTPHeaderField:@"auth"];
    NSString*url=[NSString stringWithFormat:@"%@reg-server/services/AwsToken",mainURL];
    [manager POST:url parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSLog(@"**++%@",responseObject);
        NSNumber * isSuccessNumber = (NSNumber *)[responseObject objectForKey: @"success"];
        if([isSuccessNumber boolValue] == YES){
            completionBlock(YES,nil);
        }
        else {
            completionBlock(NO,nil);
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        
   }];
}
- (void) UpdateAvatar:(NSString*)resourceId  token:(NSString*)token  completion:(void (^)(BOOL success, NSString *error))completionBlock{
    NSDictionary *parameters = @{ @"resourceId":resourceId};
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:token forHTTPHeaderField:@"auth"];
    NSString*url=[NSString stringWithFormat:@"%@reg-server/services/registrationData",mainURL];
    [manager POST:url parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSLog(@"**++%@",responseObject);
        NSNumber * isSuccessNumber = (NSNumber *)[responseObject objectForKey: @"success"];
        if([isSuccessNumber boolValue] == YES){
            completionBlock(YES,nil);
        }
        else {
            completionBlock(NO,nil);
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        
    }];

}

// UserData
- (void) addFollower:(NSString*)token   favoriteId:(NSString*)favoriteId completion:(void (^)(BOOL success, NSString *error))completionBlock
{
    NSDictionary *parameters = @{@"favoriteId":favoriteId};
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSString*url=[NSString stringWithFormat:@"%@reg-server/services/node/unfollowing",mainURL];
    //  manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/plain"];
=======
        
    }];
}
- (void) authVK:(NSString*)token completion:(void (^)(BOOL success, NSString *error))completionBlock{

}

// UserData
- (void) addFollower:(NSString*)token completion:(void (^)(BOOL success, NSString *error))completionBlock
{

}
- (void) addPostComment:(NSString*)userId postId:(NSString*)postId comment:(NSString*)comment completion:(void (^)(BOOL success, NSString *error))completionBlock{
    NSDictionary *parameters = @{ @"userId":userId,@"postId":postId,@"comment":comment,};
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSString*url=[NSString stringWithFormat:@"%@reg-server/services/refresh",mainURL];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/plain"];
>>>>>>> 0480d4163c366406f7e51190add26214ae62c3b4
    [manager POST:url parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        //NSLog(@"**++%@",responseObject);
        
        completionBlock(YES,nil);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        
    }];
}
- (void) getTapePosts:(NSString*)token completion:(void (^)(BOOL success, NSString *error,id objects))completionBlock{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
<<<<<<< HEAD
   manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSLog(@"%@",token);
   [manager.requestSerializer setValue:token forHTTPHeaderField:@"auth"];
    NSString*url=[NSString stringWithFormat:@"%@reg-server/services/node/getTapePosts?limit=10&start=0",mainURL];
    NSLog(@"+*-%@",url);
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSArray* latestLoans = [responseObject objectForKey:@"body"][@"list"];
        NSLog(@"++**%@",responseObject);
=======
   manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/plain"];
    NSLog(@"%@",token);
   [manager.requestSerializer setValue:token forHTTPHeaderField:@"auth"];
    NSString*url=[NSString stringWithFormat:@"%@reg-server/services/node/getTapePosts?limit=0&start=0",mainURL];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSArray* latestLoans = [responseObject objectForKey:@"body"];
        NSLog(@"++**%@",latestLoans);
>>>>>>> 0480d4163c366406f7e51190add26214ae62c3b4
        completionBlock(YES,nil,latestLoans);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        
    }];
}
<<<<<<< HEAD
- (void) findContens:(NSString*)longitude latitude:(NSString*)latitude hashtags:(NSString*)hashtags completion:(void (^)(BOOL success, NSString *error))completionBlock{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSString*url=[NSString stringWithFormat:@"%@reg-server/services/node/findContens?longitude=121.15644&latitude=45.16145&hashtags=test1,test2",mainURL];
  //  manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/plain"];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        //NSLog(@"**++%@",responseObject);
        
        completionBlock(YES,nil);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        
    }];
    

}
- (void) addPostComment:(NSString*)postId comment:(NSString*)comment token:(NSString*)token  completion:(void (^)(BOOL success, NSString *error))completionBlock
{
    NSDictionary *parameters = @{@"postId":postId,@"comment":comment};
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSString*url=[NSString stringWithFormat:@"%@/reg-server/services/node/addPostComment",mainURL];
  //  manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/plain"];
     [manager.requestSerializer setValue:token forHTTPHeaderField:@"auth"];
    [manager POST:url parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        //NSLog(@"**++%@",responseObject);
        
        completionBlock(YES,nil);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        
    }];

}
- (void) userContents:(NSString*)userId completion:(void (^)(BOOL success, NSString *error))completionBlock{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSString*url=[NSString stringWithFormat:@"%@reg-server/services/node/userContents?userId=asfafs&start=saasf&limit=asfsafsfa",mainURL];
    //  manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/plain"];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        //NSLog(@"**++%@",responseObject);
        
        completionBlock(YES,nil);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        
    }];

}
- (void) blockUser:(NSString*)token completion:(void (^)(BOOL success, NSString *error))completionBlock{



}
- (void) userLike:(NSString*)postid completion:(void (^)(BOOL success, NSString *error))completionBlock{
        NSDictionary *parameters = @{@"postId":postid};
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSString*url=[NSString stringWithFormat:@"%@reg-server/services/node/userLike",mainURL];
    //  manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/plain"];
    [manager POST:url parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        //NSLog(@"**++%@",responseObject);
        
        completionBlock(YES,nil);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        
    }];

}
- (void) unfollowing:(NSString*)favoriteId completion:(void (^)(BOOL success, NSString *error))completionBlock{
    NSDictionary *parameters = @{@"favoriteId":favoriteId};
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSString*url=[NSString stringWithFormat:@"%@reg-server/services/node/unfollowing",mainURL];
    //  manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/plain"];
    [manager POST:url parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        //NSLog(@"**++%@",responseObject);
        
        completionBlock(YES,nil);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        
    }];

}
- (void) ignoreUser:(NSString*)ignoredUser completion:(void (^)(BOOL success, NSString *error))completionBlock{
    NSDictionary *parameters = @{@"favoriteId":ignoredUser};
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSString*url=[NSString stringWithFormat:@"%@reg-server/services/node/ignoreUser",mainURL];
    //  manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/plain"];
    [manager POST:url parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        //NSLog(@"**++%@",responseObject);
        
        completionBlock(YES,nil);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        
    }];

}
- (void) unignoreUser:(NSString*)ignoredUser completion:(void (^)(BOOL success, NSString *error))completionBlock{
    NSDictionary *parameters = @{@"ignoredUser":ignoredUser};
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSString*url=[NSString stringWithFormat:@"%@reg-server/services/node/unignoreUser",mainURL];
    //  manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/plain"];
    [manager POST:url parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        //NSLog(@"**++%@",responseObject);
        
        completionBlock(YES,nil);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        
 }];

}
- (void) findUserFollowers:(NSString*)token completion:(void (^)(BOOL success, NSString *error))completionBlock{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/plain"];
    NSLog(@"%@",token);
    [manager.requestSerializer setValue:token forHTTPHeaderField:@"auth"];
    NSString*url=[NSString stringWithFormat:@"%@reg-server/services/node/findUserFollowers?limit=0&start=0",mainURL];
    NSLog(@"+*-%@",url);
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSArray* latestLoans = [responseObject objectForKey:@"body"][@"list"];
        NSLog(@"++**%@",responseObject);
        completionBlock(YES,nil);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        
    }];

}
- (void) popularUsers:(NSString*)token completion:(void (^)(BOOL success, NSString *error))completionBlock{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/plain"];
    NSLog(@"%@",token);
    [manager.requestSerializer setValue:token forHTTPHeaderField:@"auth"];
    NSString*url=[NSString stringWithFormat:@"%@reg-server/services/node/popularUsers?limit=0&start=0",mainURL];
    NSLog(@"+*-%@",url);
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSArray* latestLoans = [responseObject objectForKey:@"body"][@"list"];
        NSLog(@"++**%@",responseObject);
        completionBlock(YES,nil);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        
    }];
}
<<<<<<< HEAD
- (void) addPost:(NSString*)contentType resourceId:(NSString*)resourceId  description:(NSString*)description longitude:(NSString*)longitude latitude:(NSString*)latitude  completion:(void (^)(BOOL success, NSString *error))completionBlock{
        NSString *token = [[NSUserDefaults standardUserDefaults] stringForKey:@"token"];
    NSString*urls=[NSString stringWithFormat:@"%@reg-server/services/node/addPost",mainURL];
    NSString*params=[NSString stringWithFormat:@"{'contentType':'%@','resourceId' : '%@','description':'%@','longitude':'%@','latitude':'%@'}",contentType,resourceId,description,longitude,latitude];
    NSLog(@"%@",params);
    
    NSURL* url = [NSURL URLWithString:urls];
    
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"POST";
    [request setValue:token forHTTPHeaderField:@"auth"];
    request.HTTPBody = [params dataUsingEncoding:NSUTF8StringEncoding];
    
    NSURLResponse *responce;
    NSData *GETReply = [NSURLConnection sendSynchronousRequest:request returningResponse:&responce error:nil];
    NSString *theReply = [[NSString alloc] initWithBytes:[GETReply bytes] length:[GETReply length] encoding:NSASCIIStringEncoding];
    NSError* error;
    NSLog(@"%@",theReply);
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:GETReply
                                                         options:kNilOptions
                                                           error:&error];
    NSNumber * isSuccessNumber = (NSNumber *)[json objectForKey: @"success"];
    
    if([isSuccessNumber boolValue] == YES){
        completionBlock(YES,nil);
    }
    else {
        completionBlock(NO,nil);
    }

}
=======
>>>>>>> 0480d4163c366406f7e51190add26214ae62c3b4
=======

>>>>>>> parent of f7c153f... AWS integration &&More requests&&UI updated.
@end
