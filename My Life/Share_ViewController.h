//
//  Share_ViewController.h
//  My Life
//
//  Created by Admin on 16.10.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CAPSPageMenu.h"

#import "Direct_subscription.h"
#import "Subscription.h"
@interface Share_ViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIButton *title_btn;
@property (strong, nonatomic) IBOutlet UIButton *back;
- (IBAction)back:(id)sender;

@end
