//
//  Collection_View_cell.h
//  My Life
//
//  Created by Admin on 08.11.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Collection_View_cell : UITableViewCell
- (void)setCollectionData:(NSArray *)collectionData;
@end

