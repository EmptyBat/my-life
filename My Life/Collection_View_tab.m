//
//  ORGContainerCellView.m
//  HorizontalCollectionViews
//
//  Created by James Clark on 4/22/13.
//  Copyright (c) 2013 OrgSync, LLC. All rights reserved.
//

#import "Collection_View_tab.h"
#import "ArticleCollectionViewCell.h"
#import "Collection_View_cell.h"
#import <QuartzCore/QuartzCore.h>
int deleteRows;
@interface Collection_View_tab () <UICollectionViewDataSource, UICollectionViewDelegate>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) NSMutableArray *collectionData;
@end

@implementation Collection_View_tab

- (void)awakeFromNib {
    
    [_collectionView registerNib:[UINib nibWithNibName:@"Collection_view_cell" bundle:nil] forCellWithReuseIdentifier:@"Collection_view_cell"];

}

#pragma mark - Getter/Setter overrides
- (void)setCollectionData:(NSArray *)collectionData {
    _collectionData = [[NSMutableArray alloc] init];
    [_collectionData addObjectsFromArray:collectionData];
    [_collectionView setContentOffset:CGPointZero animated:NO];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.collectionView reloadData];
    });
    
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString*transform=[NSString stringWithFormat: @"%.2f", self.collectionView.contentSize.height];
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"COLLECTION"
         object:transform];
    });
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}
#pragma mark - UICollectionViewDataSource methods
- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.collectionData.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    ArticleCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Collection_view_cell" forIndexPath:indexPath];
      cell.articleImage.image = [UIImage imageNamed:_collectionData[indexPath.row]];
    return cell;

}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(collectionView.bounds.size.width/3-1, 106);
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {

    
}
@end
