//
//  SearchTab.h
//  My Life
//
//  Created by Admin on 29.09.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchTab : UIViewController
@property (strong, nonatomic) IBOutlet UICollectionView *collection;
@property (strong, nonatomic) IBOutlet UIButton *My_life_icon;
@property (strong, nonatomic) IBOutlet UIButton *add_Button;
@property (strong, nonatomic) IBOutlet UIButton *search_button;
@property (strong, nonatomic) IBOutlet UITextField *search;
@property (strong, nonatomic) IBOutlet UIView *container;

@end
