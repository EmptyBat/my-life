//
//  WaitingView.h
//  Rolyal Car Wash
//
//  Created by Almas on 11.09.15.
//  Copyright (c) 2015 acelight. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "SVProgressHUD.h"


@interface WaitingView : NSObject 

- (void) addWaitinViewToView:(UIView*)view;
- (void) removeWaitActivity ;

- (void) addBlockViewToView:(UIView*)view;
- (void) removeBlock;

@end
