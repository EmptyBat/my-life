//
//  SettingTab.h
//  My Life
//
//  Created by Admin on 25.09.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingTab : UITableViewController
@property (weak, nonatomic) IBOutlet UISwitch *close_account;
- (IBAction)back:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *back;
@property (strong, nonatomic) IBOutlet UIImageView *facebook;
@property (strong, nonatomic) IBOutlet UILabel *facebook_label;
@property (strong, nonatomic) IBOutlet UIImageView *vk;
@property (strong, nonatomic) IBOutlet UILabel *vk_label;
@property (strong, nonatomic) IBOutlet UILabel *header;
@property (strong, nonatomic) IBOutlet UILabel *theme;
@property (strong, nonatomic) IBOutlet UILabel *edit;
@property (strong, nonatomic) IBOutlet UILabel *change_password;
@property (strong, nonatomic) IBOutlet UILabel *like_it;
@property (strong, nonatomic) IBOutlet UILabel *close_account_label;
- (IBAction)close_account:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *close_account_label2;
@property (strong, nonatomic) IBOutlet UILabel *joined_account;
@property (strong, nonatomic) IBOutlet UILabel *select_language;
@property (strong, nonatomic) IBOutlet UILabel *use_data;
@property (strong, nonatomic) IBOutlet UILabel *push_notification;
@property (strong, nonatomic) IBOutlet UILabel *save_started_photo;
@property (strong, nonatomic) IBOutlet UISwitch *save_started_switch;
- (IBAction)save_started_switch:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *call_center;
@property (strong, nonatomic) IBOutlet UILabel *report_problem;
@property (strong, nonatomic) IBOutlet UILabel *advertising;
@property (strong, nonatomic) IBOutlet UILabel *blog;
@property (strong, nonatomic) IBOutlet UILabel *privacy_policy;
@property (strong, nonatomic) IBOutlet UILabel *conditions;
@property (strong, nonatomic) IBOutlet UILabel *open_source_img_library;
@property (strong, nonatomic) IBOutlet UILabel *clean_history;
@property (strong, nonatomic) IBOutlet UILabel *add_acount;
@property (strong, nonatomic) IBOutlet UILabel *exit;
@property (strong, nonatomic) IBOutlet UIButton *title_btn;


@end
