//
//  Reset_selection.h
//  My Life
//
//  Created by Admin on 28.09.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Reset_selection : UIViewController
@property (strong, nonatomic) IBOutlet UIButton *back;
- (IBAction)back:(id)sender;
@property (strong, nonatomic) IBOutlet UIImageView *avatar;
@property (strong, nonatomic) IBOutlet UIButton *send_to_email;
- (IBAction)reset_FB:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *reset_FB;
- (IBAction)send_to_email:(id)sender;

@end
