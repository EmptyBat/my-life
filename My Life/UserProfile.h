//
//  UserProfile.h
//  My Life
//
//  Created by Admin on 13.10.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserProfile : UITableViewController
@property (strong, nonatomic) IBOutlet UIButton *follower;
@property (strong, nonatomic) IBOutlet UIButton *setting;
@property (strong, nonatomic) IBOutlet UIButton *subs;
- (IBAction)subs:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *posts;
@property (strong, nonatomic) IBOutlet UILabel *followers;
@property (strong, nonatomic) IBOutlet UILabel *following;
- (IBAction)setting:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *header;
@property (strong, nonatomic) IBOutlet UIButton *Title_btn;
@property (strong, nonatomic) IBOutlet UIButton *tab_btn_1;
@property (strong, nonatomic) IBOutlet UIButton *tab_btn_2;
@property (strong, nonatomic) IBOutlet UIButton *tab_btn_3;
- (IBAction)tab_btn_1:(id)sender;
- (IBAction)tab_btn_2:(id)sender;
- (IBAction)tab_btn_3:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *posts_title;
@property (strong, nonatomic) IBOutlet UILabel *followers_title;
@property (strong, nonatomic) IBOutlet UILabel *following_title;
@property (strong, nonatomic) IBOutlet UILabel *name;
@property (strong, nonatomic) IBOutlet UILabel *status_label;
@end
