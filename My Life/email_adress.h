//
//  email_adress.h
//  My Life
//
//  Created by Admin on 05.10.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface email_adress : UITableViewController
- (IBAction)back:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *title_btn;
@property (strong, nonatomic) IBOutlet UITextField *email;
@property (strong, nonatomic) IBOutlet UIButton *x;
- (IBAction)x:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *done;
- (IBAction)done:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *back;

@end
