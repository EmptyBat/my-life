//
//  UserProfile.m
//  My Life
//
//  Created by Admin on 13.10.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "UserProfile.h"
#import "TLYShyNavBarManager.h"
#import "ArticleCollectionViewCell.h"
#import "Collection_View_cell.h"
#import "Collection_View_tab.h"
@interface UserProfile ()
{
    UIColor*tableCellColor;
    UIColor*textColor;
    UIColor*imageColor;
    UIColor*textColor_main;
    NSArray*images;
    int mode;
    CGFloat Content_size;
}
@end

@implementation UserProfile

- (void)viewDidLoad {
    [super viewDidLoad];
    mode=1;
    images=@[@"261087",@"Inside-Chechnya_pixanews-4",@"Layer 591",@"1182e81d6f7efe4f916de7a0d4a_prev.jpg",@"261087",@"Inside-Chechnya_pixanews-4",@"Layer 591",@"1182e81d6f7efe4f916de7a0d4a_prev.jpg",@"261087",@"Inside-Chechnya_pixanews-4",@"Layer 591",@"1182e81d6f7efe4f916de7a0d4a_prev.jpg",@"261087",@"Inside-Chechnya_pixanews-4",@"Layer 591",@"1182e81d6f7efe4f916de7a0d4a_prev.jpg"];
    _follower.layer.cornerRadius = 8;
    NSString *theme = [[NSUserDefaults standardUserDefaults] stringForKey:@"theme"];
    if ([theme isEqualToString:@"black_diamond"]){
        [self black_diamond_style];
    }
    else  if ([theme isEqualToString:@"sea_blue"]){
        [self sea_blue_style];
    }
    else  if ([theme isEqualToString:@"soft_pink"]){
        [self soft_pink_style];
    }
    else  if ([theme isEqualToString:@"green"]){
        [self green_style];
    }
    else {
        [self black_pearl_style];
    }
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(THEME:)
                                                 name:@"THEME" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(collection:)
                                                 name:@"COLLECTION" object:nil];
    [self.tableView registerClass:[Collection_View_cell class] forCellReuseIdentifier:@"Collection_view_cell"];
}
-(void)collection:(NSNotification *)notice{
    CGFloat strFloat = (CGFloat)[[notice object] floatValue];
    Content_size=strFloat;
    NSLog(@"%f",strFloat);
    [self.tableView reloadData];
}
- (void)THEME:(NSNotification *)notice{
    NSString *theme = [[NSUserDefaults standardUserDefaults] stringForKey:@"theme"];
    if ([theme isEqualToString:@"black_diamond"]){
        [self black_diamond_style];
    }
    else  if ([theme isEqualToString:@"sea_blue"]){
        [self sea_blue_style];
    }
    else  if ([theme isEqualToString:@"soft_pink"]){
        [self soft_pink_style];
    }
    else  if ([theme isEqualToString:@"green"]){
        [self green_style];
    }
    else {
        [self black_pearl_style];
    }
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (mode==1){
        static NSString *cellIdentifier = @"main";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        }
        UILabel*name=(UILabel*) [cell viewWithTag:1];
        UILabel*place=(UILabel*) [cell viewWithTag:3];
        UIButton*Fillled=(UIButton*) [cell viewWithTag:5];
        UIButton*like_count=(UIButton*) [cell viewWithTag:6];
        UIButton*Repost=(UIButton*) [cell viewWithTag:7];
        UIButton*love=(UIButton*) [cell viewWithTag:8];
        UIButton*question=(UIButton*) [cell viewWithTag:9];
        UIButton* checkbox=(UIButton*) [cell viewWithTag:5];
        UIImage *image22 = [[UIImage imageNamed:@"love"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        UIImage *imageSelected = [[UIImage imageNamed:@"Fillled"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [checkbox setImage:image22 forState:UIControlStateNormal];
        [checkbox setImage:imageSelected forState:UIControlStateSelected];
        [checkbox setImage:imageSelected forState:UIControlStateHighlighted];
        checkbox.tintColor= imageColor;
        checkbox.adjustsImageWhenHighlighted=YES;
        [checkbox addTarget:self
                     action:@selector(chkBtnHandler:)
           forControlEvents:UIControlEventTouchUpInside];
        UIImage *image = [[UIImage imageNamed:@"Fillled"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [Fillled setImage:image forState:UIControlStateNormal];
        Fillled.tintColor= imageColor;
        UIImage *image1 = [[UIImage imageNamed:@"Repost"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [Repost setImage:image1 forState:UIControlStateNormal];
        Repost.tintColor= imageColor;
        UIImage *image2 = [[UIImage imageNamed:@"love"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [love setImage:image2 forState:UIControlStateNormal];
        love.tintColor= imageColor;
        UIImage *image3 = [[UIImage imageNamed:@"question"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [question setImage:image3 forState:UIControlStateNormal];
        question.tintColor= imageColor;
        name.textColor=textColor_main;
        place.textColor=textColor_main;
        [like_count setTitleColor:textColor_main forState:UIControlStateNormal];
        UIImageView*img=(UIImageView*) [cell viewWithTag:4];
        img.image=[UIImage imageNamed:images[indexPath.section]];
        UIButton*points=(UIButton*) [cell viewWithTag:11];
        UIImage *image4 = [[UIImage imageNamed:@"3 points"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [points setImage:image4 forState:UIControlStateNormal];
        points.tintColor= imageColor;
        /*  NSDictionary* place = [_count objectAtIndex:indexPath.row];
         NSString *name = [place objectForKey:@"category_name"];
         cell.textLabel.text=name;*/
        return cell;
    }
    else {
        Collection_View_cell *cell = [tableView dequeueReusableCellWithIdentifier:@"Collection_view_cell"];
        [cell setCollectionData:images];
        cell.userInteractionEnabled=YES;
        return cell;
        
    }
    
}
- (void)chkBtnHandler:(id)sender {
    // If checked, uncheck and visa versa
    [(UIButton *)sender setSelected:![(UIButton *)sender isSelected]];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (mode==1){
        return 8;
    }
    else {
        return 1;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (mode==1){
        if (indexPath.section==0&&indexPath.row==0){
            return 360;
        }
        else if (indexPath.section==1&&indexPath.row==0){
            return 450;
        }
        else if (indexPath.section==2&&indexPath.row==0){
            return 250;
        }
        else if (indexPath.section==3&&indexPath.row==0){
            return 450;
        }
        else if (indexPath.section==4&&indexPath.row==0){
            return 450;
        }
        else if (indexPath.section==5&&indexPath.row==0){
            return 450;
        }
        else if (indexPath.section==6&&indexPath.row==0){
            return 360;
        }
        else if (indexPath.section==7&&indexPath.row==0){
            return 450;
        }
        else if (indexPath.section==8&&indexPath.row==0){
            return 360;
        }
        else {
            return 63;
            
        }
    }
    else {
        return Content_size;
    }
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}


- (IBAction)back:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)subs:(id)sender {
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    [cell setBackgroundColor:tableCellColor];
    [cell.contentView setBackgroundColor:tableCellColor];
    if (tableCellColor==nil){
        [cell setBackgroundColor:[UIColor whiteColor]];
        [cell.contentView setBackgroundColor:[UIColor whiteColor]];
    }
}
-(void)black_pearl_style{
    tableCellColor=[UIColor whiteColor];
    _name.textColor=[UIColor blackColor];
    _status_label.textColor=[UIColor blackColor];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleDefault;
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1.0];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor blackColor]};
    self.tableView.backgroundColor =[UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1.0];
    UIImage *image1 = [[UIImage imageNamed:@"setting"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.setting setImage:image1 forState:UIControlStateNormal];
    self.setting.tintColor = [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    UIImage *image2 = [[UIImage imageNamed:@"profile_tab_1"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.tab_btn_1 setImage:image2 forState:UIControlStateNormal];
    self.tab_btn_1.tintColor = [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    UIImage *image3 = [[UIImage imageNamed:@"profile_tab_2"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.tab_btn_2 setImage:image3 forState:UIControlStateNormal];
    self.tab_btn_2.tintColor = [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    UIImage *image4 = [[UIImage imageNamed:@"profile_tab_3"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.tab_btn_3 setImage:image4 forState:UIControlStateNormal];
    self.tab_btn_3.tintColor = [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    _header.layer.shadowColor = [UIColor colorWithRed:235.0/255.0 green:235.0/255.0 blue:235.0/255.0 alpha:1.0].CGColor;
    _header.layer.shadowOffset = CGSizeMake(10, 10);
    _header.layer.shadowOpacity = 1;
    _header.layer.shadowRadius = 1.0;
    _header.backgroundColor=[UIColor whiteColor];
    [_Title_btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    self.tabBarController.tabBar.barTintColor = [UIColor whiteColor];
    self.tabBarController.tabBar.tintColor = [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    imageColor= [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    textColor = [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    textColor_main=[UIColor blackColor];
    _posts.textColor= [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    _followers.textColor= [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    _following.textColor= [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    [_subs setBackgroundColor: [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1]];
    [self.tableView reloadData];
}
-(void)black_diamond_style{
    _name.textColor=[UIColor whiteColor];
    _status_label.textColor=[UIColor whiteColor];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleBlackOpaque;
    tableCellColor=[UIColor blackColor];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor =[UIColor blackColor];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    self.tableView.backgroundColor =[UIColor colorWithRed:23.0/255 green:23.0/255 blue:23.0/255 alpha:1.0];
    UIImage *image1 = [[UIImage imageNamed:@"setting"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.setting setImage:image1 forState:UIControlStateNormal];
    self.setting.tintColor = [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    UIImage *image2 = [[UIImage imageNamed:@"profile_tab_1"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.tab_btn_1 setImage:image2 forState:UIControlStateNormal];
    self.tab_btn_1.tintColor =  [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    UIImage *image3 = [[UIImage imageNamed:@"profile_tab_2"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.tab_btn_2 setImage:image3 forState:UIControlStateNormal];
    self.tab_btn_2.tintColor =  [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    UIImage *image4 = [[UIImage imageNamed:@"profile_tab_3"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.tab_btn_3 setImage:image4 forState:UIControlStateNormal];
    self.tab_btn_3.tintColor =  [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    [_Title_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _header.layer.shadowColor =[UIColor colorWithRed:23.0/255 green:23.0/255 blue:23.0/255 alpha:1.0].CGColor;
    _header.layer.shadowOffset = CGSizeMake(10, 10);
    _header.layer.shadowOpacity = 1;
    _header.layer.shadowRadius = 1.0;
    _header.backgroundColor=[UIColor blackColor];
    self.tabBarController.tabBar.barTintColor = [UIColor blackColor];
    self.tabBarController.tabBar.tintColor = [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    imageColor=  [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    textColor = [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    textColor_main=[UIColor whiteColor];
    _posts.textColor= [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    _followers.textColor= [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    _following.textColor= [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    [_subs setBackgroundColor:[UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1]];
    _posts_title.textColor=[UIColor whiteColor];
    _followers_title.textColor=[UIColor whiteColor];
    _following_title.textColor=[UIColor whiteColor];
    [self.tableView reloadData];
}
-(void)sea_blue_style{
    _name.textColor=[UIColor blackColor];
    _status_label.textColor=[UIColor blackColor];
    tableCellColor=[UIColor whiteColor];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:2.0/255 green:35.0/255 blue:143.0/255 alpha:1.0];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    self.tableView.backgroundColor =[UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1.0];
    UIImage *image1 = [[UIImage imageNamed:@"setting"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.setting setImage:image1 forState:UIControlStateNormal];
    self.setting.tintColor = [UIColor whiteColor];
    UIImage *image2 = [[UIImage imageNamed:@"profile_tab_1"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.tab_btn_1 setImage:image2 forState:UIControlStateNormal];
    self.tab_btn_1.tintColor =  [UIColor colorWithRed:2.0/255 green:35.0/255 blue:143.0/255 alpha:1.0];
    UIImage *image3 = [[UIImage imageNamed:@"profile_tab_2"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.tab_btn_2 setImage:image3 forState:UIControlStateNormal];
    self.tab_btn_2.tintColor =  [UIColor colorWithRed:2.0/255 green:35.0/255 blue:143.0/255 alpha:1.0];
    UIImage *image4 = [[UIImage imageNamed:@"profile_tab_3"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.tab_btn_3 setImage:image4 forState:UIControlStateNormal];
    self.tab_btn_3.tintColor =  [UIColor colorWithRed:2.0/255 green:35.0/255 blue:143.0/255 alpha:1.0];
    [_Title_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _header.layer.shadowColor = [UIColor colorWithRed:235.0/255.0 green:235.0/255.0 blue:235.0/255.0 alpha:1.0].CGColor;
    _header.layer.shadowOffset = CGSizeMake(10, 10);
    _header.layer.shadowOpacity = 1;
    _header.layer.shadowRadius = 1.0;
    _header.backgroundColor=[UIColor whiteColor];
    self.tabBarController.tabBar.barTintColor =[UIColor whiteColor];
    self.tabBarController.tabBar.tintColor = [UIColor colorWithRed:2.0/255 green:35.0/255 blue:143.0/255 alpha:1.0];
    imageColor= [UIColor colorWithRed:2.0/255 green:35.0/255 blue:143.0/255 alpha:1.0];
    textColor = [UIColor colorWithRed:2.0/255 green:35.0/255 blue:143.0/255 alpha:1.0];
    textColor_main=[UIColor blackColor];
    _posts.textColor= [UIColor colorWithRed:2.0/255 green:35.0/255 blue:143.0/255 alpha:1.0];
    _followers.textColor= [UIColor colorWithRed:2.0/255 green:35.0/255 blue:143.0/255 alpha:1.0];
    _following.textColor= [UIColor colorWithRed:2.0/255 green:35.0/255 blue:143.0/255 alpha:1.0];
    [_subs setBackgroundColor: [UIColor colorWithRed:2.0/255 green:35.0/255 blue:143.0/255 alpha:1.0]];
    _posts_title.textColor=[UIColor blackColor];
    _followers_title.textColor=[UIColor blackColor];
    _following_title.textColor=[UIColor blackColor];
    [self.tableView reloadData];
}
-(void)soft_pink_style{
    _name.textColor=[UIColor blackColor];
    _status_label.textColor=[UIColor blackColor];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:231.0/255 green:194.0/255 blue:202.0/255 alpha:1.0];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    self.tableView.backgroundColor =[UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1.0];
    UIImage *image1 = [[UIImage imageNamed:@"setting"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.setting setImage:image1 forState:UIControlStateNormal];
    self.setting.tintColor = [UIColor whiteColor];
    tableCellColor=[UIColor whiteColor];
    UIImage *image2 = [[UIImage imageNamed:@"profile_tab_1"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.tab_btn_1 setImage:image2 forState:UIControlStateNormal];
    self.tab_btn_1.tintColor =  [UIColor colorWithRed:231.0/255 green:194.0/255 blue:202.0/255 alpha:1.0];
    UIImage *image3 = [[UIImage imageNamed:@"profile_tab_2"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.tab_btn_2 setImage:image3 forState:UIControlStateNormal];
    self.tab_btn_2.tintColor =  [UIColor colorWithRed:231.0/255 green:194.0/255 blue:202.0/255 alpha:1.0];
    UIImage *image4 = [[UIImage imageNamed:@"profile_tab_3"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.tab_btn_3 setImage:image4 forState:UIControlStateNormal];
    self.tab_btn_3.tintColor =  [UIColor colorWithRed:231.0/255 green:194.0/255 blue:202.0/255 alpha:1.0];
    [_Title_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _header.layer.shadowColor = [UIColor colorWithRed:235.0/255.0 green:235.0/255.0 blue:235.0/255.0 alpha:1.0].CGColor;
    _header.layer.shadowOffset = CGSizeMake(10, 10);
    _header.layer.shadowOpacity = 1;
    _header.layer.shadowRadius = 1.0;
    _header.backgroundColor=[UIColor whiteColor];
    self.tabBarController.tabBar.barTintColor =[UIColor whiteColor];
    self.tabBarController.tabBar.tintColor =[UIColor colorWithRed:231.0/255 green:194.0/255 blue:202.0/255 alpha:1.0];
    imageColor= [UIColor colorWithRed:231.0/255 green:194.0/255 blue:202.0/255 alpha:1.0];
    textColor = [UIColor colorWithRed:231.0/255 green:194.0/255 blue:202.0/255 alpha:1.0];
    textColor_main=[UIColor blackColor];
    _posts.textColor= [UIColor colorWithRed:231.0/255 green:194.0/255 blue:202.0/255 alpha:1.0];
    _followers.textColor= [UIColor colorWithRed:231.0/255 green:194.0/255 blue:202.0/255 alpha:1.0];
    _following.textColor= [UIColor colorWithRed:231.0/255 green:194.0/255 blue:202.0/255 alpha:1.0];
    [_subs setBackgroundColor:  [UIColor colorWithRed:231.0/255 green:194.0/255 blue:202.0/255 alpha:1.0]];
    _posts_title.textColor=[UIColor blackColor];
    _followers_title.textColor=[UIColor blackColor];
    _following_title.textColor=[UIColor blackColor];
    [self.tableView reloadData];
}
-(void)green_style{
    _name.textColor=[UIColor blackColor];
    _status_label.textColor=[UIColor blackColor];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:23.0/255 green:125.0/255 blue:46.0/255 alpha:1.0];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    self.tableView.backgroundColor =[UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1.0];
    UIImage *image1 = [[UIImage imageNamed:@"setting"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.setting setImage:image1 forState:UIControlStateNormal];
    self.setting.tintColor = [UIColor whiteColor];
    tableCellColor=[UIColor whiteColor];
    UIImage *image2 = [[UIImage imageNamed:@"profile_tab_1"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.tab_btn_1 setImage:image2 forState:UIControlStateNormal];
    self.tab_btn_1.tintColor = [UIColor colorWithRed:23.0/255 green:125.0/255 blue:46.0/255 alpha:1.0];
    UIImage *image3 = [[UIImage imageNamed:@"profile_tab_2"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.tab_btn_2 setImage:image3 forState:UIControlStateNormal];
    self.tab_btn_2.tintColor = [UIColor colorWithRed:23.0/255 green:125.0/255 blue:46.0/255 alpha:1.0];
    UIImage *image4 = [[UIImage imageNamed:@"profile_tab_3"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.tab_btn_3 setImage:image4 forState:UIControlStateNormal];
    self.tab_btn_3.tintColor = [UIColor colorWithRed:23.0/255 green:125.0/255 blue:46.0/255 alpha:1.0];
    [_Title_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _header.layer.shadowColor = [UIColor colorWithRed:235.0/255.0 green:235.0/255.0 blue:235.0/255.0 alpha:1.0].CGColor;
    _header.layer.shadowOffset = CGSizeMake(10, 10);
    _header.layer.shadowOpacity = 1;
    _header.layer.shadowRadius = 1.0;
    _header.backgroundColor=[UIColor whiteColor];
    self.tabBarController.tabBar.barTintColor =[UIColor whiteColor];
    self.tabBarController.tabBar.tintColor = [UIColor colorWithRed:23.0/255 green:125.0/255 blue:46.0/255 alpha:1.0];
    imageColor= [UIColor colorWithRed:23.0/255 green:125.0/255 blue:46.0/255 alpha:1.0];
    textColor = [UIColor colorWithRed:23.0/255 green:125.0/255 blue:46.0/255 alpha:1.0];
    textColor_main=[UIColor blackColor];
    _posts.textColor= [UIColor colorWithRed:23.0/255 green:125.0/255 blue:46.0/255 alpha:1.0];
    _followers.textColor= [UIColor colorWithRed:23.0/255 green:125.0/255 blue:46.0/255 alpha:1.0];
    _following.textColor= [UIColor colorWithRed:23.0/255 green:125.0/255 blue:46.0/255 alpha:1.0];
    [_subs setBackgroundColor: [UIColor colorWithRed:23.0/255 green:125.0/255 blue:46.0/255 alpha:1.0]];
    _posts_title.textColor=[UIColor blackColor];
    _followers_title.textColor=[UIColor blackColor];
    _following_title.textColor=[UIColor blackColor];
    [self.tableView reloadData];
}
- (IBAction)setting:(id)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self
                                                    cancelButtonTitle:nil
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:nil];
    UIImageView *background = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"actionsheet_bg.png"]];
    [background setFrame:CGRectMake(0, 0, 320, 320)];
    background.contentMode = UIViewContentModeScaleToFill;
    [actionSheet addSubview:background];
    
    UIButton *cancelButton = [UIButton buttonWithType: UIButtonTypeCustom];
    cancelButton.frame = CGRectMake(0, 260, 320, 50);
    [cancelButton setBackgroundImage:[UIImage imageNamed:@"actionsheet_button.png"] forState: UIControlStateNormal];
    [cancelButton addTarget:self action:@selector(cancelButtonClicked:)    forControlEvents:UIControlEventTouchUpInside];
    cancelButton.adjustsImageWhenHighlighted = YES;
    [cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancelButton setTitleColor:[UIColor colorWithRed:0/255.0f green:177/255.0f blue:148/255.0f alpha:1.0f] forState:UIControlStateNormal];
    cancelButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    cancelButton.titleLabel.font = [UIFont fontWithName: @"SourceSansPro-Light" size: 25];
    
    [actionSheet addSubview: cancelButton];
    
    UIButton *emailResultsButton = [UIButton buttonWithType: UIButtonTypeCustom];
    emailResultsButton.frame = CGRectMake(25, 12, 232, 25);
    [emailResultsButton addTarget:self action:@selector(emailResultsTapped:) forControlEvents:UIControlEventTouchUpInside];
    emailResultsButton.adjustsImageWhenHighlighted = YES;
    [emailResultsButton setTitle:@"Email Results" forState:UIControlStateNormal];
    [emailResultsButton setTitleColor:[UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:1.0f] forState:UIControlStateNormal];
    [emailResultsButton setTitleColor:[UIColor colorWithRed:0/255.0f green:177/255.0f blue:148/255.0f alpha:1.0f] forState:UIControlStateHighlighted];
    emailResultsButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    emailResultsButton.titleLabel.font = [UIFont fontWithName: @"SourceSansPro-Light" size: 20];
    [actionSheet addSubview: emailResultsButton];
    [actionSheet showInView:self.view];
    [actionSheet setBounds:CGRectMake(0,0,320, 600)];
}
- (IBAction)tab_btn_1:(id)sender {
    mode=1;
    [self.tableView reloadData];
}
- (IBAction)tab_btn_2:(id)sender {
    mode=2;
    [self.tableView reloadData];
}
- (IBAction)tab_btn_3:(id)sender {
    self.tabBarController.selectedIndex=1;
}
@end
