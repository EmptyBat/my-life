//
//  SHModel.h
//  My Life
//
//  Created by Admin on 05.12.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"
@interface SHModel : NSObject


// ------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Authorization
- (void) LoginRequest:(NSString*)email pwd:(NSString*)pwd type:(NSString*)type completion:(void (^)(BOOL success, NSString *error,NSString*expires,NSString*token))completionBlock;
- (void) registerWithEmail:(NSString*)email  completion:(void  (^)(BOOL success, NSString *error))completionBlock;
- (void) registerWithPhone:(NSString*)phone  completion:(void  (^)(BOOL success, NSString *error))completionBlock;
- (void) resetPasswordWithEmail:(NSString*)email completion:(void (^)(BOOL success, NSString *message, NSString *error))completionBlock;
- (void) logoutWithCompletion:(void (^)(BOOL success, NSString *error))completionBlock;;

- (void) checkEmail:(NSString*)email completion:(void (^)(BOOL success, NSString *error))completionBlock;
- (void) checkPhone:(NSString*)phone completion:(void (^)(BOOL success, NSString *error))completionBlock;
- (void) frendsByPhoneList:(NSArray*)numbers completion:(void (^)(BOOL success, NSString *error))completionBlock;
- (void) authFB:(NSString*)token completion:(void (^)(BOOL success, NSString *error,NSString*expires,NSString*token))completionBlock;
- (void) authVK:(NSString*)token completion:(void (^)(BOOL success, NSString *error))completionBlock;
<<<<<<< HEAD
- (void) refreshToken:(NSString*)token completion:(void (^)(BOOL success, NSString *error,NSString*expires,NSString*token))completionBlock;
- (void) registrationDataPhone:(NSString*)phone password:(NSString*)password surname:(NSString*)surname name:(NSString*)name completion:(void (^)(BOOL success, NSString *error))completionBlock;
- (void) registrationDataEmail:(NSString*)email password:(NSString*)password surname:(NSString*)surname name:(NSString*)name completion:(void (^)(BOOL success, NSString *error))completionBlock;
- (void) AwsToken:(NSString*)token contentHashString:(NSString*)contentHashString contentLength:(NSString*)contentLength  completion:(void (^)(BOOL success, NSString *error))completionBlock;
- (void) UpdateAvatar:(NSString*)resourceId  token:(NSString*)token  completion:(void (^)(BOOL success, NSString *error))completionBlock;
// UserData
- (void) addFollower:(NSString*)token   favoriteId:(NSString*)favoriteId completion:(void (^)(BOOL success, NSString *error))completionBlock;
- (void) addPostComment:(NSString*)postId comment:(NSString*)comment   token:(NSString*)token   completion:(void (^)(BOOL success, NSString *error))completionBlock;
- (void) addPost:(NSString*)userId resourceId:(NSString*)resourceId description:(NSString*)description completion:(void (^)(BOOL success, NSString *error))completionBlock;
- (void) blockUser:(NSString*)token completion:(void (^)(BOOL success, NSString *error))completionBlock;
- (void) deleteUser:(NSString*)token completion:(void (^)(BOOL success, NSString *error))completionBlock;
- (void) findContens:(NSString*)longitude latitude:(NSString*)latitude hashtags:(NSString*)hashtags completion:(void (^)(BOOL success, NSString *error))completionBlock;
- (void) getTapePosts:(NSString*)token completion:(void (^)(BOOL success, NSString *error,id objects))completionBlock;
- (void) updateUser:(NSString*)token completion:(void (^)(BOOL success, NSString *error))completionBlock;
- (void) popularUsers:(NSString*)token completion:(void (^)(BOOL success, NSString *error))completionBlock;
- (void) userContents:(NSString*)userId completion:(void (^)(BOOL success, NSString *error))completionBlock;
- (void) userLike:(NSString*)postid completion:(void (^)(BOOL success, NSString *error))completionBlock;
- (void) unfollowing:(NSString*)favoriteId completion:(void (^)(BOOL success, NSString *error))completionBlock;
- (void) ignoreUser:(NSString*)ignoredUser completion:(void (^)(BOOL success, NSString *error))completionBlock;
- (void) unignoreUser:(NSString*)ignoredUser completion:(void (^)(BOOL success, NSString *error))completionBlock;
- (void) findUserFollowers:(NSString*)tpken completion:(void (^)(BOOL success, NSString *error))completionBlock;
=======
- (void) refreshToken:(NSString*)token completion:(void (^)(BOOL success, NSString *error))completionBlock;
>>>>>>> 0480d4163c366406f7e51190add26214ae62c3b4

// UserData
- (void) addFollower:(NSString*)token completion:(void (^)(BOOL success, NSString *error))completionBlock;
- (void) addPostComment:(NSString*)userId postId:(NSString*)postId comment:(NSString*)comment completion:(void (^)(BOOL success, NSString *error))completionBlock;
- (void) addPost:(NSString*)userId resourceId:(NSString*)resourceId description:(NSString*)description completion:(void (^)(BOOL success, NSString *error))completionBlock;
- (void) blockUser:(NSString*)token completion:(void (^)(BOOL success, NSString *error))completionBlock;
- (void) deleteUser:(NSString*)token completion:(void (^)(BOOL success, NSString *error))completionBlock;
- (void) findContens:(NSString*)token completion:(void (^)(BOOL success, NSString *error))completionBlock;
- (void) getTapePosts:(NSString*)token completion:(void (^)(BOOL success, NSString *error,id objects))completionBlock;
- (void) updateUser:(NSString*)token completion:(void (^)(BOOL success, NSString *error))completionBlock;
- (void) popularUsers:(NSString*)token completion:(void (^)(BOOL success, NSString *error))completionBlock;
- (void) userContents:(NSString*)token completion:(void (^)(BOOL success, NSString *error))completionBlock;
@end
