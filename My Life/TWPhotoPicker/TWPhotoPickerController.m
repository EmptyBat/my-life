//
//  TWPhotoPickerController.m
//  InstagramPhotoPicker
//
//  Created by Emar on 12/4/14.
//  Copyright (c) 2014 wenzhaot. All rights reserved.
//

#import "TWPhotoPickerController.h"
#import "TWPhotoCollectionViewCell.h"
#import "TWImageScrollView.h"
#import "TWPhotoLoader.h"
#import "HomeViewController.h"
@interface TWPhotoPickerController ()<UICollectionViewDataSource, UICollectionViewDelegate> {
    CGFloat beginOriginY;
    UIColor*tableCellColor;
    UIColor*textColor;
    UIColor*imageColor;
    UIColor*textColor_main;
}
@property (strong, nonatomic) UIView *topView;
@property (strong, nonatomic) UIImageView *maskView;
@property (strong, nonatomic) UICollectionView *collectionView;
@property (strong, nonatomic) TWImageScrollView *imageScrollView;

@property (strong, nonatomic) NSArray *allPhotos;
@property(strong, nonatomic) UIButton*camera_btn;
@property(strong, nonatomic) UIButton*video_btn;
@property(strong, nonatomic) UIButton*library_btn;
@property(strong, nonatomic) UIButton*back;
@property(strong, nonatomic) UIButton*cropBtn;
@property(strong,nonnull) UIView *buttonView;
@property(strong,nonnull) UIView *navView;
@property(strong,nonnull) UIView *dragView;
@property(strong,nonnull) UILabel *titleLabel;
@property (strong, nonatomic) UIImageView *gripView;
@end

@implementation TWPhotoPickerController

#pragma mark - life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view setBackgroundColor:[UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1.0]];
    [self.view addSubview:self.topView];
    [self.view insertSubview:self.collectionView belowSubview:self.topView];
    [self addButtons];
       [self loadPhotos];
    NSString *theme = [[NSUserDefaults standardUserDefaults] stringForKey:@"theme"];
    if ([theme isEqualToString:@"black_diamond"]){
        [self black_diamond_style];
    }
    else  if ([theme isEqualToString:@"sea_blue"]){
        [self sea_blue_style];
    }
    else  if ([theme isEqualToString:@"soft_pink"]){
        [self soft_pink_style];
    }
    else  if ([theme isEqualToString:@"green"]){
        [self green_style];
    }
    else {
        [self black_pearl_style];
    }
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(THEME:)
                                                 name:@"THEME" object:nil];
    
}
- (void)THEME:(NSNotification *)notice{
    NSString *theme = [[NSUserDefaults standardUserDefaults] stringForKey:@"theme"];
    if ([theme isEqualToString:@"black_diamond"]){
        [self black_diamond_style];
    }
    else  if ([theme isEqualToString:@"sea_blue"]){
        [self sea_blue_style];
    }
    else  if ([theme isEqualToString:@"soft_pink"]){
        [self soft_pink_style];
    }
    else  if ([theme isEqualToString:@"green"]){
        [self green_style];
    }
    else {
        [self black_pearl_style];
    }
    
}
-(void)black_pearl_style{
    tableCellColor=[UIColor whiteColor];
    textColor = [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    textColor_main=[UIColor blackColor];
    
    tableCellColor=[UIColor whiteColor];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleDefault;
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1.0];;
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor blackColor]};
     UIImage *image = [[UIImage imageNamed:@"back.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.back setImage:image forState:UIControlStateNormal];
    self.navView.backgroundColor=tableCellColor;
    self.back.tintColor = textColor;
    self.titleLabel.textColor=textColor_main;
    self.buttonView.backgroundColor=tableCellColor;
    UIImage *image1 = [[UIImage imageNamed:@"Camera_button"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    UIImage *image2 = [[UIImage imageNamed:@"video_button-1"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    UIImage *image3 = [[UIImage imageNamed:@"cameraroll-picker-grip.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.gripView.image=image3;
    self.gripView.tintColor=textColor;
    [self.camera_btn setImage:image1 forState:UIControlStateNormal];
    [self.video_btn setImage:image2 forState:UIControlStateNormal];
    self.camera_btn.tintColor=textColor;
    self.video_btn.tintColor=textColor;
    [self.library_btn setTitleColor:textColor forState:UIControlStateNormal];
    [self.cropBtn setTitleColor:tableCellColor forState:UIControlStateNormal];
    _back.tintColor = textColor;
    [_cropBtn setTitleColor:textColor forState:UIControlStateNormal];
    _navView.backgroundColor=tableCellColor;
}
-(void)black_diamond_style{
    tableCellColor=[UIColor blackColor];
    textColor = [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    textColor_main=[UIColor whiteColor];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleBlackOpaque;
    tableCellColor=[UIColor blackColor];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor =[UIColor blackColor];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    self.tabBarController.tabBar.barTintColor = [UIColor blackColor];
    self.tabBarController.tabBar.tintColor = [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    UIImage *image = [[UIImage imageNamed:@"back.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.back setImage:image forState:UIControlStateNormal];
    self.navView.backgroundColor=tableCellColor;
    self.titleLabel.textColor=textColor_main;
    self.back.tintColor = textColor;
    self.buttonView.backgroundColor=tableCellColor;
    UIImage *image1 = [[UIImage imageNamed:@"Camera_button"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    UIImage *image2 = [[UIImage imageNamed:@"video_button-1"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    UIImage *image3 = [[UIImage imageNamed:@"cameraroll-picker-grip.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.gripView.image=image3;
    self.gripView.tintColor=textColor;
    [self.camera_btn setImage:image1 forState:UIControlStateNormal];
    [self.video_btn setImage:image2 forState:UIControlStateNormal];
    self.camera_btn.tintColor=textColor;
    self.video_btn.tintColor=textColor;
    [self.library_btn setTitleColor:textColor forState:UIControlStateNormal];
    [self.cropBtn setTitleColor:textColor forState:UIControlStateNormal];
    _back.tintColor = textColor;
    [_cropBtn setTitleColor:textColor forState:UIControlStateNormal];
        _navView.backgroundColor=tableCellColor;
    
}
-(void)sea_blue_style{
    tableCellColor=[UIColor whiteColor];
    textColor = [UIColor colorWithRed:2.0/255 green:35.0/255 blue:143.0/255 alpha:1.0];
    textColor_main=[UIColor blackColor];
    
    tableCellColor=[UIColor whiteColor];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:2.0/255 green:35.0/255 blue:143.0/255 alpha:1.0];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    tableCellColor=[UIColor whiteColor];
    self.tabBarController.tabBar.barTintColor =[UIColor whiteColor];
    self.tabBarController.tabBar.tintColor = [UIColor colorWithRed:2.0/255 green:35.0/255 blue:143.0/255 alpha:1.0];
    UIImage *image = [[UIImage imageNamed:@"back.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.back setImage:image forState:UIControlStateNormal];
    self.navView.backgroundColor=tableCellColor;
    self.titleLabel.textColor=tableCellColor;
    self.back.tintColor = textColor;
    self.buttonView.backgroundColor=tableCellColor;
    UIImage *image1 = [[UIImage imageNamed:@"Camera_button"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    UIImage *image2 = [[UIImage imageNamed:@"video_button-1"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    UIImage *image3 = [[UIImage imageNamed:@"cameraroll-picker-grip.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.gripView.image=image3;
    self.gripView.tintColor=textColor;
    [self.camera_btn setImage:image1 forState:UIControlStateNormal];
    [self.video_btn setImage:image2 forState:UIControlStateNormal];
    self.camera_btn.tintColor=textColor;
    self.video_btn.tintColor=textColor;
    [self.library_btn setTitleColor:textColor forState:UIControlStateNormal];
    [self.cropBtn setTitleColor:tableCellColor forState:UIControlStateNormal];
    _back.tintColor = tableCellColor;
    [_cropBtn setTitleColor:tableCellColor forState:UIControlStateNormal];
        _navView.backgroundColor=textColor;
}
-(void)soft_pink_style{
    tableCellColor=[UIColor whiteColor];
    textColor = [UIColor colorWithRed:231.0/255 green:194.0/255 blue:202.0/255 alpha:1.0];
    textColor_main=[UIColor blackColor];
    
    tableCellColor=[UIColor whiteColor];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:231.0/255 green:194.0/255 blue:202.0/255 alpha:1.0];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    tableCellColor=[UIColor whiteColor];
    self.tabBarController.tabBar.barTintColor =[UIColor whiteColor];
    self.tabBarController.tabBar.tintColor =[UIColor colorWithRed:231.0/255 green:194.0/255 blue:202.0/255 alpha:1.0];
    UIImage *image = [[UIImage imageNamed:@"back.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.back setImage:image forState:UIControlStateNormal];
    self.back.tintColor = tableCellColor;
    self.navView.backgroundColor=textColor;
    self.titleLabel.textColor=tableCellColor;
    self.buttonView.backgroundColor=tableCellColor;
    UIImage *image1 = [[UIImage imageNamed:@"Camera_button"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    UIImage *image2 = [[UIImage imageNamed:@"video_button-1"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    UIImage *image3 = [[UIImage imageNamed:@"cameraroll-picker-grip.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.gripView.image=image3;
    self.gripView.tintColor=textColor;
    [self.camera_btn setImage:image1 forState:UIControlStateNormal];
    [self.video_btn setImage:image2 forState:UIControlStateNormal];
    self.camera_btn.tintColor=textColor;
    self.video_btn.tintColor=textColor;
    [self.library_btn setTitleColor:textColor forState:UIControlStateNormal];
    [self.cropBtn setTitleColor:tableCellColor forState:UIControlStateNormal];
    _back.tintColor = tableCellColor;
    [_cropBtn setTitleColor:tableCellColor forState:UIControlStateNormal];
        _navView.backgroundColor=textColor;

}
-(void)green_style{
    tableCellColor=[UIColor whiteColor];
    textColor = [UIColor colorWithRed:23.0/255 green:125.0/255 blue:46.0/255 alpha:1.0];
    textColor_main=[UIColor blackColor];
    
    tableCellColor=[UIColor whiteColor];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:23.0/255 green:125.0/255 blue:46.0/255 alpha:1.0];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    tableCellColor=[UIColor whiteColor];
    self.tabBarController.tabBar.barTintColor =[UIColor whiteColor];
    self.tabBarController.tabBar.tintColor = [UIColor colorWithRed:23.0/255 green:125.0/255 blue:46.0/255 alpha:1.0];
    UIImage *image = [[UIImage imageNamed:@"back.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.back setImage:image forState:UIControlStateNormal];
    self.back.tintColor = tableCellColor;
    self.navView.backgroundColor=textColor;
    self.titleLabel.textColor=tableCellColor;
    self.buttonView.backgroundColor=tableCellColor;
    UIImage *image1 = [[UIImage imageNamed:@"Camera_button"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    UIImage *image2 = [[UIImage imageNamed:@"video_button-1"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    UIImage *image3 = [[UIImage imageNamed:@"cameraroll-picker-grip.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.gripView.image=image3;
    self.gripView.tintColor=textColor;
    [self.camera_btn setImage:image1 forState:UIControlStateNormal];
    [self.video_btn setImage:image2 forState:UIControlStateNormal];
    self.camera_btn.tintColor=textColor;
    self.video_btn.tintColor=textColor;
    [self.library_btn setTitleColor:textColor forState:UIControlStateNormal];
    [self.cropBtn setTitleColor:tableCellColor forState:UIControlStateNormal];
    _back.tintColor = tableCellColor;
    [_cropBtn setTitleColor:tableCellColor forState:UIControlStateNormal];
        _navView.backgroundColor=textColor;

}

-(void)addButtons{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    _buttonView=[[UIView alloc]initWithFrame:CGRectMake(0.0, screenHeight-40, screenWidth, 40.0)];
    [_buttonView setBackgroundColor:[UIColor whiteColor]];
    _buttonView.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    _buttonView.layer.shadowOffset = CGSizeMake(5, 5);
    _buttonView.layer.shadowOpacity = 1;
    _buttonView.layer.shadowRadius = 4.0;
    [self.view addSubview:_buttonView];
    _camera_btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_camera_btn addTarget:self
                    action:@selector(camera)
          forControlEvents:UIControlEventTouchUpInside];
    [_camera_btn setImage:[UIImage imageNamed:@"Camera_button"] forState:UIControlStateNormal];
    _camera_btn.frame = CGRectMake(25.0, screenHeight-35,  30, 30);
    [_camera_btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    _camera_btn.imageView.contentMode=UIViewContentModeScaleAspectFit;
    _camera_btn.clipsToBounds = YES;
    [self.view addSubview:_camera_btn];
    _video_btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_video_btn addTarget:self
                   action:@selector(video)
         forControlEvents:UIControlEventTouchUpInside];
    [_video_btn setImage:[UIImage imageNamed:@"video_button-1"] forState:UIControlStateNormal];
    _video_btn.frame = CGRectMake(screenWidth-55, screenHeight-35, 30, 30);
    _video_btn.imageView.contentMode=UIViewContentModeScaleAspectFit;
    _video_btn.clipsToBounds = YES;
    [_video_btn setTitleColor:[UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:0.4] forState:UIControlStateNormal];
    [self.view addSubview:_video_btn];
    
    _library_btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_library_btn addTarget:self
                   action:@selector(video)
         forControlEvents:UIControlEventTouchUpInside];
    [_library_btn setTitle:@"Библиотека" forState:UIControlStateNormal];
    _library_btn.font=[UIFont systemFontOfSize:16];
    NSLog(@"%f",self.view.center.x);
    _library_btn.frame = CGRectMake(30.0, screenHeight-35, screenWidth-65, 30);
    _library_btn.imageView.contentMode=UIViewContentModeScaleAspectFit;
    _library_btn.clipsToBounds = YES;
    [_library_btn setTitleColor:[UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    [self.view addSubview:_library_btn];

}
-(void)camera{
    [_video_btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_camera_btn setTitleColor:[UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:0.4] forState:UIControlStateNormal];
    HomeViewController*home = [[HomeViewController alloc] init];
    [self presentViewController:home animated:YES completion:nil];
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"VIDEO_OR_CAMERA"
     object:@"1"];
}
-(void)video{;
    [_camera_btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_video_btn setTitleColor:[UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:0.4] forState:UIControlStateNormal];
    HomeViewController*home = [[HomeViewController alloc] init];
    [self presentViewController:home animated:YES completion:nil];
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"VIDEO_OR_CAMERA"
     object:@"2"];
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.allPhotos count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"TWPhotoCollectionViewCell";
    
    TWPhotoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    TWPhoto *photo = [self.allPhotos objectAtIndex:indexPath.row];
    cell.imageView.image = photo.thumbnailImage;
    
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    TWPhoto *photo = [self.allPhotos objectAtIndex:indexPath.row];
    [self.imageScrollView displayImage:photo.originalImage];
    if (self.topView.frame.origin.y != 0) {
        [self tapGestureAction:nil];
    }
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
    if (velocity.y >= 2.0 && self.topView.frame.origin.y == 0) {
        [self tapGestureAction:nil];
    }
}



#pragma mark - event response

- (void)backAction {

    [self dismissViewControllerAnimated:YES completion:NULL];
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"PUSH_PICKER"
     object:nil];
    
}

- (void)cropAction {
    if (self.cropBlock) {
        self.cropBlock(self.imageScrollView.capture);
    }
    [self backAction];
}

- (void)panGestureAction:(UIPanGestureRecognizer *)panGesture {
    switch (panGesture.state)
    {
        case UIGestureRecognizerStateEnded:
        case UIGestureRecognizerStateCancelled:
        case UIGestureRecognizerStateFailed:
        {
            CGRect topFrame = self.topView.frame;
            CGFloat endOriginY = self.topView.frame.origin.y;
            if (endOriginY > beginOriginY) {
                topFrame.origin.y = (endOriginY - beginOriginY) >= 20 ? 0 : -(CGRectGetHeight(self.topView.bounds)-20-44);
            } else if (endOriginY < beginOriginY) {
                topFrame.origin.y = (beginOriginY - endOriginY) >= 20 ? -(CGRectGetHeight(self.topView.bounds)-20-44) : 0;
            }
            
            CGRect collectionFrame = self.collectionView.frame;
            collectionFrame.origin.y = CGRectGetMaxY(topFrame);
            collectionFrame.size.height = CGRectGetHeight(self.view.bounds) - CGRectGetMaxY(topFrame)-50;
            [UIView animateWithDuration:.3f animations:^{
                self.topView.frame = topFrame;
                self.collectionView.frame = collectionFrame;
            }];
            break;
        }
        case UIGestureRecognizerStateBegan:
        {
            beginOriginY = self.topView.frame.origin.y;
            break;
        }
        case UIGestureRecognizerStateChanged:
        {
            CGPoint translation = [panGesture translationInView:self.view];
            CGRect topFrame = self.topView.frame;
            topFrame.origin.y = translation.y + beginOriginY;
            
            CGRect collectionFrame = self.collectionView.frame;
            collectionFrame.origin.y = CGRectGetMaxY(topFrame);
            collectionFrame.size.height = CGRectGetHeight(self.view.bounds) - CGRectGetMaxY(topFrame)-50;
            
            if (topFrame.origin.y <= 0 && (topFrame.origin.y >= -(CGRectGetHeight(self.topView.bounds)-20-44))) {
                self.topView.frame = topFrame;
                self.collectionView.frame = collectionFrame;
            }
            
            break;
        }
        default:
            break;
    }
}

- (void)tapGestureAction:(UITapGestureRecognizer *)tapGesture {
    CGRect topFrame = self.topView.frame;
    topFrame.origin.y = topFrame.origin.y == 0 ? -(CGRectGetHeight(self.topView.bounds)-20-44) : 0;
    
    CGRect collectionFrame = self.collectionView.frame;
    collectionFrame.origin.y = CGRectGetMaxY(topFrame);
    collectionFrame.size.height = CGRectGetHeight(self.view.bounds) - CGRectGetMaxY(topFrame)-50;
    [UIView animateWithDuration:.3f animations:^{
        self.topView.frame = topFrame;
        self.collectionView.frame = collectionFrame;
    }];
}



#pragma mark - private methods

- (void)loadPhotos {
    [TWPhotoLoader loadAllPhotos:^(NSArray *photos, NSError *error) {
        if (!error) {
            self.allPhotos = [NSArray arrayWithArray:photos];
            if (self.allPhotos.count) {
                TWPhoto *firstPhoto = [self.allPhotos objectAtIndex:0];
                [self.imageScrollView displayImage:firstPhoto.originalImage];
            }
            [self.collectionView reloadData];
        } else {
            NSLog(@"Load Photos Error: %@", error);
        }
    }];
    
}



#pragma mark - getters & setters

- (UIView *)topView {
    if (_topView == nil) {
        CGFloat handleHeight = 44.0f;
        NSBundle *bundle = [NSBundle bundleForClass:[self class]];
        CGRect rect = CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), CGRectGetWidth(self.view.bounds)+handleHeight*2);
        self.topView = [[UIView alloc] initWithFrame:rect];
        self.topView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
        self.topView.backgroundColor = [UIColor whiteColor];
        self.topView.clipsToBounds = YES;
        
        rect = CGRectMake(0, 0, CGRectGetWidth(self.topView.bounds), handleHeight);
         _navView = [[UIView alloc] initWithFrame:rect];//26 29 33
        _navView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:.8f];
        [self.topView addSubview:_navView];
        
        rect = CGRectMake(0, 0, 60, CGRectGetHeight(_navView.bounds));
        _back = [UIButton buttonWithType:UIButtonTypeCustom];
        _back.frame = rect;
        [_back setImage:[UIImage imageNamed:@"back.png" inBundle:bundle compatibleWithTraitCollection:nil]
                 forState:UIControlStateNormal];
        UIImage *image = [[UIImage imageNamed:@"back.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [_back setImage:image forState:UIControlStateNormal];
        _back.tintColor = [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
        [_back addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        [_navView addSubview:_back];
        
        rect = CGRectMake((CGRectGetWidth(_navView.bounds)-100)/2, 0, 100, CGRectGetHeight(_navView.bounds));
        _titleLabel = [[UILabel alloc] initWithFrame:rect];
        _titleLabel.text = @"Фотопленка";
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.backgroundColor = [UIColor clearColor];
        _titleLabel.textColor = [UIColor blackColor];
        _titleLabel.font = [UIFont boldSystemFontOfSize:16.0f];
        [_navView addSubview:_titleLabel];
        
        rect = CGRectMake(CGRectGetWidth(_navView.bounds)-80, 0, 80, CGRectGetHeight(_navView.bounds));
        _cropBtn = [[UIButton alloc] initWithFrame:rect];
        [_cropBtn setTitle:@"Далее" forState:UIControlStateNormal];
        [_cropBtn.titleLabel setFont:[UIFont systemFontOfSize:14.0f]];
        [_cropBtn setTitleColor:[UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1] forState:UIControlStateNormal];

        [_cropBtn addTarget:self action:@selector(cropAction) forControlEvents:UIControlEventTouchUpInside];
        [_navView addSubview:_cropBtn];
        
        rect = CGRectMake(0, CGRectGetHeight(self.topView.bounds)-handleHeight, CGRectGetWidth(self.topView.bounds), handleHeight);
        _dragView = [[UIView alloc] initWithFrame:rect];
        _dragView.backgroundColor = [UIColor colorWithRed:26.0/255 green:29.0/255 blue:33.0/255 alpha:1.0];
        _dragView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
        [self.topView addSubview:_dragView];
        
        UIImage *img = [UIImage imageNamed:@"cameraroll-picker-grip.png" inBundle:bundle compatibleWithTraitCollection:nil];
        rect = CGRectMake((CGRectGetWidth(_dragView.bounds)-img.size.width)/2, (CGRectGetHeight(_dragView.bounds)-img.size.height)/2, img.size.width, img.size.height);
         _gripView = [[UIImageView alloc] initWithFrame:rect];
        _gripView.image = img;
        [_dragView addSubview:_gripView];
        
        UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureAction:)];
        [_dragView addGestureRecognizer:panGesture];
        
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureAction:)];
        [_dragView addGestureRecognizer:tapGesture];
        
        [tapGesture requireGestureRecognizerToFail:panGesture];
        rect = CGRectMake(0, handleHeight, CGRectGetWidth(self.topView.bounds), CGRectGetHeight(self.topView.bounds)-handleHeight*2);
        self.imageScrollView = [[TWImageScrollView alloc] initWithFrame:rect];
        [self.topView addSubview:self.imageScrollView];
        [self.topView sendSubviewToBack:self.imageScrollView];
        
        self.maskView = [[UIImageView alloc] initWithFrame:rect];
        
        self.maskView.image = [UIImage imageNamed:@"straighten-grid.png" inBundle:bundle compatibleWithTraitCollection:nil];
        [self.topView insertSubview:self.maskView aboveSubview:self.imageScrollView];
    }
    return _topView;
}

- (UICollectionView *)collectionView {
    if (_collectionView == nil) {
        CGFloat colum = 4.0, spacing = 2.0;
        CGFloat value = floorf((CGRectGetWidth(self.view.bounds) - (colum - 1) * spacing) / colum);
        
        UICollectionViewFlowLayout *layout  = [[UICollectionViewFlowLayout alloc] init];
        layout.itemSize                     = CGSizeMake(value, value);
        layout.sectionInset                 = UIEdgeInsetsMake(0, 0, 0, 0);
        layout.minimumInteritemSpacing      = spacing;
        layout.minimumLineSpacing           = spacing;
    
        CGRect rect = CGRectMake(0, CGRectGetMaxY(self.topView.frame), CGRectGetWidth(self.view.bounds), CGRectGetHeight(self.view.bounds)-CGRectGetHeight(self.topView.bounds)-50);
        _collectionView = [[UICollectionView alloc] initWithFrame:rect collectionViewLayout:layout];
        _collectionView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        _collectionView.backgroundColor = [UIColor whiteColor];
        
        [_collectionView registerClass:[TWPhotoCollectionViewCell class] forCellWithReuseIdentifier:@"TWPhotoCollectionViewCell"];
    }
    return _collectionView;
}

@end
