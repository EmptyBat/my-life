//
//  NewMessage.m
//  My Life
//
//  Created by Admin on 12.10.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "NewMessage.h"
#import "BEMCheckBox/BEMCheckBox.h"
@interface NewMessage ()<BEMCheckBoxDelegate>
@end

@implementation NewMessage
{
    UIColor*tableCellColor;
    UIColor*textColor;
    UIColor*textColor_main;
    NSMutableArray *message;
    
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.messageComposerView = [[MessageComposerView alloc] init];
    self.messageComposerView.delegate = self;
    self.messageComposerView.messagePlaceholder = @"Написать сообщение";
    [self.messageComposerView.sendButton setTitle:@"Отпр." forState:UIControlStateNormal];
    self.messageComposerView.sendButton.backgroundColor= [UIColor clearColor];
 /*   UIButton *cameraButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 20,20)];
    [cameraButton setImage:[UIImage imageNamed:@"Camera_button"] forState:UIControlStateNormal];
    [cameraButton addTarget:self action:@selector(cameraClicked:) forControlEvents:UIControlEventTouchUpInside];
    [cameraButton setContentMode:UIViewContentModeScaleAspectFit];
    [self.messageComposerView configureWithAccessory:cameraButton];*/
    [self.view addSubview:self.messageComposerView];
    self.messageComposerView.hidden=true;
    message  = [[NSMutableArray alloc] init];
    self.tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.tableView.bounds.size.width, 10.01f)];
           _fake_btn_1.layer.cornerRadius=8;
           _fake_btn_2.layer.cornerRadius=8;
    NSString *theme = [[NSUserDefaults standardUserDefaults] stringForKey:@"theme"];
    if ([theme isEqualToString:@"black_diamond"]){
        [self black_diamond_style];
    }
    else  if ([theme isEqualToString:@"sea_blue"]){
        [self sea_blue_style];
    }
    else  if ([theme isEqualToString:@"soft_pink"]){
        [self soft_pink_style];
    }
    else  if ([theme isEqualToString:@"green"]){
        [self green_style];
    }
    else {
        [self black_pearl_style];
    }
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(THEME:)
                                                 name:@"THEME" object:nil];
}
- (void)THEME:(NSNotification *)notice{
    NSString *theme = [[NSUserDefaults standardUserDefaults] stringForKey:@"theme"];
    if ([theme isEqualToString:@"black_diamond"]){
        [self black_diamond_style];
    }
    else  if ([theme isEqualToString:@"sea_blue"]){
        [self sea_blue_style];
    }
    else  if ([theme isEqualToString:@"soft_pink"]){
        [self soft_pink_style];
    }
    else  if ([theme isEqualToString:@"green"]){
        [self green_style];
    }
    else {
        [self black_pearl_style];
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#warning Incomplete implementation, return the number of sections
    return 6;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete implementation, return the number of rows
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    UILabel*name=(UILabel*) [cell viewWithTag:1];
    UILabel*text=(UILabel*) [cell viewWithTag:3];
    BEMCheckBox*CheckBox=(BEMCheckBox*) [cell viewWithTag:15];
    CheckBox.onAnimationType = BEMAnimationTypeBounce;
    CheckBox.offAnimationType = BEMAnimationTypeBounce;
    CheckBox.boxType =BEMBoxTypeCircle;
    CheckBox.tintColor = textColor;
    CheckBox.onTintColor = textColor;
    CheckBox.lineWidth = 1.0f;
    CheckBox.onCheckColor = tableCellColor;
    CheckBox.onFillColor = textColor;
    CheckBox.delegate=self;
    name.textColor=textColor_main;
    text.textColor=textColor;

    return cell;
    
    
}
- (void)didTapCheckBox:(BEMCheckBox *)checkBox{ NSLog(@"you touched: %d,%ld",checkBox.on,(long)checkBox.tag);
    if (checkBox.on){
    [message addObject:@"Allu Kaazmucha"];
    [self.Collection reloadData];
    [self.view layoutIfNeeded];
    [self.Collection scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:message.count+1 inSection:0] atScrollPosition:UICollectionViewScrollPositionNone animated:YES];
    self.messageComposerView.hidden=false;
    [self.messageComposerView.messageTextView becomeFirstResponder];
}
    else {
        [message removeObject:@"Allu Kaazmucha"];
        [self.Collection reloadData];
        [self.view layoutIfNeeded];
        [self.Collection scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:message.count+1 inSection:0] atScrollPosition:UICollectionViewScrollPositionNone animated:YES];
    }
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    [cell setBackgroundColor:tableCellColor];
    [cell.contentView setBackgroundColor:tableCellColor];
    if (tableCellColor==nil){
        [cell setBackgroundColor:[UIColor whiteColor]];
        [cell.contentView setBackgroundColor:[UIColor whiteColor]];
    }
}
- (void)didSelectItemAtIndex:(NSInteger)index
{
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 67;
}
- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    
    return message.count+2;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [message addObject:@"Allu Kaazmucha"];
    [self.Collection reloadData];
    [self.view layoutIfNeeded];
    [self.Collection scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:message.count+1 inSection:0] atScrollPosition:UICollectionViewScrollPositionNone animated:YES];
}
- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    return 1;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row==0){
        UICollectionViewCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"1" forIndexPath:indexPath];
        cell.layer.borderWidth=1.0f;
        cell.layer.borderColor=tableCellColor.CGColor;
        [cell layoutIfNeeded];
        UILabel*search=(UILabel*) [cell viewWithTag:1];
        cell.contentView.backgroundColor=tableCellColor;
        search.textColor=textColor_main;
           return cell;
        }
    else  if (indexPath.row==1){
        UICollectionViewCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"2" forIndexPath:indexPath];
        cell.layer.borderWidth=1.0f;
           cell.contentView.backgroundColor=tableCellColor;
        UITextField*search=(UITextField*) [cell viewWithTag:1];
        cell.layer.borderColor=tableCellColor.CGColor;
        [search setValue:textColor_main forKeyPath:@"_placeholderLabel.textColor"];
        search.textColor=textColor_main;
        search.backgroundColor=tableCellColor;
        [cell layoutIfNeeded];
            return cell;
    }
    else{
        UICollectionViewCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"3" forIndexPath:indexPath];
        cell.layer.borderWidth=1.0f;
        cell.layer.borderColor=tableCellColor.CGColor;
           cell.contentView.backgroundColor=tableCellColor;
        UIButton*text=(UIButton*) [cell viewWithTag:1];
        UIImageView*x=(UIImageView*) [cell viewWithTag:2];
        UIImage *image = [[UIImage imageNamed:@"X"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        x.image=image;
        x.tintColor = tableCellColor;
        text.backgroundColor=textColor;
        [text setTitleColor:tableCellColor forState:UIControlStateNormal];
        text.titleLabel.numberOfLines = 1;
        text.titleLabel.adjustsFontSizeToFitWidth = YES;
        text.titleLabel.lineBreakMode = NSLineBreakByClipping;
        text.titleEdgeInsets = UIEdgeInsetsMake(0.0f, 5.0f, 0.0f, 0.0f);
        text.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
       [text setTitle:message[indexPath.row-2] forState:UIControlStateNormal];
        text.layer.cornerRadius=8;
        [cell layoutIfNeeded];
            return cell;
}

}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row==0){
        return CGSizeMake(43, 29);
    }
    else if (indexPath.row ==1){
        return CGSizeMake(57, 29);
        
        }
    else {
        UIFont * font =[UIFont systemFontOfSize:14];
        CGSize stringSize = [message[indexPath.row-2] sizeWithFont:font];
        CGFloat width = stringSize.width;
       return CGSizeMake(width+35, 29);
    }
    }
-(void)black_pearl_style{
    tableCellColor=[UIColor whiteColor];
    textColor = [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleDefault;
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1.0];;
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor blackColor]};
    self.tableView.backgroundColor =[UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1.0];
    UIImage *image = [[UIImage imageNamed:@"YellowBack"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.back setImage:image forState:UIControlStateNormal];
    self.back.tintColor = textColor;
    self.next.tintColor = textColor;
    textColor_main=[UIColor blackColor];
    self.fake_btn_1.backgroundColor=textColor;
    self.fake_btn_2.backgroundColor=textColor;
    [self.fake_btn_1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.fake_btn_2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.tabBarController.tabBar.barTintColor = [UIColor whiteColor];
    self.tabBarController.tabBar.tintColor = textColor;
    textColor_main=[UIColor blackColor];
    [_title_btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    self.navigationController.navigationBar.barTintColor =[UIColor whiteColor];
    self.view.backgroundColor=[UIColor whiteColor];
    self.whom.textColor=[UIColor blackColor];
    self.Collection.backgroundColor=tableCellColor;
    [self.tableView reloadData];
      [self.Collection reloadData];
}
-(void)black_diamond_style{
    tableCellColor=[UIColor blackColor];
    textColor = [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleBlackOpaque;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor =[UIColor blackColor];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    self.tableView.backgroundColor =[UIColor colorWithRed:23.0/255 green:23.0/255 blue:23.0/255 alpha:1.0];
    UIImage *image = [[UIImage imageNamed:@"YellowBack"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.back setImage:image forState:UIControlStateNormal];
    self.back.tintColor = textColor;
    self.next.tintColor = textColor;
    textColor_main=[UIColor blackColor];
    self.fake_btn_1.backgroundColor=textColor;
    self.fake_btn_2.backgroundColor=textColor;
    [self.fake_btn_1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.fake_btn_2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];

    self.tabBarController.tabBar.barTintColor = [UIColor blackColor];
    self.tabBarController.tabBar.tintColor = textColor;
    textColor_main=[UIColor whiteColor];
    [_title_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.navigationController.navigationBar.barTintColor =[UIColor blackColor];
    self.view.backgroundColor=[UIColor blackColor];
    self.whom.textColor=[UIColor whiteColor];
    self.Collection.backgroundColor=tableCellColor;
    [self.tableView reloadData];
    [self.Collection reloadData];
}
-(void)sea_blue_style{
    tableCellColor=[UIColor whiteColor];
    textColor = [UIColor colorWithRed:2.0/255 green:35.0/255 blue:143.0/255 alpha:1.0];
    textColor_main=[UIColor blackColor];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleDefault;
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1.0];;
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor blackColor]};
    self.tableView.backgroundColor =[UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1.0];
    UIImage *image = [[UIImage imageNamed:@"YellowBack"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.back setImage:image forState:UIControlStateNormal];
    textColor_main=[UIColor blackColor];
    self.back.tintColor = [UIColor whiteColor];
    self.next.tintColor = [UIColor whiteColor];
    self.fake_btn_1.backgroundColor=textColor;
    self.fake_btn_2.backgroundColor=textColor;
    [self.fake_btn_1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.fake_btn_2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.tabBarController.tabBar.tintColor = textColor;
    [_title_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.navigationController.navigationBar.barTintColor =textColor;
    self.view.backgroundColor=[UIColor whiteColor];
    self.whom.textColor=[UIColor blackColor];
    self.Collection.backgroundColor=tableCellColor;
    [self.tableView reloadData];
      [self.Collection reloadData];
}
-(void)soft_pink_style{
     tableCellColor=[UIColor whiteColor];
    textColor = [UIColor colorWithRed:231.0/255 green:194.0/255 blue:202.0/255 alpha:1.0];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleDefault;
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1.0];;
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor blackColor]};
    self.tableView.backgroundColor =[UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1.0];
    UIImage *image = [[UIImage imageNamed:@"YellowBack"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.back setImage:image forState:UIControlStateNormal];
    textColor_main=[UIColor blackColor];
    self.back.tintColor = [UIColor whiteColor];
    self.next.tintColor = [UIColor whiteColor];
    self.fake_btn_1.backgroundColor=textColor;
    self.fake_btn_2.backgroundColor=textColor;
    [self.fake_btn_1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.fake_btn_2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.tabBarController.tabBar.barTintColor = [UIColor whiteColor];
    self.tabBarController.tabBar.tintColor = textColor;
    [_title_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.navigationController.navigationBar.barTintColor =textColor;
    self.view.backgroundColor=[UIColor whiteColor];
    self.whom.textColor=[UIColor blackColor];
    self.Collection.backgroundColor=tableCellColor;
    [self.tableView reloadData];
      [self.Collection reloadData];
}
-(void)green_style{
     tableCellColor=[UIColor whiteColor];
    textColor = [UIColor colorWithRed:23.0/255 green:125.0/255 blue:46.0/255 alpha:1.0];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleDefault;
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1.0];;
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor blackColor]};
    self.tableView.backgroundColor =[UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1.0];
    UIImage *image = [[UIImage imageNamed:@"YellowBack"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.back setImage:image forState:UIControlStateNormal];
    textColor_main=[UIColor blackColor];
    self.back.tintColor = [UIColor whiteColor];
    self.next.tintColor = [UIColor whiteColor];
    self.fake_btn_1.backgroundColor=textColor;
    self.fake_btn_2.backgroundColor=textColor;
    [self.fake_btn_1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.fake_btn_2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.tabBarController.tabBar.barTintColor = [UIColor whiteColor];
    self.tabBarController.tabBar.tintColor = textColor;
    [_title_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.navigationController.navigationBar.barTintColor =textColor;
    self.view.backgroundColor=[UIColor whiteColor];
    self.whom.textColor=[UIColor blackColor];
    self.Collection.backgroundColor=tableCellColor;
    [self.tableView reloadData];
      [self.Collection reloadData];
}
- (IBAction)back:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];

}
@end
