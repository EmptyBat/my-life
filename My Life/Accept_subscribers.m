//
//  Accept_subscribers.m
//  My Life
//
//  Created by Admin on 28.09.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "Accept_subscribers.h"

@interface Accept_subscribers ()
{
    UIColor*tableCellColor;
    UIColor*textColor;
    UIColor*textColor_main;
}
@end

@implementation Accept_subscribers

- (void)viewDidLoad {
    [super viewDidLoad];
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    self.tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.tableView.bounds.size.width, 10.01f)];
    NSString *theme = [[NSUserDefaults standardUserDefaults] stringForKey:@"theme"];
    if ([theme isEqualToString:@"black_diamond"]){
        [self black_diamond_style];
    }
    else  if ([theme isEqualToString:@"sea_blue"]){
        [self sea_blue_style];
    }
    else  if ([theme isEqualToString:@"soft_pink"]){
        [self soft_pink_style];
    }
    else  if ([theme isEqualToString:@"green"]){
        [self green_style];
    }
    else {
        [self black_pearl_style];
    }
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(THEME:)
                                                 name:@"THEME" object:nil];
}
- (void)THEME:(NSNotification *)notice{
        NSString *theme = [[NSUserDefaults standardUserDefaults] stringForKey:@"theme"];
        if ([theme isEqualToString:@"black_diamond"]){
            [self black_diamond_style];
        }
        else  if ([theme isEqualToString:@"sea_blue"]){
            [self sea_blue_style];
        }
        else  if ([theme isEqualToString:@"soft_pink"]){
            [self soft_pink_style];
        }
        else  if ([theme isEqualToString:@"green"]){
            [self green_style];
        }
        else {
            [self black_pearl_style];
        }
        
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#warning Incomplete implementation, return the number of sections
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete implementation, return the number of rows
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    UILabel*name=(UILabel*) [cell viewWithTag:1];
    UILabel*place=(UILabel*) [cell viewWithTag:3];
    UIImageView*x=(UIImageView*) [cell viewWithTag:4];
     UIButton*accept=(UIButton*) [cell viewWithTag:5];
    name.textColor=textColor_main;
    place.textColor=textColor;
    UIImage *image1 = [[UIImage imageNamed:@"X"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    UIImage *image2 = [[UIImage imageNamed:@"accept"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    x.image=image1;
    x.tintColor = textColor;
    [accept setImage:image2 forState:UIControlStateNormal];
    accept.tintColor = textColor;
    return cell;
  
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    [cell setBackgroundColor:tableCellColor];
    [cell.contentView setBackgroundColor:tableCellColor];
    if (tableCellColor==nil){
        [cell setBackgroundColor:[UIColor whiteColor]];
        [cell.contentView setBackgroundColor:[UIColor whiteColor]];
    }
}

-(void)black_pearl_style{
    tableCellColor=[UIColor whiteColor];
    textColor = [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleDefault;
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1.0];;
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor blackColor]};
    self.tabBarController.tabBar.barTintColor = [UIColor whiteColor];
    self.tabBarController.tabBar.tintColor = textColor;
    textColor_main=[UIColor blackColor];
    [_title_btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    UIImage *image = [[UIImage imageNamed:@"YellowBack"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.back setImage:image forState:UIControlStateNormal];
    self.back.tintColor = textColor;
    UIImage *image4 = [[UIImage imageNamed:@"tab_2"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.search setImage:image4 forState:UIControlStateNormal];
    self.search.tintColor = textColor;
      self.search_view.backgroundColor=tableCellColor;
    self.tableView.backgroundColor =[UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1.0];
       [self.search_field setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
     self.search_field.textColor=[UIColor blackColor];
       self.search_field.backgroundColor=tableCellColor;
    [self.tableView reloadData];
}
-(void)black_diamond_style{
    tableCellColor=[UIColor blackColor];
    textColor = [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleBlackOpaque;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor =[UIColor blackColor];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    
    self.tabBarController.tabBar.barTintColor = [UIColor blackColor];
    self.tabBarController.tabBar.tintColor = textColor;
    textColor_main=[UIColor whiteColor];
    [_title_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    UIImage *image = [[UIImage imageNamed:@"YellowBack"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.back setImage:image forState:UIControlStateNormal];
    self.back.tintColor = textColor;
    UIImage *image4 = [[UIImage imageNamed:@"tab_2"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.search setImage:image4 forState:UIControlStateNormal];
    self.search.tintColor = textColor;
  self.search_view.backgroundColor=tableCellColor;
    self.tableView.backgroundColor =tableCellColor;
    [self.search_field setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    self.search_field.textColor=[UIColor whiteColor];
   self.search_field.backgroundColor=tableCellColor;
    [self.tableView reloadData];
}
-(void)sea_blue_style{
    tableCellColor=[UIColor whiteColor];
    textColor = [UIColor colorWithRed:2.0/255 green:35.0/255 blue:143.0/255 alpha:1.0];
    textColor_main=[UIColor blackColor];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleDefault;
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1.0];;
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor blackColor]};
    self.tabBarController.tabBar.barTintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor =textColor;
    self.tabBarController.tabBar.tintColor = textColor;
    [_title_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    UIImage *image = [[UIImage imageNamed:@"YellowBack"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.back setImage:image forState:UIControlStateNormal];
    self.back.tintColor = [UIColor whiteColor];
    UIImage *image4 = [[UIImage imageNamed:@"tab_2"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.search setImage:image4 forState:UIControlStateNormal];
    self.search.tintColor = textColor;
    self.search_view.backgroundColor=tableCellColor;
    self.tableView.backgroundColor =[UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1.0];
    [self.search_field setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
    self.search_field.textColor=[UIColor blackColor];
    self.search_field.backgroundColor=tableCellColor;
    [self.tableView reloadData];
}
-(void)soft_pink_style{
    tableCellColor=[UIColor whiteColor];
    textColor = [UIColor colorWithRed:231.0/255 green:194.0/255 blue:202.0/255 alpha:1.0];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleDefault;
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1.0];;
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor blackColor]};
    self.tabBarController.tabBar.barTintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor =textColor;
    self.tabBarController.tabBar.tintColor = textColor;
    textColor_main=[UIColor blackColor];
    [_title_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    UIImage *image = [[UIImage imageNamed:@"YellowBack"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.back setImage:image forState:UIControlStateNormal];
    self.back.tintColor = [UIColor whiteColor];
    UIImage *image4 = [[UIImage imageNamed:@"tab_2"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.search setImage:image4 forState:UIControlStateNormal];
    self.search.tintColor = textColor;
    self.search_view.backgroundColor=tableCellColor;
    self.tableView.backgroundColor =[UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1.0];
    [self.search_field setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
    self.search_field.textColor=[UIColor blackColor];
    self.search_field.backgroundColor=tableCellColor;
    [self.tableView reloadData];
}
-(void)green_style{
    tableCellColor=[UIColor whiteColor];
    textColor = [UIColor colorWithRed:23.0/255 green:125.0/255 blue:46.0/255 alpha:1.0];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleDefault;
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1.0];;
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor blackColor]};
    self.tabBarController.tabBar.barTintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor =textColor;
    self.tabBarController.tabBar.tintColor = textColor;
    textColor_main=[UIColor blackColor];
    [_title_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    UIImage *image = [[UIImage imageNamed:@"YellowBack"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.back setImage:image forState:UIControlStateNormal];
    self.back.tintColor = [UIColor whiteColor];
    UIImage *image4 = [[UIImage imageNamed:@"tab_2"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.search setImage:image4 forState:UIControlStateNormal];
    self.search.tintColor = textColor;
    self.search_view.backgroundColor=tableCellColor;
    self.tableView.backgroundColor =[UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1.0];
    [self.search_field setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
    self.search_field.textColor=[UIColor blackColor];
    self.search_field.backgroundColor=tableCellColor;
    [self.tableView reloadData];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)back:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
}
@end
