//
//  Push_notification_settings.h
//  My Life
//
//  Created by Admin on 23.10.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Push_notification_settings : UITableViewController
@property (strong, nonatomic) IBOutlet UIButton *back;
- (IBAction)back:(id)sender;
- (IBAction)done:(id)sender;
@property (strong, nonatomic) IBOutlet UIImageView *img_1;
@property (strong, nonatomic) IBOutlet UILabel *lbl_1;
@property (strong, nonatomic) IBOutlet UIImageView *img_2;
@property (strong, nonatomic) IBOutlet UILabel *lbl_2;
@property (strong, nonatomic) IBOutlet UIImageView *img_3;
@property (strong, nonatomic) IBOutlet UILabel *lbl_3;
@property (strong, nonatomic) IBOutlet UIButton *done;
@property (strong, nonatomic) IBOutlet UIButton *title_btn;
@property (strong, nonatomic) IBOutlet UIImageView *img_4;
@property (strong, nonatomic) IBOutlet UILabel *lbl_4;
@property (strong, nonatomic) IBOutlet UIImageView *img_5;
@property (strong, nonatomic) IBOutlet UILabel *lbl_5;
@property (strong, nonatomic) IBOutlet UIImageView *img_6;
@property (strong, nonatomic) IBOutlet UILabel *lbl_6;

@end
