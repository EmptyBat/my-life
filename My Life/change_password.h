//
//  change_password.h
//  My Life
//
//  Created by Admin on 05.10.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface change_password : UITableViewController
- (IBAction)back:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *title_btn;
@property (strong, nonatomic) IBOutlet UIButton *back;
@property (strong, nonatomic) IBOutlet UIButton *done;
- (IBAction)done:(id)sender;
@property (strong, nonatomic) IBOutlet UIImageView *lock_1;
@property (strong, nonatomic) IBOutlet UIImageView *lock_2;
@property (strong, nonatomic) IBOutlet UIImageView *lock_3;
@property (strong, nonatomic) IBOutlet UITextField *current_password;
@property (strong, nonatomic) IBOutlet UITextField *field_new_password;
@property (strong, nonatomic) IBOutlet UITextField *return_new_password;

@end
