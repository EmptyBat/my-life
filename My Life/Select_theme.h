//
//  Select_theme.h
//  My Life
//
//  Created by Admin on 05.10.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Select_theme : UITableViewController
- (IBAction)back:(id)sender;
@property (strong, nonatomic) IBOutlet UISwitch *black_pearl;
- (IBAction)black_pearl:(id)sender;
@property (strong, nonatomic) IBOutlet UISwitch *black_diamond;
- (IBAction)black_diamond:(id)sender;
@property (strong, nonatomic) IBOutlet UISwitch *sea_blue;
- (IBAction)sea_blue:(id)sender;
- (IBAction)soft_pink:(id)sender;
@property (strong, nonatomic) IBOutlet UISwitch *soft_pink;
@property (strong, nonatomic) IBOutlet UISwitch *green;
- (IBAction)green:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *done;
@property (strong, nonatomic) IBOutlet UIButton *back;
@property (strong, nonatomic) IBOutlet UILabel *black_pearl_label;
@property (strong, nonatomic) IBOutlet UILabel *black_diamon_label;
@property (strong, nonatomic) IBOutlet UILabel *sea_blue_label;
@property (strong, nonatomic) IBOutlet UILabel *soft_pink_label;

@property (strong, nonatomic) IBOutlet UILabel *green_label;
@property (strong, nonatomic) IBOutlet UITableView *tableview;
@property (strong, nonatomic) IBOutlet UIButton *title_btn;


@end
