//
//  SettingTab.m
//  My Life
//
//  Created by Admin on 25.09.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "SettingTab.h"
@interface SettingTab ()
{
    UIColor*tableCellColor;
}
@end

@implementation SettingTab

- (void)viewDidLoad {
    [super viewDidLoad];
    NSString *theme = [[NSUserDefaults standardUserDefaults] stringForKey:@"theme"];
    if ([theme isEqualToString:@"black_diamond"]){
        [self black_diamond_style];
    }
    else  if ([theme isEqualToString:@"sea_blue"]){
        [self sea_blue_style];
    }
    else  if ([theme isEqualToString:@"soft_pink"]){
        [self soft_pink_style];
    }
    else  if ([theme isEqualToString:@"green"]){
        [self green_style];
    }
    else {
        [self black_pearl_style];
    }
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(THEME:)
                                                 name:@"THEME" object:nil];
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    NSString *theme = [[NSUserDefaults standardUserDefaults] stringForKey:@"theme"];
    if ([theme isEqualToString:@"black_diamond"]){
        [self black_diamond_style];
    }
    else  if ([theme isEqualToString:@"sea_blue"]){
        [self sea_blue_style];
    }
    else  if ([theme isEqualToString:@"soft_pink"]){
        [self soft_pink_style];
    }
    else  if ([theme isEqualToString:@"green"]){
        [self green_style];
    }
    else {
        [self black_pearl_style];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)THEME:(NSNotification *)notice{
    NSString *theme = [[NSUserDefaults standardUserDefaults] stringForKey:@"theme"];
    if ([theme isEqualToString:@"black_diamond"]){
        [self black_diamond_style];
    }
    else  if ([theme isEqualToString:@"sea_blue"]){
        [self sea_blue_style];
    }
    else  if ([theme isEqualToString:@"soft_pink"]){
        [self soft_pink_style];
    }
    else  if ([theme isEqualToString:@"green"]){
        [self green_style];
    }
    else {
        [self black_pearl_style];
    }
    
}

#pragma mark - Table view data source

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    [cell setBackgroundColor:tableCellColor];
    [cell.contentView setBackgroundColor:tableCellColor];
    if (tableCellColor==nil){
        [cell setBackgroundColor:[UIColor whiteColor]];
        [cell.contentView setBackgroundColor:[UIColor whiteColor]];
    }
}
-(void)black_pearl_style{
    [_title_btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    tableCellColor=[UIColor whiteColor];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleDefault;
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1.0];;
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor blackColor]};
    self.tableView.backgroundColor =[UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1.0];
    UIImage *image = [[UIImage imageNamed:@"YellowBack"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.back setImage:image forState:UIControlStateNormal];
    self.back.tintColor = [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    _close_account.tintColor =[UIColor whiteColor];
    _close_account.onTintColor =[UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    _save_started_switch.tintColor =[UIColor whiteColor];
    _save_started_switch.onTintColor =[UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    _facebook_label.textColor=[UIColor blackColor];
    _vk_label.textColor=[UIColor blackColor];
    _header.textColor=[UIColor blackColor];
    _theme.textColor=[UIColor blackColor];
    _edit.textColor=[UIColor blackColor];
    _change_password.textColor=[UIColor blackColor];
    _like_it.textColor=[UIColor blackColor];
    _close_account_label.textColor=[UIColor blackColor];
    _close_account_label2.textColor=[UIColor blackColor];
    _joined_account.textColor=[UIColor blackColor];
    _select_language.textColor=[UIColor blackColor];
    _use_data.textColor=[UIColor blackColor];
    _push_notification.textColor=[UIColor blackColor];
    _save_started_photo.textColor=[UIColor blackColor];
    _call_center.textColor=[UIColor blackColor];
    _report_problem.textColor=[UIColor blackColor];
    _advertising.textColor=[UIColor blackColor];
    _blog.textColor=[UIColor blackColor];
    _privacy_policy.textColor=[UIColor blackColor];
    _conditions.textColor=[UIColor blackColor];
     _clean_history.textColor=[UIColor blackColor];
    _add_acount.textColor=[UIColor blackColor];
    _exit.textColor=[UIColor blackColor];
     _open_source_img_library.textColor=[UIColor blackColor];
    self.tabBarController.tabBar.barTintColor = [UIColor whiteColor];
    self.tabBarController.tabBar.tintColor = [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    UIImage *image1 = [[UIImage imageNamed:@"facebook"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    _facebook.image=image1;
    _facebook.tintColor=[UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    UIImage *image2 = [[UIImage imageNamed:@"vk"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    _vk.image=image2;
    _vk.tintColor=[UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    [self.tableView reloadData];
}
-(void)black_diamond_style{
    [_title_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleBlackOpaque;
    tableCellColor=[UIColor blackColor];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor =[UIColor blackColor];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    self.tableView.backgroundColor =[UIColor colorWithRed:23.0/255 green:23.0/255 blue:23.0/255 alpha:1.0];
    UIImage *image = [[UIImage imageNamed:@"YellowBack"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.back setImage:image forState:UIControlStateNormal];
    self.back.tintColor = [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    _close_account.tintColor =[UIColor whiteColor];
    _close_account.onTintColor =[UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    _save_started_switch.tintColor =[UIColor whiteColor];
    _save_started_switch.onTintColor =[UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    _facebook_label.textColor=[UIColor whiteColor];
    _vk_label.textColor=[UIColor whiteColor];
    _header.textColor=[UIColor whiteColor];
    _theme.textColor=[UIColor whiteColor];
    _edit.textColor=[UIColor whiteColor];
    _change_password.textColor=[UIColor whiteColor];
    _like_it.textColor=[UIColor whiteColor];
    _close_account_label.textColor=[UIColor whiteColor];
    _close_account_label2.textColor=[UIColor whiteColor];
    _joined_account.textColor=[UIColor whiteColor];
    _select_language.textColor=[UIColor whiteColor];
    _use_data.textColor=[UIColor whiteColor];
    _push_notification.textColor=[UIColor whiteColor];
    _save_started_photo.textColor=[UIColor whiteColor];
    _call_center.textColor=[UIColor whiteColor];
    _report_problem.textColor=[UIColor whiteColor];
    _advertising.textColor=[UIColor whiteColor];
    _blog.textColor=[UIColor whiteColor];
    _privacy_policy.textColor=[UIColor whiteColor];
    _conditions.textColor=[UIColor whiteColor];
    _clean_history.textColor=[UIColor whiteColor];
    _add_acount.textColor=[UIColor whiteColor];
    _exit.textColor=[UIColor whiteColor];
     _open_source_img_library.textColor=[UIColor whiteColor];
    self.tabBarController.tabBar.barTintColor = [UIColor blackColor];
    self.tabBarController.tabBar.tintColor = [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    UIImage *image1 = [[UIImage imageNamed:@"facebook"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    _facebook.image=image1;
    _facebook.tintColor=[UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    UIImage *image2 = [[UIImage imageNamed:@"vk"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    _vk.image=image2;
    _vk.tintColor=[UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    [self.tableView reloadData];
    
}
-(void)sea_blue_style{
    [_title_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:2.0/255 green:35.0/255 blue:143.0/255 alpha:1.0];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    self.tableView.backgroundColor =[UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1.0];
    UIImage *image = [[UIImage imageNamed:@"YellowBack"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.back setImage:image forState:UIControlStateNormal];
    self.back.tintColor = [UIColor whiteColor];
    tableCellColor=[UIColor whiteColor];
    self.tabBarController.tabBar.barTintColor =[UIColor whiteColor];
    self.tabBarController.tabBar.tintColor = [UIColor colorWithRed:2.0/255 green:35.0/255 blue:143.0/255 alpha:1.0];
    _close_account.tintColor =[UIColor whiteColor];
    _close_account.onTintColor =[UIColor colorWithRed:2.0/255 green:35.0/255 blue:143.0/255 alpha:1.0];
    _save_started_switch.tintColor =[UIColor whiteColor];
    _save_started_switch.onTintColor =[UIColor colorWithRed:2.0/255 green:35.0/255 blue:143.0/255 alpha:1.0];
    _facebook_label.textColor=[UIColor blackColor];
    _vk_label.textColor=[UIColor blackColor];
    _header.textColor=[UIColor blackColor];
    _theme.textColor=[UIColor blackColor];
    _edit.textColor=[UIColor blackColor];
    _change_password.textColor=[UIColor blackColor];
    _like_it.textColor=[UIColor blackColor];
    _close_account_label.textColor=[UIColor blackColor];
    _close_account_label2.textColor=[UIColor blackColor];
    _joined_account.textColor=[UIColor blackColor];
    _select_language.textColor=[UIColor blackColor];
    _use_data.textColor=[UIColor blackColor];
    _push_notification.textColor=[UIColor blackColor];
    _save_started_photo.textColor=[UIColor blackColor];
    _call_center.textColor=[UIColor blackColor];
    _report_problem.textColor=[UIColor blackColor];
    _advertising.textColor=[UIColor blackColor];
    _blog.textColor=[UIColor blackColor];
    _privacy_policy.textColor=[UIColor blackColor];
    _conditions.textColor=[UIColor blackColor];
    _clean_history.textColor=[UIColor blackColor];
    _add_acount.textColor=[UIColor blackColor];
    _exit.textColor=[UIColor blackColor];
     _open_source_img_library.textColor=[UIColor blackColor];
    UIImage *image1 = [[UIImage imageNamed:@"facebook"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    _facebook.image=image1;
    _facebook.tintColor=[UIColor colorWithRed:2.0/255 green:35.0/255 blue:143.0/255 alpha:1.0];
    UIImage *image2 = [[UIImage imageNamed:@"vk"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    _vk.image=image2;
    _vk.tintColor=[UIColor colorWithRed:2.0/255 green:35.0/255 blue:143.0/255 alpha:1.0];
        [self.tableView reloadData];
}
-(void)soft_pink_style{
    [_title_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:231.0/255 green:194.0/255 blue:202.0/255 alpha:1.0];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    self.tableView.backgroundColor =[UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1.0];
    UIImage *image = [[UIImage imageNamed:@"YellowBack"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.back setImage:image forState:UIControlStateNormal];
    self.back.tintColor = [UIColor whiteColor];
    tableCellColor=[UIColor whiteColor];
    self.tabBarController.tabBar.barTintColor =[UIColor whiteColor];
    self.tabBarController.tabBar.tintColor =[UIColor colorWithRed:231.0/255 green:194.0/255 blue:202.0/255 alpha:1.0];
    _close_account.tintColor =[UIColor whiteColor];
    _close_account.onTintColor =[UIColor colorWithRed:231.0/255 green:194.0/255 blue:202.0/255 alpha:1.0];
    _save_started_switch.tintColor =[UIColor whiteColor];
    _save_started_switch.onTintColor =[UIColor colorWithRed:231.0/255 green:194.0/255 blue:202.0/255 alpha:1.0];
    _facebook_label.textColor=[UIColor blackColor];
    _vk_label.textColor=[UIColor blackColor];
    _header.textColor=[UIColor blackColor];
    _theme.textColor=[UIColor blackColor];
    _edit.textColor=[UIColor blackColor];
    _change_password.textColor=[UIColor blackColor];
    _like_it.textColor=[UIColor blackColor];
    _close_account_label.textColor=[UIColor blackColor];
    _close_account_label2.textColor=[UIColor blackColor];
    _joined_account.textColor=[UIColor blackColor];
    _select_language.textColor=[UIColor blackColor];
    _use_data.textColor=[UIColor blackColor];
    _push_notification.textColor=[UIColor blackColor];
    _save_started_photo.textColor=[UIColor blackColor];
    _call_center.textColor=[UIColor blackColor];
    _report_problem.textColor=[UIColor blackColor];
    _advertising.textColor=[UIColor blackColor];
    _blog.textColor=[UIColor blackColor];
    _privacy_policy.textColor=[UIColor blackColor];
    _conditions.textColor=[UIColor blackColor];
    _clean_history.textColor=[UIColor blackColor];
    _add_acount.textColor=[UIColor blackColor];
    _exit.textColor=[UIColor blackColor];
     _open_source_img_library.textColor=[UIColor blackColor];
    UIImage *image1 = [[UIImage imageNamed:@"facebook"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    _facebook.image=image1;
    _facebook.tintColor=[UIColor colorWithRed:231.0/255 green:194.0/255 blue:202.0/255 alpha:1.0];
    UIImage *image2 = [[UIImage imageNamed:@"vk"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    _vk.image=image2;
    _vk.tintColor=[UIColor colorWithRed:231.0/255 green:194.0/255 blue:202.0/255 alpha:1.0];
        [self.tableView reloadData];
    
}
-(void)green_style{
    [_title_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:23.0/255 green:125.0/255 blue:46.0/255 alpha:1.0];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    self.tableView.backgroundColor =[UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1.0];
    UIImage *image = [[UIImage imageNamed:@"YellowBack"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.back setImage:image forState:UIControlStateNormal];
    self.back.tintColor = [UIColor whiteColor];
    tableCellColor=[UIColor whiteColor];
    self.tabBarController.tabBar.barTintColor =[UIColor whiteColor];
    self.tabBarController.tabBar.tintColor = [UIColor colorWithRed:23.0/255 green:125.0/255 blue:46.0/255 alpha:1.0];
    _close_account.tintColor =[UIColor whiteColor];
    _close_account.onTintColor =[UIColor colorWithRed:23.0/255 green:125.0/255 blue:46.0/255 alpha:1.0];
    _save_started_switch.tintColor =[UIColor whiteColor];
    _save_started_switch.onTintColor =[UIColor colorWithRed:23.0/255 green:125.0/255 blue:46.0/255 alpha:1.0];
    _facebook_label.textColor=[UIColor blackColor];
    _vk_label.textColor=[UIColor blackColor];
    _header.textColor=[UIColor blackColor];
    _theme.textColor=[UIColor blackColor];
    _edit.textColor=[UIColor blackColor];
    _change_password.textColor=[UIColor blackColor];
    _like_it.textColor=[UIColor blackColor];
    _close_account_label.textColor=[UIColor blackColor];
    _close_account_label2.textColor=[UIColor blackColor];
    _joined_account.textColor=[UIColor blackColor];
    _select_language.textColor=[UIColor blackColor];
    _use_data.textColor=[UIColor blackColor];
    _push_notification.textColor=[UIColor blackColor];
    _save_started_photo.textColor=[UIColor blackColor];
    _call_center.textColor=[UIColor blackColor];
    _report_problem.textColor=[UIColor blackColor];
    _advertising.textColor=[UIColor blackColor];
    _blog.textColor=[UIColor blackColor];
    _privacy_policy.textColor=[UIColor blackColor];
    _conditions.textColor=[UIColor blackColor];
    _clean_history.textColor=[UIColor blackColor];
    _add_acount.textColor=[UIColor blackColor];
    _exit.textColor=[UIColor blackColor];
    _open_source_img_library.textColor=[UIColor blackColor];
    UIImage *image1 = [[UIImage imageNamed:@"facebook"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    _facebook.image=image1;
    _facebook.tintColor=[UIColor colorWithRed:23.0/255 green:125.0/255 blue:46.0/255 alpha:1.0];
    UIImage *image2 = [[UIImage imageNamed:@"vk"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    _vk.image=image2;
    _vk.tintColor=[UIColor colorWithRed:23.0/255 green:125.0/255 blue:46.0/255 alpha:1.0];
        [self.tableView reloadData];
    
}
- (IBAction)back:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)close_account:(id)sender {
}
- (IBAction)save_started_switch:(id)sender {
}
@end
