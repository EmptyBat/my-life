//
//  editProfiles.h
//  My Life
//
//  Created by Admin on 05.10.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface
editProfiles : UITableViewController
- (IBAction)back:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *back;
@property (strong, nonatomic) IBOutlet UITextField *name;
@property (strong, nonatomic) IBOutlet UIImageView *compose;
@property (strong, nonatomic) IBOutlet UIImageView *accept;
@property (strong, nonatomic) IBOutlet UITextField *nickname;
@property (strong, nonatomic) IBOutlet UITextField *websait;
@property (strong, nonatomic) IBOutlet UIImageView *websait_compose;
@property (strong, nonatomic) IBOutlet UITextField *about_me;
@property (strong, nonatomic) IBOutlet UIImageView *about_me_compose;
@property (strong, nonatomic) IBOutlet UILabel *email;
@property (strong, nonatomic) IBOutlet UILabel *phone;
@property (strong, nonatomic) IBOutlet UILabel *sex;
@property (strong, nonatomic) IBOutlet UIButton *done;
- (IBAction)done:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *title_btn;
- (IBAction)sex:(id)sender;

@end
