//
//  search_pager.m
//  My Life
//
//  Created by Admin on 16.10.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "search_pager.h"

@interface search_pager ()
{
    UIColor*tableCellColor;
    UIColor*textColor;
    UIColor*imageColor;
    UIColor*textColor_main;
}
@property (nonatomic) CAPSPageMenu *pageMenu;
@end

@implementation search_pager

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Поделиться";
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor blackColor]}];
    self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
    self.navigationController.navigationBar.shadowImage = [[UIImage alloc] init];
    [self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.translucent = false;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    NSString *theme = [[NSUserDefaults standardUserDefaults] stringForKey:@"theme"];
    if ([theme isEqualToString:@"black_diamond"]){
        [self black_diamond_style];
    }
    else  if ([theme isEqualToString:@"sea_blue"]){
        [self sea_blue_style];
    }
    else  if ([theme isEqualToString:@"soft_pink"]){
        [self soft_pink_style];
    }
    else  if ([theme isEqualToString:@"green"]){
        [self green_style];
    }
    else {
        [self black_pearl_style];
    }

    UIStoryboard*  sb = [UIStoryboard storyboardWithName:@"Main"
                                                  bundle:nil];
    Search_Bests* controller1 = [sb instantiateViewControllerWithIdentifier:@"bests"];
    controller1.title = @"Лучшее";
    Search_peoples* controller2 = [sb instantiateViewControllerWithIdentifier:@"peoples"];
        controller2.title = @"Люди";
    Search_marks* controller3 = [sb instantiateViewControllerWithIdentifier:@"marks"];
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    
    controller3.title = @"Метки";
    NSArray *controllerArray = @[controller1, controller2,controller3];
    NSDictionary *parameters = @{
                                 CAPSPageMenuOptionScrollMenuBackgroundColor: tableCellColor,
                                 CAPSPageMenuOptionViewBackgroundColor:tableCellColor,
                                 CAPSPageMenuOptionSelectionIndicatorColor: textColor,
                                 CAPSPageMenuOptionBottomMenuHairlineColor: [UIColor colorWithRed:70.0/255.0 green:70.0/255.0 blue:70.0/255.0 alpha:1.0],
                                 CAPSPageMenuOptionSelectedMenuItemLabelColor:textColor_main,
                                 CAPSPageMenuOptionUnselectedMenuItemLabelColor:textColor_main,
                                 CAPSPageMenuOptionMenuItemFont: [UIFont fontWithName:@"HelveticaNeue" size:13.0],
                                 CAPSPageMenuOptionMenuHeight: @(40.0),
                                 CAPSPageMenuOptionMenuItemWidth: @(screenWidth/3),
                                 CAPSPageMenuOptionCenterMenuItems: @(YES),
                                 CAPSPageMenuOptionMenuMargin:@(0),
                                 };
    
    _pageMenu = [[CAPSPageMenu alloc] initWithViewControllers:controllerArray frame:CGRectMake(0.0, 50.0, self.view.frame.size.width, self.view.frame.size.height) options:parameters];
    [self.view addSubview:_pageMenu.view];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(THEME:)
                                                 name:@"THEME" object:nil];
}
- (void)THEME:(NSNotification *)notice{
    NSString *theme = [[NSUserDefaults standardUserDefaults] stringForKey:@"theme"];
    if ([theme isEqualToString:@"black_diamond"]){
        [self black_diamond_style];
    }
    else  if ([theme isEqualToString:@"sea_blue"]){
        [self sea_blue_style];
    }
    else  if ([theme isEqualToString:@"soft_pink"]){
        [self soft_pink_style];
    }
    else  if ([theme isEqualToString:@"green"]){
        [self green_style];
    }
    else {
        [self black_pearl_style];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)didTapGoToLeft {
    NSInteger currentIndex = self.pageMenu.currentPageIndex;
    
    if (currentIndex > 0) {
        [_pageMenu moveToPage:currentIndex - 1];
    }
}

- (void)didTapGoToRight {
    NSInteger currentIndex = self.pageMenu.currentPageIndex;
    
    if (currentIndex < self.pageMenu.controllerArray.count) {
        [self.pageMenu moveToPage:currentIndex + 1];
    }
}
-(void)black_pearl_style{
    imageColor= [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    textColor = [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    [_title_btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    tableCellColor=[UIColor whiteColor];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleDefault;
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1.0];;
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor blackColor]};
    UIImage *image = [[UIImage imageNamed:@"YellowBack"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.back setImage:image forState:UIControlStateNormal];
    UIImage *image1 = [[UIImage imageNamed:@"tab_2"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.search_btn setImage:image1 forState:UIControlStateNormal];
    self.back.tintColor = textColor;
    self.search_btn.tintColor = textColor;
   [self.cancel_btn setTitleColor:textColor forState:UIControlStateNormal];
    self.line_1.backgroundColor=textColor;
    self.line_2.backgroundColor=textColor;
    self.search_btn.tintColor=textColor;
    self.tabBarController.tabBar.barTintColor = [UIColor whiteColor];
    self.tabBarController.tabBar.tintColor = [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    textColor_main=[UIColor blackColor];
    [self.search_textfeild setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
    self.search_textfeild.textColor=[UIColor blackColor];
    self.search_textfeild.backgroundColor=tableCellColor;
  self.search_textfeild.backgroundColor=[UIColor colorWithRed:242.0/255 green:242.0/255 blue:242.0/255 alpha:1.0];
    self.view.backgroundColor =[UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1.0];
}
-(void)black_diamond_style{
    imageColor=  [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    textColor = [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    [_title_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleBlackOpaque;
    tableCellColor=[UIColor blackColor];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor =[UIColor blackColor];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    UIImage *image = [[UIImage imageNamed:@"YellowBack"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.back setImage:image forState:UIControlStateNormal];
    UIImage *image1 = [[UIImage imageNamed:@"tab_2"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.search_btn setImage:image1 forState:UIControlStateNormal];
    self.back.tintColor = textColor;
    self.search_btn.tintColor = textColor;
    [self.cancel_btn setTitleColor:textColor forState:UIControlStateNormal];
    self.line_1.backgroundColor=textColor;
    self.line_2.backgroundColor=textColor;
    self.search_btn.tintColor=textColor;
    self.tabBarController.tabBar.barTintColor = [UIColor blackColor];
    self.tabBarController.tabBar.tintColor = [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    textColor_main=[UIColor whiteColor];
    [self.search_textfeild setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    self.search_textfeild.textColor=[UIColor whiteColor];
    self.search_textfeild.backgroundColor=tableCellColor;
    self.search_view.backgroundColor=tableCellColor;
    self.view.backgroundColor =[UIColor colorWithRed:23.0/255 green:23.0/255 blue:23.0/255 alpha:1.0];
}
-(void)sea_blue_style{
    tableCellColor=[UIColor whiteColor];
    imageColor= [UIColor colorWithRed:2.0/255 green:35.0/255 blue:143.0/255 alpha:1.0];
    textColor = [UIColor colorWithRed:2.0/255 green:35.0/255 blue:143.0/255 alpha:1.0];
    [_title_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor=textColor;
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    UIImage *image = [[UIImage imageNamed:@"YellowBack"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.back setImage:image forState:UIControlStateNormal];
    UIImage *image1 = [[UIImage imageNamed:@"tab_2"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.search_btn setImage:image1 forState:UIControlStateNormal];
     self.back.tintColor = [UIColor whiteColor];
    self.search_btn.tintColor = textColor;
    [self.cancel_btn setTitleColor:textColor forState:UIControlStateNormal];
    self.line_1.backgroundColor=textColor;
    self.line_2.backgroundColor=textColor;
    self.search_btn.tintColor=textColor;
    self.tabBarController.tabBar.barTintColor =[UIColor whiteColor];
    self.tabBarController.tabBar.tintColor = [UIColor colorWithRed:2.0/255 green:35.0/255 blue:143.0/255 alpha:1.0];
    textColor_main=[UIColor blackColor];
    [self.search_textfeild setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
    self.search_textfeild.textColor=[UIColor blackColor];
    self.search_textfeild.backgroundColor=[UIColor colorWithRed:242.0/255 green:242.0/255 blue:242.0/255 alpha:1.0];
    self.search_view.backgroundColor=tableCellColor;
    self.view.backgroundColor =[UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1.0];
}
-(void)soft_pink_style{
    imageColor= [UIColor colorWithRed:231.0/255 green:194.0/255 blue:202.0/255 alpha:1.0];
    textColor = [UIColor colorWithRed:231.0/255 green:194.0/255 blue:202.0/255 alpha:1.0];
    [_title_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor=textColor;
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    UIImage *image = [[UIImage imageNamed:@"YellowBack"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.back setImage:image forState:UIControlStateNormal];
    UIImage *image1 = [[UIImage imageNamed:@"tab_2"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.search_btn setImage:image1 forState:UIControlStateNormal];
    self.back.tintColor = [UIColor whiteColor];
    self.search_btn.tintColor = textColor;
    [self.cancel_btn setTitleColor:textColor forState:UIControlStateNormal];
    self.line_1.backgroundColor=textColor;
    self.line_2.backgroundColor=textColor;
    self.search_btn.tintColor=textColor;
    tableCellColor=[UIColor whiteColor];
    self.tabBarController.tabBar.barTintColor =[UIColor whiteColor];
    self.tabBarController.tabBar.tintColor =[UIColor colorWithRed:231.0/255 green:194.0/255 blue:202.0/255 alpha:1.0];
    textColor_main=[UIColor blackColor];
    [self.search_textfeild setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
    self.search_textfeild.textColor=[UIColor blackColor];
  self.search_textfeild.backgroundColor=[UIColor colorWithRed:242.0/255 green:242.0/255 blue:242.0/255 alpha:1.0];
    self.search_view.backgroundColor=tableCellColor;
    self.view.backgroundColor =[UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1.0];
}
-(void)green_style{
    imageColor= [UIColor colorWithRed:23.0/255 green:125.0/255 blue:46.0/255 alpha:1.0];
    textColor = [UIColor colorWithRed:23.0/255 green:125.0/255 blue:46.0/255 alpha:1.0];
    [_title_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor=textColor;
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    UIImage *image = [[UIImage imageNamed:@"YellowBack"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.back setImage:image forState:UIControlStateNormal];
    UIImage *image1 = [[UIImage imageNamed:@"tab_2"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.search_btn setImage:image1 forState:UIControlStateNormal];
     self.back.tintColor = [UIColor whiteColor];
    self.search_btn.tintColor = textColor;
    [self.cancel_btn setTitleColor:textColor forState:UIControlStateNormal];
    self.line_1.backgroundColor=textColor;
    self.line_2.backgroundColor=textColor;
    self.search_btn.tintColor=textColor;
    tableCellColor=[UIColor whiteColor];
    self.tabBarController.tabBar.barTintColor =[UIColor whiteColor];
    self.tabBarController.tabBar.tintColor = [UIColor colorWithRed:23.0/255 green:125.0/255 blue:46.0/255 alpha:1.0];
    textColor_main=[UIColor blackColor];
    [self.search_textfeild setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
    self.search_textfeild.textColor=[UIColor blackColor];
  self.search_textfeild.backgroundColor=[UIColor colorWithRed:242.0/255 green:242.0/255 blue:242.0/255 alpha:1.0];
    self.search_view.backgroundColor=tableCellColor;
    self.view.backgroundColor =[UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1.0];
}

- (IBAction)back:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
}
@end
