//
//  CLRotateTool.h
//
//  Created by sho yakushiji on 2013/11/08.
//  Copyright (c) 2013年 CALACULU. All rights reserved.
//

#import "CLImageToolBase.h"

@interface CLRotateTool : CLImageToolBase
@property(strong, nonatomic) UIButton*rotate_90;
@property(strong, nonatomic) UIButton*rotate_180;
@property(strong, nonatomic) UIButton*mirror_effect;
@end
