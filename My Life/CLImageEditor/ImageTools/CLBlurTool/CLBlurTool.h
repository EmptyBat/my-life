//
//  CLBlurTool.h
//
//  Created by sho yakushiji on 2013/10/19.
//  Copyright (c) 2013年 CALACULU. All rights reserved.
//

#import "CLImageToolBase.h"

@interface CLBlurTool : CLImageToolBase
@property(strong, nonatomic) UIButton*radial_btn;
@property(strong, nonatomic) UIButton*band_btn;
@end
