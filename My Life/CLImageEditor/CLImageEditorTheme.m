//
//  CLImageEditorTheme.m
//
//  Created by sho yakushiji on 2013/12/05.
//  Copyright (c) 2013年 CALACULU. All rights reserved.
//

#import "CLImageEditorTheme.h"

@implementation CLImageEditorTheme
{
    UIColor*tableCellColor;
    UIColor*textColor;
    UIColor*imageColor;
    UIColor*textColor_main;
}
#pragma mark - singleton pattern

static CLImageEditorTheme *_sharedInstance = nil;

+ (CLImageEditorTheme*)theme
{
    static dispatch_once_t  onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[CLImageEditorTheme alloc] init];
    });
    return _sharedInstance;
}

+ (id)allocWithZone:(NSZone *)zone
{
    @synchronized(self) {
        if (_sharedInstance == nil) {
            _sharedInstance = [super allocWithZone:zone];
            return _sharedInstance;
        }
    }
    return nil;
}

- (id)copyWithZone:(NSZone *)zone
{
    return self;
}
-(void)black_pearl_style{
    imageColor= [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    textColor = [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    tableCellColor=[UIColor whiteColor];

}
-(void)black_diamond_style{
    imageColor=  [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    textColor = [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    tableCellColor=[UIColor blackColor];
   }
-(void)sea_blue_style{
    tableCellColor=[UIColor whiteColor];
    imageColor= [UIColor colorWithRed:2.0/255 green:35.0/255 blue:143.0/255 alpha:1.0];
    textColor = [UIColor colorWithRed:2.0/255 green:35.0/255 blue:143.0/255 alpha:1.0];
}
-(void)soft_pink_style{
    imageColor= [UIColor colorWithRed:231.0/255 green:194.0/255 blue:202.0/255 alpha:1.0];
    textColor = [UIColor colorWithRed:231.0/255 green:194.0/255 blue:202.0/255 alpha:1.0];
}
-(void)green_style{
    imageColor= [UIColor colorWithRed:23.0/255 green:125.0/255 blue:46.0/255 alpha:1.0];
    textColor = [UIColor colorWithRed:23.0/255 green:125.0/255 blue:46.0/255 alpha:1.0];
    
}
- (void)THEME:(NSNotification *)notice{
    NSString *theme = [[NSUserDefaults standardUserDefaults] stringForKey:@"theme"];
    if ([theme isEqualToString:@"black_diamond"]){
        [self black_diamond_style];
    }
    else  if ([theme isEqualToString:@"sea_blue"]){
        [self sea_blue_style];
    }
    else  if ([theme isEqualToString:@"soft_pink"]){
        [self soft_pink_style];
    }
    else  if ([theme isEqualToString:@"green"]){
        [self green_style];
    }
    else {
        [self black_pearl_style];
    }
}
- (id)init
{
    NSString *theme = [[NSUserDefaults standardUserDefaults] stringForKey:@"theme"];
    if ([theme isEqualToString:@"black_diamond"]){
        [self black_diamond_style];
    }
    else  if ([theme isEqualToString:@"sea_blue"]){
        [self sea_blue_style];
    }
    else  if ([theme isEqualToString:@"soft_pink"]){
        [self soft_pink_style];
    }
    else  if ([theme isEqualToString:@"green"]){
        [self green_style];
    }
    else {
        [self black_pearl_style];
    }

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(THEME:)
                                                 name:@"THEME" object:nil];
    self = [super init];
    if (self) {
        self.bundleName                     = @"CLImageEditor";
        
        self.backgroundColor                = [UIColor whiteColor];
        self.toolbarColor                   = tableCellColor;
		self.toolIconColor                  = @"black";
        self.toolbarTextColor               = textColor;
        self.toolbarSelectedButtonColor     = [[UIColor cyanColor] colorWithAlphaComponent:0.2];
        self.toolbarTextFont                = [UIFont systemFontOfSize:10];
    }
    return self;
}

@end
