//
//  Write_EmailAdress.h
//  My Life
//
//  Created by Admin on 28.09.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Write_EmailAdress : UIViewController
- (IBAction)back:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *back;
@property (strong, nonatomic) IBOutlet UITextField *email;
@property (strong, nonatomic) IBOutlet UIButton *next;
- (IBAction)next:(id)sender;

@end
