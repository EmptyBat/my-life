//
//  Check_Confirmation.m
//  My Life
//
//  Created by Admin on 28.09.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "Check_Confirmation.h"
#import "Create_username.h"
@interface Check_Confirmation ()

@end

@implementation Check_Confirmation

- (void)viewDidLoad {
    [super viewDidLoad];
  [self.Write_code setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    [gestureRecognizer setCancelsTouchesInView:NO];
       [self.view addGestureRecognizer:gestureRecognizer];
}
-(void)hideKeyboard{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    Create_username*seg=segue.destinationViewController;
    seg.phone=self.phone_number;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (IBAction)back:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)next:(id)sender {
    [self performSegueWithIdentifier:@"next" sender:self];
}
@end
