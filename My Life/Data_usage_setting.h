//
//  Data_usage_setting.h
//  My Life
//
//  Created by Admin on 23.10.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Data_usage_setting : UITableViewController
@property (strong, nonatomic) IBOutlet UIButton *title_btn;
@property (strong, nonatomic) IBOutlet UIButton *back;
- (IBAction)back:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *done;
- (IBAction)done:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *footer;
@property (strong, nonatomic) IBOutlet UILabel *footer_label;
@property (strong, nonatomic) IBOutlet UISwitch *switch_on;
@property (strong, nonatomic) IBOutlet UILabel *label_data;

@end
