//
//  Share_ViewController.m
//  My Life
//
//  Created by Admin on 16.10.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "Share_ViewController.h"
#import "SHModel.h"
#import "SWActionSheet.h"
#import "WaitingView.h"
#import <CommonCrypto/CommonHMAC.h>
#import "AFNetworking.h"
#import "Constant.h"
@interface Share_ViewController ()
{
    SHModel *sModel;
    WaitingView *wView;
 
}
@property (nonatomic) CAPSPageMenu *pageMenu;
@end

@implementation Share_ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    sModel = [SHModel new];
    wView  = [WaitingView new];
    self.title = @"Поделиться";
    self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
    self.navigationController.navigationBar.shadowImage = [[UIImage alloc] init];
    [self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.translucent = false;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Готово" style:UIBarButtonItemStyleDone target:self action:@selector(didTapGoToRight)];
   self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    UIStoryboard*  sb = [UIStoryboard storyboardWithName:@"Main"
                                                  bundle:nil];
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    Subscription* controller1 = [sb instantiateViewControllerWithIdentifier:@"subs"];
    controller1.title = @"Подписчики";
    Direct_subscription* controller2 = [sb instantiateViewControllerWithIdentifier:@"direct"];
    controller2.title = @"Direct";
    NSArray *controllerArray = @[controller1, controller2];
    NSDictionary *parameters = @{
                                 CAPSPageMenuOptionScrollMenuBackgroundColor: [UIColor whiteColor],
                                 CAPSPageMenuOptionViewBackgroundColor: [UIColor whiteColor],
                                 CAPSPageMenuOptionSelectionIndicatorColor:[UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1],
                                 CAPSPageMenuOptionBottomMenuHairlineColor: [UIColor colorWithRed:70.0/255.0 green:70.0/255.0 blue:70.0/255.0 alpha:1.0],
                                 CAPSPageMenuOptionSelectedMenuItemLabelColor:[UIColor blackColor],
                                 CAPSPageMenuOptionUnselectedMenuItemLabelColor:[UIColor blackColor],
                                 CAPSPageMenuOptionMenuItemFont: [UIFont fontWithName:@"HelveticaNeue" size:13.0],
                                 CAPSPageMenuOptionMenuHeight: @(40.0),
                                 CAPSPageMenuOptionMenuItemWidth: @(screenWidth/2),
                                 CAPSPageMenuOptionCenterMenuItems: @(YES),
                                 CAPSPageMenuOptionMenuMargin:@(0),
                                 };
    
    _pageMenu = [[CAPSPageMenu alloc] initWithViewControllers:controllerArray frame:CGRectMake(0.0, 0.0, self.view.frame.size.width, self.view.frame.size.height) options:parameters];
    [self.view addSubview:_pageMenu.view];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)didTapGoToLeft {
    NSInteger currentIndex = self.pageMenu.currentPageIndex;
    
    if (currentIndex > 0) {
        [_pageMenu moveToPage:currentIndex - 1];
    }
}

- (void)didTapGoToRight {
    [self testS3];
    
    NSInteger currentIndex = self.pageMenu.currentPageIndex;
    
  /*  if (currentIndex < self.pageMenu.controllerArray.count) {
        [self.pageMenu moveToPage:currentIndex + 1];
    }*/
}
- (IBAction)back:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
}
static inline char itohs(int i) {
    if (i > 9) return 'A' + (i - 10);
    return '0' + i;
}
NSString * NSDataToHexs(NSData *data) {
    NSUInteger i, len;
    unsigned char *buf, *bytes;
    
    len = data.length;
    bytes = (unsigned char*)data.bytes;
    buf = malloc(len*2);
    
    for (i=0; i<len; i++) {
        buf[i*2] = itohs((bytes[i] >> 4) & 0xF);
        buf[i*2+1] = itohs(bytes[i] & 0xF);
    }
    
    return [[NSString alloc] initWithBytesNoCopy:buf
                                          length:len*2
                                        encoding:NSASCIIStringEncoding
                                    freeWhenDone:YES];
}
- (void)testS3
{
    //[wView addWaitinViewToView:self.view];
    
    [wView removeWaitActivity];
    NSData* imageData = [[NSUserDefaults standardUserDefaults] objectForKey:@"img_filtred"];
    UIImage* image = [UIImage imageWithData:imageData];

    NSData* datas = UIImageJPEGRepresentation(image, 1.0);
    NSMutableData *macOut = [NSMutableData dataWithLength:CC_SHA256_DIGEST_LENGTH];
    
    CC_SHA256(datas.bytes, datas.length,  macOut.mutableBytes);
    NSString*contentHashString=NSDataToHexs(macOut);
    NSLog(@"dataIn: %@", contentHashString);
    NSLog(@"macOut: %@", macOut);
    NSLog(@"%lu",(unsigned long)macOut.length);
    NSString *token = [[NSUserDefaults standardUserDefaults] stringForKey:@"token"];
    NSString*hash=[self base64forData:datas];
    NSString*params=[NSString stringWithFormat:@"{'contentHashString':'%@','contentLength' : '%@'}",contentHashString,[NSString stringWithFormat:@"%lu",(unsigned long)macOut.length]];
    NSLog(@"======%@",params);
    NSString*urls=[NSString stringWithFormat:@"%@reg-server/services/AwsToken",mainURL];
    NSLog(@"=====%@",urls);
    NSURL* url = [NSURL URLWithString:urls];
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"POST";
    [request setValue:token forHTTPHeaderField:@"auth"];
    request.HTTPBody = [params dataUsingEncoding:NSUTF8StringEncoding];
    NSURLResponse *responce;
    NSData *GETReply = [NSURLConnection sendSynchronousRequest:request returningResponse:&responce error:nil];
    NSString *theReply = [[NSString alloc] initWithBytes:[GETReply bytes] length:[GETReply length] encoding:NSASCIIStringEncoding];
    NSError* error;
    NSLog(@"+++++++%@",theReply);
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:GETReply
                                                         options:kNilOptions
                                                           error:&error];
    NSNumber * isSuccessNumber = (NSNumber *)[json objectForKey: @"success"];
    NSString *authorization = [json objectForKey:@"authorization"];
    NSString *endpointUrl = [json objectForKey:@"endpointUrl"];
    NSString *resourceId = [json objectForKey:@"resourceId"];
    NSString *errorDesc = [json objectForKey:@"errorDesc"];
<<<<<<< HEAD
    NSString *dates = [json objectForKey:@"x-amz-date"];
    NSString *Host = [json objectForKey:@"Host"];
    
    if([isSuccessNumber boolValue] == YES){
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init] ;
        [dateFormatter setDateFormat:@"EEE, d MMM yyyy HH:mm:ss zzzz"];
        NSString *date = [dateFormatter stringFromDate:[NSDate date]];
=======
    [[[UIAlertView alloc] initWithTitle:nil message:errorDesc delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
    if([isSuccessNumber boolValue] == YES){
>>>>>>> parent of f7c153f... AWS integration &&More requests&&UI updated.
        NSMutableURLRequest *uploadRequest = [[NSMutableURLRequest alloc] init];
        [uploadRequest setURL:[NSURL URLWithString:endpointUrl]];
        uploadRequest.cachePolicy = NSURLRequestReloadIgnoringLocalCacheData;
        uploadRequest.cachePolicy = NSURLRequestReloadIgnoringLocalCacheData;
        [uploadRequest setHTTPMethod:@"PUT"];
        [uploadRequest setValue:@"image/jpeg" forHTTPHeaderField:@"Content-Type"];
        [uploadRequest setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[datas length]] forHTTPHeaderField:@"Content-Length"];
<<<<<<< HEAD
        [uploadRequest setHTTPBody:datas];
        [uploadRequest setValue:authorization forHTTPHeaderField:@"authorization"];
        [uploadRequest setValue:contentHashString forHTTPHeaderField:@"x-amz-content-sha256"];
        [uploadRequest setValue:@"REDUCED_REDUNDANCY" forHTTPHeaderField:@"x-amz-storage-class"];
      //  [uploadRequest setValue:@"public-read" forHTTPHeaderField:@"x-amz-acl"];
        [uploadRequest setValue:dates forHTTPHeaderField:@"x-amz-date"];
        [uploadRequest setValue:Host forHTTPHeaderField:@"Host"];
        
=======
        [uploadRequest setValue:@"image/png" forHTTPHeaderField:@"Content-Type"];
        //  [uploadRequest setValue:authorization forHTTPHeaderField:@"Authorization"];
>>>>>>> parent of f7c153f... AWS integration &&More requests&&UI updated.
        NSURLSession *session = [NSURLSession sharedSession];
        NSLog(@"Requet===%@", [uploadRequest allHTTPHeaderFields]);
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:uploadRequest completionHandler:^(NSData * _Nullable datae, NSURLResponse * _Nullable responses, NSError * _Nullable error) {
              NSString *theReplys = [[NSString alloc] initWithBytes:[datae bytes] length:[datae length] encoding:NSASCIIStringEncoding];
            NSLog(@"++***%@",theReplys);
             NSLog(@"++***%@",responses);
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
<<<<<<< HEAD
            NSString *token = [[NSUserDefaults standardUserDefaults] stringForKey:@"token"];
            [self addPost:resourceId];
            
=======
>>>>>>> parent of f7c153f... AWS integration &&More requests&&UI updated.
        }];
        
        [dataTask resume];
<<<<<<< HEAD
    }//[self.navigationController popViewControllerAnimated:YES];
       //self.tabBarController.selectedIndex = 0;
       /* NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init] ;
=======
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init] ;
>>>>>>> parent of f7c153f... AWS integration &&More requests&&UI updated.
        [dateFormatter setDateFormat:@"EEE, d MMM yyyy HH:mm:ss zzzz"];
        NSString *date = [dateFormatter stringFromDate:[NSDate date]];
        //   [manager.requestSerializer setValue:[NSString stringWithFormat:@"%@",datas] forHTTPHeaderField:@"Expect"];
    }
    /*  [manager.requestSerializer setValue:@"image/png" forHTTPHeaderField:@"Content-Type"];
     [manager.requestSerializer setValue:authorization forHTTPHeaderField:@"Authorization"];
     [manager.requestSerializer setValue:contentHashString forHTTPHeaderField:@"x-amz-content-sha256"];
     [manager.requestSerializer setValue:date forHTTPHeaderField:@"Date"];
     [manager.requestSerializer setValue:[NSString stringWithFormat:@"%lu",(unsigned long)datas.length] forHTTPHeaderField:@"content-length"];
     [manager.requestSerializer setValue:@"REDUCED_REDUNDANCY" forHTTPHeaderField:@"x-amz-storage-class"];
     NSURLSessionDataTask *task = [manager dataTaskWithRequest:request uploadProgress:nil downloadProgress:nil completionHandler:^(NSURLResponse *
     
     */
    
    
    
    //      [[[UIAlertView alloc] initWithTitle:nil message:errorDesc delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
    
    /*  NSString *filePath = @"/path/to/file";
     NSString *contentType = @"text/plain";
     NSString *bucket = @"mybucket";
     NSString *path = @"test";
     NSString *secretAccessKey = @"my-secret-access-key";
     NSString *accessKey = @"my-access-key";
     
     NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init] ;
     [dateFormatter setDateFormat:@"EEE, d MMM yyyy HH:mm:ss zzzz"];
     NSString *date = [dateFormatter stringFromDate:[NSDate date]];
     
     ASIHTTPRequest *request = [[[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@.s3.amazonaws.com/%@",bucket,path]]] autorelease];
     [request setPostBodyFilePath:filePath];
     [request setShouldStreamPostDataFromDisk:YES];
     [request setRequestMethod:@"PUT"];
     
     [request addRequestHeader:@"x-amz-acl" value:@"private"];
     [request addRequestHeader:@"Content-Type" value:contentType];
     [request addRequestHeader:@"Date" value:date];
     
     NSString *canonicalizedAmzHeaders = @"x-amz-acl:private";
     NSString *canonicalizedResource = [NSString stringWithFormat:@"/%@/%@",bucket,path];
     NSString *stringToSign = [NSString stringWithFormat:@"PUT\n\n%@\n%@\n%@\n%@",contentType,date,canonicalizedAmzHeaders,canonicalizedResource];
     
     NSString *signature = [self base64forData:[self HMACSHA1withKey:secretAccessKey forString:stringToSign]];
     NSString *auth = [NSString stringWithFormat:@"AWS %@:%@",accessKey,signature];
     [request addRequestHeader:@"Authorization" value:auth];
     
     
     [request start];
     NSLog(@"%@",[request responseString]);*/
    
}

/*  headers.put("x-amz-content-sha256", contentHashString);
 headers.put("content-length", "" + objectContent.length());
 headers.put("x-amz-storage-class", "REDUCED_REDUNDANCY");
 
 AWS4SignerForAuthorizationHeader signer = new AWS4SignerForAuthorizationHeader(
 endpointUrl, "PUT", "s3", regionName);
 String authorization = signer.computeSignature(headers,
 null, // no query parameters
 contentHashString,
 awsAccessKey,
 awsSecretKey);
 */
// express authorization for this as a header
// Source: http://stackoverflow.com/questions/476455/is-there-a-library-for-iphone-to-work-with-hmac-sha-1-encoding

- (NSData *)HMACSHA1withKey:(NSString *)key forString:(NSString *)string
{
    NSData *clearTextData = [string dataUsingEncoding:NSUTF8StringEncoding];
    NSData *keyData = [key dataUsingEncoding:NSUTF8StringEncoding];
    
    uint8_t digest[CC_SHA1_DIGEST_LENGTH] = {0};
    
    CCHmacContext hmacContext;
    CCHmacInit(&hmacContext, kCCHmacAlgSHA1, keyData.bytes, keyData.length);
    CCHmacUpdate(&hmacContext, clearTextData.bytes, clearTextData.length);
    CCHmacFinal(&hmacContext, digest);
    
    return [NSData dataWithBytes:digest length:CC_SHA1_DIGEST_LENGTH];
}

//Source http://www.cocoadev.com/index.pl?BaseSixtyFour

- (NSString *)base64forData:(NSData *)data
{
    static const char encodingTable[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
    
    if ([data length] == 0)
        return @"";
    
    char *characters = malloc((([data length] + 2) / 3) * 4);
    if (characters == NULL)
        return nil;
    NSUInteger length = 0;
    
    NSUInteger i = 0;
    while (i < [data length])
    {
        char buffer[3] = {0,0,0};
        short bufferLength = 0;
        while (bufferLength < 3 && i < [data length])
            buffer[bufferLength++] = ((char *)[data bytes])[i++];
        
        //  Encode the bytes in the buffer to four characters, including padding "=" characters if necessary.
        characters[length++] = encodingTable[(buffer[0] & 0xFC) >> 2];
        characters[length++] = encodingTable[((buffer[0] & 0x03) << 4) | ((buffer[1] & 0xF0) >> 4)];
        if (bufferLength > 1)
            characters[length++] = encodingTable[((buffer[1] & 0x0F) << 2) | ((buffer[2] & 0xC0) >> 6)];
        else characters[length++] = '=';
        if (bufferLength > 2)
            characters[length++] = encodingTable[buffer[2] & 0x3F];
        else characters[length++] = '=';
    }
    
    return [[NSString alloc] initWithBytesNoCopy:characters length:length encoding:NSASCIIStringEncoding freeWhenDone:YES] ;
}

@end
