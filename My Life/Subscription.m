//
//  Subscription.m
//  My Life
//
//  Created by Admin on 16.10.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "Subscription.h"

@interface Subscription ()
{
    UIColor*tableCellColor;
    UIColor*textColor;
}
@end

@implementation Subscription

- (void)viewDidLoad {
    [super viewDidLoad];
    NSData* imageData = [[NSUserDefaults standardUserDefaults] objectForKey:@"img_filtred"];
    UIImage* image = [UIImage imageWithData:imageData];
    _img.image=image;
    NSString *theme = [[NSUserDefaults standardUserDefaults] stringForKey:@"theme"];
  if ([theme isEqualToString:@"black_diamond"]){
        [self black_diamond_style];
    }
    else  if ([theme isEqualToString:@"sea_blue"]){
        [self sea_blue_style];
    }
    else  if ([theme isEqualToString:@"soft_pink"]){
        [self soft_pink_style];
    }
    else  if ([theme isEqualToString:@"green"]){
        [self green_style];
    }
    else {
        [self black_pearl_style];
    }
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(THEME:)
                                                 name:@"THEME" object:nil];
    
    
    
}
- (void)THEME:(NSNotification *)notice{
    NSString *theme = [[NSUserDefaults standardUserDefaults] stringForKey:@"theme"];
    if ([theme isEqualToString:@"black_diamond"]){
        [self black_diamond_style];
    }
    else  if ([theme isEqualToString:@"sea_blue"]){
        [self sea_blue_style];
    }
    else  if ([theme isEqualToString:@"soft_pink"]){
        [self soft_pink_style];
    }
    else  if ([theme isEqualToString:@"green"]){
        [self green_style];
    }
    else {
        [self black_pearl_style];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#warning Incomplete implementation, return the number of sections
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete implementation, return the number of rows
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section==0){
    static NSString *cellIdentifier = @"select_users";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    
    return cell;
    }
    else if (indexPath.section==1){
        static NSString *cellIdentifier = @"vk";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        }
            return cell;
    }
    else {
        static NSString *cellIdentifier = @"facebook";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        }
            return cell;
    }
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 47;
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    [cell setBackgroundColor:tableCellColor];
    [cell.contentView setBackgroundColor:tableCellColor];
    if (tableCellColor==nil){
        [cell setBackgroundColor:[UIColor whiteColor]];
        [cell.contentView setBackgroundColor:[UIColor whiteColor]];
    }
}
-(void)black_pearl_style{
    tableCellColor=[UIColor whiteColor];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleDefault;
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1.0];;
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor blackColor]};
    self.tableView.backgroundColor =[UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1.0];
    [self.tableView reloadData];
}
-(void)black_diamond_style{
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleBlackOpaque;
    tableCellColor=[UIColor blackColor];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor =[UIColor blackColor];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    self.tableView.backgroundColor =[UIColor colorWithRed:23.0/255 green:23.0/255 blue:23.0/255 alpha:1.0];
    self.tabBarController.tabBar.barTintColor = [UIColor blackColor];
    self.tabBarController.tabBar.tintColor = [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    [self.tableView reloadData];
    
}
-(void)sea_blue_style{
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:2.0/255 green:35.0/255 blue:143.0/255 alpha:1.0];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    self.tableView.backgroundColor =[UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1.0];
    tableCellColor=[UIColor whiteColor];
    self.tabBarController.tabBar.barTintColor =[UIColor whiteColor];
    self.tabBarController.tabBar.tintColor = [UIColor colorWithRed:2.0/255 green:35.0/255 blue:143.0/255 alpha:1.0];
    [self.tableView reloadData];
}
-(void)soft_pink_style{
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:231.0/255 green:194.0/255 blue:202.0/255 alpha:1.0];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    self.tableView.backgroundColor =[UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1.0];
    tableCellColor=[UIColor whiteColor];
    self.tabBarController.tabBar.barTintColor =[UIColor whiteColor];
    self.tabBarController.tabBar.tintColor =[UIColor colorWithRed:231.0/255 green:194.0/255 blue:202.0/255 alpha:1.0];
    [self.tableView reloadData];
    
}
-(void)green_style{
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:23.0/255 green:125.0/255 blue:46.0/255 alpha:1.0];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    self.tableView.backgroundColor =[UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1.0];
    tableCellColor=[UIColor whiteColor];
    self.tabBarController.tabBar.barTintColor =[UIColor whiteColor];
    self.tabBarController.tabBar.tintColor = [UIColor colorWithRed:23.0/255 green:125.0/255 blue:46.0/255 alpha:1.0];
    [self.tableView reloadData];
    
}

@end
