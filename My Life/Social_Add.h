//
//  Social_Add.h
//  My Life
//
//  Created by Admin on 13.10.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Social_Add : UIViewController
@property (strong, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)back:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *title_btn;
@property (strong, nonatomic) IBOutlet UIButton *back;

@end
