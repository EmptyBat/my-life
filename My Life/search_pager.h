//
//  search_pager.h
//  My Life
//
//  Created by Admin on 16.10.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Search_Bests.h"
#import "Search_peoples.h"
#import "Search_marks.h"
#import "CAPSPageMenu.h"
#import "TextFieldCustom.h"
@interface search_pager : UIViewController
@property (strong, nonatomic) IBOutlet UIButton *search_btn;
@property (strong, nonatomic) IBOutlet UIButton *cancel_btn;
@property (strong, nonatomic) IBOutlet UIButton *title_btn;
@property (strong, nonatomic) IBOutlet TextFieldCustom *search_textfeild;
@property (strong, nonatomic) IBOutlet UIView *line_1;
@property (strong, nonatomic) IBOutlet UIView *line_2;
@property (strong, nonatomic) IBOutlet UIButton *back;
- (IBAction)back:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *search_view;

@end
