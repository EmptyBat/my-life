//
//  Liked_ViewController.h
//  My Life
//
//  Created by Admin on 28.10.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Liked_ViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource>
- (IBAction)setting:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *Title_btn;
@property (strong, nonatomic) IBOutlet UIButton *tab_btn_1;
@property (strong, nonatomic) IBOutlet UIButton *tab_btn_2;
- (IBAction)tab_btn_1:(id)sender;
- (IBAction)tab_btn_2:(id)sender;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIButton *back;
- (IBAction)back:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *btn_view;
@end
