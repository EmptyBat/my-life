//
//  Write_PhoneNumber.h
//  My Life
//
//  Created by Admin on 28.09.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Write_PhoneNumber : UIViewController
- (IBAction)back:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *back;
@property (strong, nonatomic) IBOutlet UITextField *phone_number;
@property (strong, nonatomic) IBOutlet UIButton *Next;
- (IBAction)Next:(id)sender;
@end
