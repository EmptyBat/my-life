//
//  Setting_of_publication.h
//  My Life
//
//  Created by Admin on 05.10.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Setting_of_publication : UITableViewController
- (IBAction)back:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *back;
@property (strong, nonatomic) IBOutlet UIButton *title_btn;
@property (strong, nonatomic) IBOutlet UIButton *done_btn;
- (IBAction)done_btn:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *facebook_label;
@property (strong, nonatomic) IBOutlet UIImageView *facebook_icon;
@property (strong, nonatomic) IBOutlet UILabel *vkontakte_label;
@property (strong, nonatomic) IBOutlet UIImageView *vkontakte_icon;
@property (strong, nonatomic) IBOutlet UILabel *ok_label;
@property (strong, nonatomic) IBOutlet UIImageView *ok_icon;
@property (strong, nonatomic) IBOutlet UILabel *Twitter_label;
@property (strong, nonatomic) IBOutlet UIImageView *Twitter_icon;
@property (strong, nonatomic) IBOutlet UILabel *Flickr_label;
@property (strong, nonatomic) IBOutlet UIImageView *Flickr_icon;

@end
