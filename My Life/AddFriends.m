//
//  AddFriends.m
//  My Life
//
//  Created by Admin on 28.09.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "AddFriends.h"

@implementation AddFriends

- (void)awakeFromNib {
    [super awakeFromNib];
      self.count.clipsToBounds = YES;
    self.count.layer.cornerRadius =  self.count.bounds.size.width/2.0;
    self.count.layer.borderWidth = 3.0f;
    self.count.layer.borderColor = [UIColor whiteColor].CGColor;
    self.count.userInteractionEnabled = YES;
   
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
