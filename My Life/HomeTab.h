//
//  HomeTab.h
//  My Life
//
//  Created by Admin on 25.09.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import <MobileCoreServices/MobileCoreServices.h>


@interface HomeTab : UIViewController<UIActionSheetDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UICollectionView *collection;
@property (strong, nonatomic) IBOutlet UITextField *textmessage;
- (IBAction)sendButton:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *Sharing;
//1 - 230  140
@property (strong, nonatomic) IBOutlet UIButton *home;
@property (strong, nonatomic) IBOutlet UIButton *add_button;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *add;
@property (strong, nonatomic) IBOutlet UIButton *search;
@property (strong, nonatomic) IBOutlet UILabel *getting;
@property (strong, nonatomic) IBOutlet UIButton *sending;
@property (strong, nonatomic) IBOutlet UIButton *send;
@property (strong, nonatomic) IBOutlet UIButton *my_life_icon;
- (IBAction)action_btn_1:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *action_btn_1;
@property (strong, nonatomic) IBOutlet UIButton *action_btn_2;
- (IBAction)action_btn_2:(id)sender;
- (IBAction)action_btn_3:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *action_btn_3;
@property (strong, nonatomic) IBOutlet UIButton *action_btn_4;
- (IBAction)action_btn_4:(id)sender;
- (IBAction)action_btn_5:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *actionsheet;
@property (strong, nonatomic) IBOutlet UIButton *action_btn_5;
- (IBAction)actionSheet_show:(id)sender;
- (IBAction)like_btn:(id)sender;
@property (strong, nonatomic) IBOutlet UIImageView *action_arrow_1;
@property (strong, nonatomic) IBOutlet UIImageView *action_arrow_2;
@property (strong, nonatomic) IBOutlet UIImageView *action_arrow_3;
@property (strong, nonatomic) IBOutlet UIImageView *action_arrow_4;
@property (strong, nonatomic) IBOutlet UIImageView *action_arrow_5;
@property (strong, nonatomic) IBOutlet UIButton *search_button;
@property (strong, nonatomic) IBOutlet UITextField *recipients;
@property (strong, nonatomic) MPMoviePlayerController *videoController;

@end
