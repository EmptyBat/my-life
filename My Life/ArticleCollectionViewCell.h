//
//  ArticleCollectionViewCell.h
//  My Life
//
//  Created by Admin on 08.11.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ArticleCollectionViewCell : UICollectionViewCell
@property (weak) IBOutlet UIImageView *articleImage;
@end
