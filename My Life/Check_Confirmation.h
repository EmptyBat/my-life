//
//  Check_Confirmation.h
//  My Life
//
//  Created by Admin on 28.09.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Check_Confirmation : UIViewController
@property (strong, nonatomic) IBOutlet UIButton *back;
- (IBAction)back:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *Write_code;
@property (strong, nonatomic) IBOutlet UIButton *next;
- (IBAction)next:(id)sender;
@property(nonatomic)NSString*phone_number;
@end
