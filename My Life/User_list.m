//
//  User_list.m
//  My Life
//
//  Created by Admin on 13.10.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "User_list.h"
#import "TLYShyNavBarManager.h"
@interface User_list ()
{
    UIColor*tableCellColor;
    UIColor*textColor;
    UIColor*imageColor;
    UIColor*textColor_main;
        NSArray*images;
}
@property (retain, nonatomic) IBOutlet UIView *STAlertView;
@end

@implementation User_list
- (IBAction)back:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
    
}
- (IBAction)share:(id)sender {
    _actionsheet.hidden=true;
    _STAlertView.hidden=false;
    [self.view addSubview:self.STAlertView];
    CAKeyframeAnimation *popAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
    popAnimation.duration = 0.4;
    popAnimation.values = @[[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.01f, 0.01f, 1.0f)],
                            [NSValue valueWithCATransform3D:CATransform3DMakeScale(1.1f, 1.1f, 1.0f)],
                            [NSValue valueWithCATransform3D:CATransform3DMakeScale(0.9f, 0.9f, 1.0f)],
                            [NSValue valueWithCATransform3D:CATransform3DIdentity]];
    popAnimation.keyTimes = @[@0.2f, @0.5f, @0.75f, @1.0f];
    popAnimation.timingFunctions = @[[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut],
                                     [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut],
                                     [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [self.STAlertView.layer addAnimation:popAnimation forKey:nil];
    
}
- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag {
    [self.STAlertView removeFromSuperview];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableVIew.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.tableVIew.bounds.size.width, 10.01f)];
    images=@[@"261087",@"Inside-Chechnya_pixanews-4",@"Layer 591",@"1182e81d6f7efe4f916de7a0d4a_prev.jpg",@"261087",@"Inside-Chechnya_pixanews-4",@"Layer 591",@"1182e81d6f7efe4f916de7a0d4a_prev.jpg",@"261087",@"Inside-Chechnya_pixanews-4",@"Layer 591",@"1182e81d6f7efe4f916de7a0d4a_prev.jpg",@"261087",@"Inside-Chechnya_pixanews-4",@"Layer 591",@"1182e81d6f7efe4f916de7a0d4a_prev.jpg"];
    NSString *theme = [[NSUserDefaults standardUserDefaults] stringForKey:@"theme"];
    if ([theme isEqualToString:@"black_diamond"]){
        [self black_diamond_style];
    }
    else  if ([theme isEqualToString:@"sea_blue"]){
        [self sea_blue_style];
    }
    else  if ([theme isEqualToString:@"soft_pink"]){
        [self soft_pink_style];
    }
    else  if ([theme isEqualToString:@"green"]){
        [self green_style];
    }
    else {
        [self black_pearl_style];
    }
    [self Customization];
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideVIew)];
    [gestureRecognizer setCancelsTouchesInView:NO];
    [self.tableVIew addGestureRecognizer:gestureRecognizer];
    self.shyNavBarManager.scrollView = self.tableVIew;
    [self.shyNavBarManager setStickyExtensionView:NO];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(THEME:)
                                                 name:@"THEME" object:nil];
}
-(void)Customization{
    _action_btn_1.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    _action_btn_1.contentEdgeInsets = UIEdgeInsetsMake(0, 15, 0, 0);
    _action_btn_2.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    _action_btn_2.contentEdgeInsets = UIEdgeInsetsMake(0, 15, 0, 0);
    _action_btn_3.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    _action_btn_3.contentEdgeInsets = UIEdgeInsetsMake(0, 15, 0, 0);
    _action_btn_4.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    _action_btn_4.contentEdgeInsets = UIEdgeInsetsMake(0, 15, 0, 0);
    _action_btn_5.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    _action_btn_5.contentEdgeInsets = UIEdgeInsetsMake(0, 15, 0, 0);
    [[UINavigationBar appearance] setTranslucent:NO];
 /*   [_send.layer setBorderWidth:1.0];
    [_send.layer setBorderColor:[[UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1] CGColor]];*/
    
}
- (void)THEME:(NSNotification *)notice{
    NSString *theme = [[NSUserDefaults standardUserDefaults] stringForKey:@"theme"];
    if ([theme isEqualToString:@"black_diamond"]){
        [self black_diamond_style];
    }
    else  if ([theme isEqualToString:@"sea_blue"]){
        [self sea_blue_style];
    }
    else  if ([theme isEqualToString:@"soft_pink"]){
        [self soft_pink_style];
    }
    else  if ([theme isEqualToString:@"green"]){
        [self green_style];
    }
    else {
        [self black_pearl_style];
    }
    
}
-(void)hideVIew{
    _STAlertView.hidden=true;
    _actionsheet.hidden=true;
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row==0){
        static NSString *cellIdentifier = @"main";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        }
        UILabel*name=(UILabel*) [cell viewWithTag:1];
        UILabel*place=(UILabel*) [cell viewWithTag:3];
        UIButton*Fillled=(UIButton*) [cell viewWithTag:5];
        UIButton*points=(UIButton*) [cell viewWithTag:11];
        UIButton*Repost=(UIButton*) [cell viewWithTag:7];
        UIButton*love=(UIButton*) [cell viewWithTag:8];
        UIButton*question=(UIButton*) [cell viewWithTag:9];
        UIButton* checkbox=(UIButton*) [cell viewWithTag:5];
        UIImage *image22 = [[UIImage imageNamed:@"love"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        UIImage *imageSelected = [[UIImage imageNamed:@"Fillled"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [checkbox setImage:image22 forState:UIControlStateNormal];
        [checkbox setImage:imageSelected forState:UIControlStateSelected];
        [checkbox setImage:imageSelected forState:UIControlStateHighlighted];
        checkbox.tintColor= imageColor;
        checkbox.adjustsImageWhenHighlighted=YES;
        [checkbox addTarget:self
                     action:@selector(chkBtnHandler:)
           forControlEvents:UIControlEventTouchUpInside];
        UIImage *image = [[UIImage imageNamed:@"love"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [Fillled setImage:image forState:UIControlStateNormal];
        Fillled.tintColor= imageColor;
        UIImage *image1 = [[UIImage imageNamed:@"Repost"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [Repost setImage:image1 forState:UIControlStateNormal];
        Repost.tintColor= imageColor;
        love.tintColor= imageColor;
        UIImage *image3 = [[UIImage imageNamed:@"question"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [question setImage:image3 forState:UIControlStateNormal];
        question.tintColor= imageColor;
        UIImage *image4 = [[UIImage imageNamed:@"3 points"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [points setImage:image4 forState:UIControlStateNormal];
        points.tintColor= imageColor;
        name.textColor=textColor_main;
        place.textColor=textColor_main;
        UIImageView*img=(UIImageView*) [cell viewWithTag:4];
        img.image=[UIImage imageNamed:images[indexPath.section]];
        /*  NSDictionary* place = [_count objectAtIndex:indexPath.row];
         NSString *name = [place objectForKey:@"category_name"];
         cell.textLabel.text=name;*/
        return cell;
    }
    else {
        static NSString *cellIdentifier = @"comments";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        }
        UILabel*time=(UILabel*) [cell viewWithTag:2];
        time.textColor=textColor;
        UILabel*name=(UILabel*) [cell viewWithTag:1];
        NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:@"Arai.Nurgazi:Угадай кому еще интересно?)))"];
        [string addAttribute:NSForegroundColorAttributeName value:textColor_main range:NSMakeRange(0,12)];
        [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:170.0/255 green:170.0/255 blue:170.0/255 alpha:1.0] range:NSMakeRange(12,30)];
        name.attributedText=string;
        return cell;
    }
    
}
- (void)chkBtnHandler:(id)sender {
    // If checked, uncheck and visa versa
    [(UIButton *)sender setSelected:![(UIButton *)sender isSelected]];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 8;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==0&&indexPath.row==0){
        return 360;
    }
    else if (indexPath.section==1&&indexPath.row==0){
        return 450;
    }
    else if (indexPath.section==2&&indexPath.row==0){
        return 250;
    }
    else if (indexPath.section==3&&indexPath.row==0){
        return 450;
    }
    else if (indexPath.section==4&&indexPath.row==0){
        return 450;
    }
    else if (indexPath.section==5&&indexPath.row==0){
        return 450;
    }
    else if (indexPath.section==6&&indexPath.row==0){
        return 360;
    }
    else if (indexPath.section==7&&indexPath.row==0){
        return 450;
    }
    else if (indexPath.section==8&&indexPath.row==0){
        return 360;
    }
    else {
        return 63;
        
    }
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    [cell setBackgroundColor:tableCellColor];
    [cell.contentView setBackgroundColor:tableCellColor];
    if (tableCellColor==nil){
        [cell setBackgroundColor:[UIColor whiteColor]];
        [cell.contentView setBackgroundColor:[UIColor whiteColor]];
    }
}
- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    
    return 7;
    
    
}
- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    return 1;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    return cell;
    
}

-(void)black_pearl_style{
        [_title_btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    UIImage *image_title = [[UIImage imageNamed:@"MyListory"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    
    UIView *myView = [[UIView alloc] initWithFrame: CGRectMake(0, 0, 300, 60)];
    ;
    UIImageView *logo = [[UIImageView alloc] initWithImage:image_title];
    logo.contentMode = UIViewContentModeScaleAspectFit;
    logo.tintColor = [UIColor blackColor];
    logo.frame = CGRectMake(self.view.center.x-80, 0, 90, 60);
    [myView setBackgroundColor:[UIColor  clearColor]];
    [myView addSubview:logo];
    self.navigationItem.titleView = myView;
    tableCellColor=[UIColor whiteColor];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleDefault;
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1.0];;
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor blackColor]};
    self.tableVIew.backgroundColor =[UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1.0];
    self.tabBarController.tabBar.barTintColor = [UIColor whiteColor];
    self.tabBarController.tabBar.tintColor = [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    imageColor= [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    textColor = [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    textColor_main=[UIColor blackColor];
    UIImage *image = [[UIImage imageNamed:@"YellowBack"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.back setImage:image forState:UIControlStateNormal];
    self.back.tintColor = textColor;
    [self.tableVIew reloadData];
}
-(void)black_diamond_style{
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleBlackOpaque;
    tableCellColor=[UIColor blackColor];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor =[UIColor blackColor];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    self.tableVIew.backgroundColor =[UIColor colorWithRed:23.0/255 green:23.0/255 blue:23.0/255 alpha:1.0];
    self.tabBarController.tabBar.barTintColor = [UIColor blackColor];
    self.tabBarController.tabBar.tintColor = [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    imageColor=  [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    textColor = [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    textColor_main=[UIColor whiteColor];
        [_title_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    UIImage *image = [[UIImage imageNamed:@"YellowBack"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.back setImage:image forState:UIControlStateNormal];
    self.back.tintColor = textColor;
    [self.tableVIew reloadData];
}

-(void)sea_blue_style{
    [_title_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:2.0/255 green:35.0/255 blue:143.0/255 alpha:1.0];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    self.tableVIew.backgroundColor =[UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1.0];
    tableCellColor=[UIColor whiteColor];
    self.tabBarController.tabBar.barTintColor =[UIColor whiteColor];
    self.tabBarController.tabBar.tintColor = [UIColor colorWithRed:2.0/255 green:35.0/255 blue:143.0/255 alpha:1.0];
    imageColor= [UIColor colorWithRed:2.0/255 green:35.0/255 blue:143.0/255 alpha:1.0];
    textColor = [UIColor colorWithRed:2.0/255 green:35.0/255 blue:143.0/255 alpha:1.0];
    textColor_main=[UIColor blackColor];
    UIImage *image = [[UIImage imageNamed:@"YellowBack"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.back setImage:image forState:UIControlStateNormal];
    self.back.tintColor = [UIColor whiteColor];
    [self.tableVIew reloadData];
}
-(void)soft_pink_style{
    [_title_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:231.0/255 green:194.0/255 blue:202.0/255 alpha:1.0];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    self.tableVIew.backgroundColor =[UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1.0];
        tableCellColor=[UIColor whiteColor];
    self.tabBarController.tabBar.barTintColor =[UIColor whiteColor];
    self.tabBarController.tabBar.tintColor =[UIColor colorWithRed:231.0/255 green:194.0/255 blue:202.0/255 alpha:1.0];
    imageColor= [UIColor colorWithRed:231.0/255 green:194.0/255 blue:202.0/255 alpha:1.0];
    textColor = [UIColor colorWithRed:231.0/255 green:194.0/255 blue:202.0/255 alpha:1.0];
    textColor_main=[UIColor blackColor];
    UIImage *image = [[UIImage imageNamed:@"YellowBack"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.back setImage:image forState:UIControlStateNormal];
    self.back.tintColor = [UIColor whiteColor];
    [self.tableVIew reloadData];
}
-(void)green_style{
    [_title_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:23.0/255 green:125.0/255 blue:46.0/255 alpha:1.0];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    self.tableVIew.backgroundColor =[UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1.0];
       tableCellColor=[UIColor whiteColor];
    self.tabBarController.tabBar.barTintColor =[UIColor whiteColor];
    self.tabBarController.tabBar.tintColor = [UIColor colorWithRed:23.0/255 green:125.0/255 blue:46.0/255 alpha:1.0];
    imageColor= [UIColor colorWithRed:23.0/255 green:125.0/255 blue:46.0/255 alpha:1.0];
    textColor = [UIColor colorWithRed:23.0/255 green:125.0/255 blue:46.0/255 alpha:1.0];
    textColor_main=[UIColor blackColor];
    UIImage *image = [[UIImage imageNamed:@"YellowBack"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.back setImage:image forState:UIControlStateNormal];
    self.back.tintColor = [UIColor whiteColor];
    [self.tableVIew reloadData];
}
- (IBAction)like_btn:(id)sender {
    UIButton *button = (UIButton*)sender;
    if ([[UIImage imageNamed:@"love"] isEqual:button.currentImage]) {
        [sender setImage:[UIImage imageNamed:@"Fillled"] forState:UIControlStateNormal];
    }
    else {
        [sender setImage:[UIImage imageNamed:@"love"] forState:UIControlStateNormal];
    }
    
}
- (IBAction)actionSheet_show:(id)sender {
    _actionsheet.hidden=false;
    _STAlertView.hidden=true;
    [self.view addSubview:self.actionsheet];
    CAKeyframeAnimation *popAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
    popAnimation.duration = 0.4;
    popAnimation.values = @[[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.01f, 0.01f, 1.0f)],
                            [NSValue valueWithCATransform3D:CATransform3DMakeScale(1.1f, 1.1f, 1.0f)],
                            [NSValue valueWithCATransform3D:CATransform3DMakeScale(0.9f, 0.9f, 1.0f)],
                            [NSValue valueWithCATransform3D:CATransform3DIdentity]];
    popAnimation.keyTimes = @[@0.2f, @0.5f, @0.75f, @1.0f];
    popAnimation.timingFunctions = @[[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut],
                                     [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut],
                                     [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [self.actionsheet.layer addAnimation:popAnimation forKey:nil];
    /*JGActionSheetSection *s3 = [JGActionSheetSection sectionWithTitle:@"" message:nil contentView:_STAlertView];
     JGActionSheet *sheet = [JGActionSheet actionSheetWithSections:@[s3, [JGActionSheetSection sectionWithTitle:nil message:nil buttonTitles:@[@"Cancel"] buttonStyle:JGActionSheetButtonStyleCancel]]];
     
     sheet.delegate = self;
     
     sheet.insets = UIEdgeInsetsMake(20.0f, 0.0f, 0.0f, 0.0f);
     
     [sheet showInView:self.navigationController.view animated:YES];
     
     [sheet setButtonPressedBlock:^(JGActionSheet *sheet, NSIndexPath *indexPath) {
     [sheet dismissAnimated:YES];
     }];
     */
    
}

@end
