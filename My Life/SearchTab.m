//
//  SearchTab.m
//  My Life
//
//  Created by Admin on 29.09.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "SearchTab.h"
#import "TLYShyNavBarManager.h"
@interface SearchTab ()
{
    UIColor*tableCellColor;
    NSArray*images;
}
@end

@implementation SearchTab

- (void)viewDidLoad {
    [super viewDidLoad];
    UIImage *image = [UIImage imageNamed:@"MyListory"];
    UIView *myView = [[UIView alloc] initWithFrame: CGRectMake(0, 0, 300, 60)];
    
    UIImageView *logo = [[UIImageView alloc] initWithImage:image];
    logo.contentMode = UIViewContentModeScaleAspectFit;
    logo.frame = CGRectMake(self.view.center.x-50, 0, 90, 60);
    [myView setBackgroundColor:[UIColor  clearColor]];
    [myView addSubview:logo];
    self.navigationItem.titleView = myView;

    NSString *theme = [[NSUserDefaults standardUserDefaults] stringForKey:@"theme"];
    if ([theme isEqualToString:@"black_diamond"]){
        [self black_diamond_style];
    }
    else  if ([theme isEqualToString:@"sea_blue"]){
        [self sea_blue_style];
    }
    else  if ([theme isEqualToString:@"soft_pink"]){
        [self soft_pink_style];
    }
    else  if ([theme isEqualToString:@"green"]){
        [self green_style];
    }
    else {
        [self black_pearl_style];
    }
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(THEME:)
                                                 name:@"THEME" object:nil];
     images=@[@"261087",@"Inside-Chechnya_pixanews-4",@"Layer 591",@"1182e81d6f7efe4f916de7a0d4a_prev.jpg",@"261087",@"Inside-Chechnya_pixanews-4",@"Layer 591",@"1182e81d6f7efe4f916de7a0d4a_prev.jpg",@"261087",@"Inside-Chechnya_pixanews-4",@"Layer 591",@"1182e81d6f7efe4f916de7a0d4a_prev.jpg",@"261087",@"Inside-Chechnya_pixanews-4",@"Layer 591",@"1182e81d6f7efe4f916de7a0d4a_prev.jpg"];

}
- (void)THEME:(NSNotification *)notice{
    NSString *theme = [[NSUserDefaults standardUserDefaults] stringForKey:@"theme"];
    if ([theme isEqualToString:@"black_diamond"]){
        [self black_diamond_style];
    }
    else  if ([theme isEqualToString:@"sea_blue"]){
        [self sea_blue_style];
    }
    else  if ([theme isEqualToString:@"soft_pink"]){
        [self soft_pink_style];
    }
    else  if ([theme isEqualToString:@"green"]){
        [self green_style];
    }
    else {
        [self black_pearl_style];
    }
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {

    return 15;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}
- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    return 1;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
            UICollectionViewCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
        [cell setBackgroundColor:tableCellColor];
        [cell.contentView setBackgroundColor:tableCellColor];
     UIImageView*img=(UIImageView*) [cell viewWithTag:1];
    img.image=[UIImage imageNamed:images[indexPath.row]];
        if (tableCellColor==nil){
            [cell setBackgroundColor:[UIColor whiteColor]];
            [cell.contentView setBackgroundColor:[UIColor whiteColor]];
        }

    return cell;
    
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(collectionView.bounds.size.width/3-1, 106);
}
- (void)viewDidAppear:(BOOL)animated{
    NSString *theme = [[NSUserDefaults standardUserDefaults] stringForKey:@"theme"];
    if ([theme isEqualToString:@"black_diamond"]){
        [self black_diamond_style];
    }
    else  if ([theme isEqualToString:@"sea_blue"]){
        [self sea_blue_style];
    }
    else  if ([theme isEqualToString:@"soft_pink"]){
        [self soft_pink_style];
    }
    else  if ([theme isEqualToString:@"green"]){
        [self green_style];
    }
    else {
        [self black_pearl_style];
    }
    
}
-(void)black_pearl_style{
         _container.backgroundColor=[UIColor whiteColor];
        [self.search setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
        self.search.textColor=[UIColor blackColor];
    UIImage *image_title = [[UIImage imageNamed:@"MyListory"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    
    UIView *myView = [[UIView alloc] initWithFrame: CGRectMake(0, 0, 300, 60)];
    ;
    UIImageView *logo = [[UIImageView alloc] initWithImage:image_title];
    logo.contentMode = UIViewContentModeScaleAspectFit;
    logo.tintColor = [UIColor blackColor];
    logo.frame = CGRectMake(self.view.center.x-60, 0, 90, 60);
    [myView setBackgroundColor:[UIColor  clearColor]];
    [myView addSubview:logo];
    self.navigationItem.titleView = myView;
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleDefault;
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1.0];;
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor blackColor]};
    self.collection.backgroundColor =[UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1.0];
    UIImage *image2 = [[UIImage imageNamed:@"My_life_black"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.My_life_icon setImage:image2 forState:UIControlStateNormal];
    self.My_life_icon.tintColor = [UIColor blackColor];
    UIImage *image3 = [[UIImage imageNamed:@"add"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.add_Button setImage:image3 forState:UIControlStateNormal];
    self.add_Button.tintColor = [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    UIImage *image4 = [[UIImage imageNamed:@"tab_2"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.search_button setImage:image4 forState:UIControlStateNormal];
    self.search_button.tintColor = [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    self.tabBarController.tabBar.barTintColor = [UIColor whiteColor];
    self.tabBarController.tabBar.tintColor = [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    self.view.backgroundColor=[UIColor colorWithRed:247.0/255.0 green:247.0/255.0 blue:247.0/255.0 alpha:1];;
    [self.collection reloadData];
}
-(void)black_diamond_style{
    _container.backgroundColor=[UIColor blackColor];
        [self.search setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    self.search.textColor=[UIColor whiteColor];
    UIImage *image_title = [[UIImage imageNamed:@"MyListory"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    
    UIView *myView = [[UIView alloc] initWithFrame: CGRectMake(0, 0, 300, 60)];
    
    UIImageView *logo = [[UIImageView alloc] initWithImage:image_title];
    logo.contentMode = UIViewContentModeScaleAspectFit;
    logo.tintColor = [UIColor whiteColor];
    logo.frame = CGRectMake(self.view.center.x-60, 0, 90, 60);
    [myView setBackgroundColor:[UIColor  clearColor]];
    [myView addSubview:logo];
    self.navigationItem.titleView = myView;
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleBlackOpaque;
    tableCellColor=[UIColor blackColor];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor =[UIColor blackColor];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    self.collection.backgroundColor =[UIColor colorWithRed:23.0/255 green:23.0/255 blue:23.0/255 alpha:1.0];
    UIImage *image2 = [[UIImage imageNamed:@"My_life_black"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.My_life_icon setImage:image2 forState:UIControlStateNormal];
    self.My_life_icon.tintColor = [UIColor whiteColor];
    UIImage *image3 = [[UIImage imageNamed:@"add"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.add_Button setImage:image3 forState:UIControlStateNormal];
    self.add_Button.tintColor = [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    UIImage *image4 = [[UIImage imageNamed:@"tab_2"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.search_button setImage:image4 forState:UIControlStateNormal];
    self.search_button.tintColor = [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    self.tabBarController.tabBar.barTintColor = [UIColor blackColor];
    self.tabBarController.tabBar.tintColor = [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    self.view.backgroundColor=tableCellColor;
    [self.collection reloadData];
}
-(void)sea_blue_style{
      _container.backgroundColor=[UIColor whiteColor];
      [self.search setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
     self.search.textColor=[UIColor blackColor];
    UIImage *image_title = [[UIImage imageNamed:@"MyListory"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    
    UIView *myView = [[UIView alloc] initWithFrame: CGRectMake(0, 0, 300, 60)];
    ;
    UIImageView *logo = [[UIImageView alloc] initWithImage:image_title];
    logo.contentMode = UIViewContentModeScaleAspectFit;
    logo.tintColor = [UIColor whiteColor];
    logo.frame = CGRectMake(self.view.center.x-60, 0, 90, 60);
    [myView setBackgroundColor:[UIColor  clearColor]];
    [myView addSubview:logo];
    self.navigationItem.titleView = myView;
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:2.0/255 green:35.0/255 blue:143.0/255 alpha:1.0];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    self.collection.backgroundColor =[UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1.0];
    UIImage *image2 = [[UIImage imageNamed:@"My_life_black"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    tableCellColor=[UIColor whiteColor];
    [self.My_life_icon setImage:image2 forState:UIControlStateNormal];
    self.My_life_icon.tintColor = [UIColor whiteColor];
    UIImage *image3 = [[UIImage imageNamed:@"add"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.add_Button setImage:image3 forState:UIControlStateNormal];
    self.add_Button.tintColor = [UIColor colorWithRed:2.0/255 green:35.0/255 blue:143.0/255 alpha:1.0];
    UIImage *image4 = [[UIImage imageNamed:@"tab_2"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.search_button setImage:image4 forState:UIControlStateNormal];
    self.search_button.tintColor = [UIColor colorWithRed:2.0/255 green:35.0/255 blue:143.0/255 alpha:1.0];
    self.tabBarController.tabBar.barTintColor =[UIColor whiteColor];
    self.tabBarController.tabBar.tintColor = [UIColor colorWithRed:2.0/255 green:35.0/255 blue:143.0/255 alpha:1.0];
      self.view.backgroundColor=[UIColor colorWithRed:247.0/255.0 green:247.0/255.0 blue:247.0/255.0 alpha:1];;
    [self.collection reloadData];
}
-(void)soft_pink_style{
         _container.backgroundColor=[UIColor whiteColor];
      [self.search setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
     self.search.textColor=[UIColor blackColor];
    UIImage *image_title = [[UIImage imageNamed:@"MyListory"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    
    UIView *myView = [[UIView alloc] initWithFrame: CGRectMake(0, 0, 300, 60)];
    ;
    UIImageView *logo = [[UIImageView alloc] initWithImage:image_title];
    logo.contentMode = UIViewContentModeScaleAspectFit;
    logo.tintColor = [UIColor whiteColor];
    logo.frame = CGRectMake(self.view.center.x-60, 0, 90, 60);
    [myView setBackgroundColor:[UIColor  clearColor]];
    [myView addSubview:logo];
    self.navigationItem.titleView = myView;
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:231.0/255 green:194.0/255 blue:202.0/255 alpha:1.0];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    self.collection.backgroundColor =[UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1.0];;
    UIImage *image2 = [[UIImage imageNamed:@"My_life_black"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.My_life_icon setImage:image2 forState:UIControlStateNormal];
    self.My_life_icon.tintColor = [UIColor whiteColor];
    UIImage *image3 = [[UIImage imageNamed:@"add"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.add_Button setImage:image3 forState:UIControlStateNormal];
    self.add_Button.tintColor = [UIColor colorWithRed:231.0/255 green:194.0/255 blue:202.0/255 alpha:1.0];
    UIImage *image4 = [[UIImage imageNamed:@"tab_2"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.search_button setImage:image4 forState:UIControlStateNormal];
    self.search_button.tintColor = [UIColor colorWithRed:231.0/255 green:194.0/255 blue:202.0/255 alpha:1.0];
    tableCellColor=[UIColor whiteColor];
    self.tabBarController.tabBar.barTintColor =[UIColor whiteColor];
    self.tabBarController.tabBar.tintColor =[UIColor colorWithRed:231.0/255 green:194.0/255 blue:202.0/255 alpha:1.0];
   self.view.backgroundColor=[UIColor colorWithRed:247.0/255.0 green:247.0/255.0 blue:247.0/255.0 alpha:1];;
    [self.collection reloadData];
}
-(void)green_style{
         _container.backgroundColor=[UIColor whiteColor];
      [self.search setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
     self.search.textColor=[UIColor blackColor];
    UIImage *image_title = [[UIImage imageNamed:@"MyListory"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    
    UIView *myView = [[UIView alloc] initWithFrame: CGRectMake(0, 0, 300, 60)];
    ;
    UIImageView *logo = [[UIImageView alloc] initWithImage:image_title];
    logo.contentMode = UIViewContentModeScaleAspectFit;
    logo.tintColor = [UIColor whiteColor];
    logo.frame = CGRectMake(self.view.center.x-60, 0, 90, 60);
    [myView setBackgroundColor:[UIColor  clearColor]];
    [myView addSubview:logo];
    self.navigationItem.titleView = myView;
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:23.0/255 green:125.0/255 blue:46.0/255 alpha:1.0];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    self.collection.backgroundColor =[UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1.0];
    UIImage *image2 = [[UIImage imageNamed:@"My_life_black"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.My_life_icon setImage:image2 forState:UIControlStateNormal];
    self.My_life_icon.tintColor = [UIColor whiteColor];
    UIImage *image3 = [[UIImage imageNamed:@"add"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.add_Button setImage:image3 forState:UIControlStateNormal];
    self.add_Button.tintColor = [UIColor colorWithRed:23.0/255 green:125.0/255 blue:46.0/255 alpha:1.0];
    UIImage *image4 = [[UIImage imageNamed:@"tab_2"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.search_button setImage:image4 forState:UIControlStateNormal];
    self.search_button.tintColor =[UIColor colorWithRed:23.0/255 green:125.0/255 blue:46.0/255 alpha:1.0];
    tableCellColor=[UIColor whiteColor];
    self.tabBarController.tabBar.barTintColor =[UIColor whiteColor];
    self.tabBarController.tabBar.tintColor = [UIColor colorWithRed:23.0/255 green:125.0/255 blue:46.0/255 alpha:1.0];
   self.view.backgroundColor=[UIColor colorWithRed:247.0/255.0 green:247.0/255.0 blue:247.0/255.0 alpha:1];;
    [self.collection reloadData];
}

@end
