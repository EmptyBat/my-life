//
//  Language_setting.h
//  My Life
//
//  Created by Admin on 23.10.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Language_setting : UITableViewController
@property (strong, nonatomic) IBOutlet UILabel *russki_label;
@property (strong, nonatomic) IBOutlet UILabel *russki_sub_title;
@property (strong, nonatomic) IBOutlet UIImageView *checked_russki;
@property (strong, nonatomic) IBOutlet UILabel *danks_label;
@property (strong, nonatomic) IBOutlet UILabel *danks_sub_title;
@property (strong, nonatomic) IBOutlet UIImageView *check_danks;
@property (strong, nonatomic) IBOutlet UILabel *english_label;
@property (strong, nonatomic) IBOutlet UILabel *english_sub_title;
@property (strong, nonatomic) IBOutlet UIImageView *check_english;
@property (strong, nonatomic) IBOutlet UILabel *Suomi_label;
@property (strong, nonatomic) IBOutlet UILabel *suomi_sub_title;
@property (strong, nonatomic) IBOutlet UIImageView *check_suomi;
@property (strong, nonatomic) IBOutlet UIButton *back;
- (IBAction)back:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *done;
- (IBAction)done:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *title_btn;

@end
