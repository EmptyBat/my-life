//
//  Reset_P_WriteUsername.h
//  My Life
//
//  Created by Admin on 28.09.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Reset_P_WriteUsername : UIViewController
@property (strong, nonatomic) IBOutlet UIButton *back;
- (IBAction)back:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *Write_name;
@property (strong, nonatomic) IBOutlet UIButton *done;
- (IBAction)done:(id)sender;

@end
