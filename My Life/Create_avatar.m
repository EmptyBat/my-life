//
//  Create_avatar.m
//  My Life
//
//  Created by Admin on 28.09.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "Create_avatar.h"
#import "SHModel.h"
#import "SWActionSheet.h"
#import "WaitingView.h"
#import <CommonCrypto/CommonHMAC.h>
#import "AFNetworking.h"
#import "Constant.h"
/*#import "ASIHTTPRequest.h"
#import <CommonCrypto/CommonHMAC.h>*/
@interface Create_avatar ()
{
    SHModel *sModel;
    WaitingView *wView;
}
@end

@implementation Create_avatar

- (void)viewDidLoad {
    [super viewDidLoad];
    sModel = [SHModel new];
    wView  = [WaitingView new];
  [self.name setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
  [self.password setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    [gestureRecognizer setCancelsTouchesInView:NO];
       [self.view addGestureRecognizer:gestureRecognizer];
    self.avatar.layer.cornerRadius = self.avatar.frame.size.width / 2;
    self.avatar.layer.borderWidth = 3.0f;
    self.avatar.layer.borderColor = [[UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1]CGColor];;
    self.avatar.clipsToBounds = YES;
    self.avatar.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapGesture1 = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapAction)];
    tapGesture1.numberOfTapsRequired = 1;
    [tapGesture1 setDelegate:self];
    [self.avatar addGestureRecognizer:tapGesture1];
    [self testS3];
    [self delegates];
}
-(void)delegates{
    self.name.delegate=self;
    self.password.delegate=self;
    
}
- (void)tapAction {
    UIActionSheet *select = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Отмена" destructiveButtonTitle:nil otherButtonTitles: @"Сделать фото",@"Выбрать фото", nil]; select.tag = 2; [select showInView:[UIApplication sharedApplication].keyWindow];
    
}
- ( void ) actionSheet : ( UIActionSheet * ) actionSheet clickedButtonAtIndex : ( NSInteger ) buttonIndex {
    if (buttonIndex==0){
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:picker animated:YES completion:NULL];
    }
    else if (buttonIndex==1) {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        [self presentViewController:picker animated:YES completion:NULL];
    }
    
}
/*- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    if ([navigationController.viewControllers count] == 3)
    {
        CGFloat screenHeight = [[UIScreen mainScreen] bounds].size.height;
        
        UIView *plCropOverlay = [[[viewController.view.subviews objectAtIndex:1]subviews] objectAtIndex:0];
        
        plCropOverlay.hidden = YES;
        
        int position = 0;
        
        if (screenHeight == 568)
        {
            position = 124;
        }
        else
        {
            position = 80;
        }
        
        CAShapeLayer *circleLayer = [CAShapeLayer layer];
        
        UIBezierPath *path2 = [UIBezierPath bezierPathWithOvalInRect:
                               CGRectMake(0.0f, position, 320.0f, 320.0f)];
        [path2 setUsesEvenOddFillRule:YES];
        
        [circleLayer setPath:[path2 CGPath]];
        
        [circleLayer setFillColor:[[UIColor clearColor] CGColor]];
        UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, 320, screenHeight-72) cornerRadius:0];
        
        [path appendPath:path2];
        [path setUsesEvenOddFillRule:YES];
        
        CAShapeLayer *fillLayer = [CAShapeLayer layer];
        fillLayer.path = path.CGPath;
        fillLayer.fillRule = kCAFillRuleEvenOdd;
        fillLayer.fillColor = [UIColor blackColor].CGColor;
        fillLayer.opacity = 0.8;
        [viewController.view.layer addSublayer:fillLayer];
        
        UILabel *moveLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 10, 320, 50)];
        [moveLabel setText:@"Выберите свое лицо"];
        [moveLabel setTextAlignment:NSTextAlignmentCenter];
        [moveLabel setTextColor:[UIColor whiteColor]];
        
        [viewController.view addSubview:moveLabel];
    }
}*/
- (void) imagePickerController: (UIImagePickerController *) picker didFinishPickingMediaWithInfo: (NSDictionary *) info
{
    UIImage *originalImage = (UIImage *) [info objectForKey:UIImagePickerControllerEditedImage]; //it returns the edited image,
    
    self.avatar.image=originalImage;
    [picker dismissViewControllerAnimated:YES completion:nil];
    
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)theTextField {
    if (theTextField==self.name){
        [self.password becomeFirstResponder];
    }
    else if (theTextField==self.password){
         [self.password resignFirstResponder];
        
    }

    return YES;
}
-(void)hideKeyboard{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (IBAction)back:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
}
static inline char itoh(int i) {
    if (i > 9) return 'A' + (i - 10);
    return '0' + i;
}
NSString * NSDataToHex(NSData *data) {
    NSUInteger i, len;
    unsigned char *buf, *bytes;
    
    len = data.length;
    bytes = (unsigned char*)data.bytes;
    buf = malloc(len*2);
    
    for (i=0; i<len; i++) {
        buf[i*2] = itoh((bytes[i] >> 4) & 0xF);
        buf[i*2+1] = itoh(bytes[i] & 0xF);
    }
    
    return [[NSString alloc] initWithBytesNoCopy:buf
                                          length:len*2
                                        encoding:NSASCIIStringEncoding
                                    freeWhenDone:YES];
}
- (void)testS3
{
    
<<<<<<< HEAD
    [wView addWaitinViewToView:self.view];
    NSData* datas = UIImagePNGRepresentation(self.avatar.image);
=======

    NSData* datas = UIImagePNGRepresentation(self.avatar.image);
    NSMutableData *macOut = [NSMutableData dataWithLength:CC_SHA256_DIGEST_LENGTH];
>>>>>>> parent of f7c153f... AWS integration &&More requests&&UI updated.
    
    NSMutableData *macOut = [NSMutableData dataWithLength:CC_SHA256_DIGEST_LENGTH];
    CC_SHA256(datas.bytes, datas.length,  macOut.mutableBytes);
    NSString*contentHashString=NSDataToHex(macOut);
    NSLog(@"dataIn: %@", contentHashString);
    NSLog(@"macOut: %@", macOut);
    NSLog(@"%lu",(unsigned long)datas.bytes);
      NSString *token = [[NSUserDefaults standardUserDefaults] stringForKey:@"token"];
    NSString*hash=[self base64forData:datas];
      NSString*params=[NSString stringWithFormat:@"{'contentHashString':'%@','contentLength' : '%@'}",contentHashString,[NSString stringWithFormat:@"%lu",(unsigned long)macOut.length]];
        NSLog(@"======%@",params);
        NSString*urls=[NSString stringWithFormat:@"%@reg-server/services/AwsToken",mainURL];
            NSLog(@"=====%@",urls);
        NSURL* url = [NSURL URLWithString:urls];
        NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url];
        request.HTTPMethod = @"POST";
        [request setValue:token forHTTPHeaderField:@"auth"];
        request.HTTPBody = [params dataUsingEncoding:NSUTF8StringEncoding];
        NSURLResponse *responce;
        NSData *GETReply = [NSURLConnection sendSynchronousRequest:request returningResponse:&responce error:nil];
        NSString *theReply = [[NSString alloc] initWithBytes:[GETReply bytes] length:[GETReply length] encoding:NSASCIIStringEncoding];
        NSError* error;
        NSLog(@"+++++++%@",theReply);
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:GETReply
                                                             options:kNilOptions
                                                               error:&error];
        NSNumber * isSuccessNumber = (NSNumber *)[json objectForKey: @"success"];
    NSString *authorization = [json objectForKey:@"authorization"];
     NSString *endpointUrl = [json objectForKey:@"endpointUrl"];
   NSString *resourceId = [json objectForKey:@"resourceId"];
    NSString *errorDesc = [json objectForKey:@"errorDesc"];
<<<<<<< HEAD
        NSString *dates = [json objectForKey:@"x-amz-date"];
      NSString *Host = [json objectForKey:@"Host"];
=======
      [[[UIAlertView alloc] initWithTitle:nil message:errorDesc delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
>>>>>>> parent of f7c153f... AWS integration &&More requests&&UI updated.
        if([isSuccessNumber boolValue] == YES){
            NSMutableURLRequest *uploadRequest = [[NSMutableURLRequest alloc] init];
            [uploadRequest setURL:[NSURL URLWithString:endpointUrl]];
            uploadRequest.cachePolicy = NSURLRequestReloadIgnoringLocalCacheData;
            [uploadRequest setHTTPMethod:@"PUT"];
            [uploadRequest setValue:@"image/png" forHTTPHeaderField:@"Content-Type"];
                [uploadRequest setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[datas length]] forHTTPHeaderField:@"Content-Length"];
            [uploadRequest setHTTPBody:datas];
<<<<<<< HEAD
            [uploadRequest setValue:authorization forHTTPHeaderField:@"Authorization"];
           [uploadRequest setValue:contentHashString forHTTPHeaderField:@"x-amz-content-sha256"];
           [uploadRequest setValue:@"REDUCED_REDUNDANCY" forHTTPHeaderField:@"x-amz-storage-class"];
              [uploadRequest setValue:dates forHTTPHeaderField:@"x-amz-date"];
            [uploadRequest setValue:Host forHTTPHeaderField:@"Host"];
 
            NSURLSession *session = [NSURLSession sharedSession];
            NSLog(@"Requet===%@", [uploadRequest allHTTPHeaderFields]);
                        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:uploadRequest completionHandler:^(NSData * _Nullable datae, NSURLResponse * _Nullable responses, NSError * _Nullable error) {
            NSString *theReplys = [[NSString alloc] initWithBytes:[datae bytes] length:[datae length] encoding:NSASCIIStringEncoding];
            NSLog(@"++***%@",theReplys);
            NSLog(@"++***%@",responses);
=======
            [uploadRequest setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[datas length]] forHTTPHeaderField:@"Content-Length"];
            [uploadRequest setValue:@"image/png" forHTTPHeaderField:@"Content-Type"];
             //  [uploadRequest setValue:authorization forHTTPHeaderField:@"Authorization"];
            NSURLSession *session = [NSURLSession sharedSession];
            NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:uploadRequest completionHandler:^(NSData * _Nullable datae, NSURLResponse * _Nullable responses, NSError * _Nullable error) {
                NSLog(@"%@",responses);
                 NSLog(@"%@",datae);;
>>>>>>> parent of f7c153f... AWS integration &&More requests&&UI updated.
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            }];
            [dataTask resume];
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init] ;
            [dateFormatter setDateFormat:@"EEE, d MMM yyyy HH:mm:ss zzzz"];
            NSString *date = [dateFormatter stringFromDate:[NSDate date]];
         //   [manager.requestSerializer setValue:[NSString stringWithFormat:@"%@",datas] forHTTPHeaderField:@"Expect"];
        }
          /*  [manager.requestSerializer setValue:@"image/png" forHTTPHeaderField:@"Content-Type"];
            [manager.requestSerializer setValue:authorization forHTTPHeaderField:@"Authorization"];
            [manager.requestSerializer setValue:contentHashString forHTTPHeaderField:@"x-amz-content-sha256"];
             [manager.requestSerializer setValue:date forHTTPHeaderField:@"Date"];
            [manager.requestSerializer setValue:[NSString stringWithFormat:@"%lu",(unsigned long)datas.length] forHTTPHeaderField:@"content-length"];
            [manager.requestSerializer setValue:@"REDUCED_REDUNDANCY" forHTTPHeaderField:@"x-amz-storage-class"];
            NSURLSessionDataTask *task = [manager dataTaskWithRequest:request uploadProgress:nil downloadProgress:nil completionHandler:^(NSURLResponse *

*/



    //      [[[UIAlertView alloc] initWithTitle:nil message:errorDesc delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
    
 /*  NSString *filePath = @"/path/to/file";
    NSString *contentType = @"text/plain";
    NSString *bucket = @"mybucket";
    NSString *path = @"test";
    NSString *secretAccessKey = @"my-secret-access-key";
    NSString *accessKey = @"my-access-key";
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init] ;
    [dateFormatter setDateFormat:@"EEE, d MMM yyyy HH:mm:ss zzzz"];
    NSString *date = [dateFormatter stringFromDate:[NSDate date]];
    
    ASIHTTPRequest *request = [[[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@.s3.amazonaws.com/%@",bucket,path]]] autorelease];
    [request setPostBodyFilePath:filePath];
    [request setShouldStreamPostDataFromDisk:YES];
    [request setRequestMethod:@"PUT"];
    
    [request addRequestHeader:@"x-amz-acl" value:@"private"];
    [request addRequestHeader:@"Content-Type" value:contentType];
    [request addRequestHeader:@"Date" value:date];
    
    NSString *canonicalizedAmzHeaders = @"x-amz-acl:private";
    NSString *canonicalizedResource = [NSString stringWithFormat:@"/%@/%@",bucket,path];
    NSString *stringToSign = [NSString stringWithFormat:@"PUT\n\n%@\n%@\n%@\n%@",contentType,date,canonicalizedAmzHeaders,canonicalizedResource];
    
    NSString *signature = [self base64forData:[self HMACSHA1withKey:secretAccessKey forString:stringToSign]];
    NSString *auth = [NSString stringWithFormat:@"AWS %@:%@",accessKey,signature];
    [request addRequestHeader:@"Authorization" value:auth];
    
    
    [request start];
    NSLog(@"%@",[request responseString]);*/
    
}

/*  headers.put("x-amz-content-sha256", contentHashString);
headers.put("content-length", "" + objectContent.length());
headers.put("x-amz-storage-class", "REDUCED_REDUNDANCY");

AWS4SignerForAuthorizationHeader signer = new AWS4SignerForAuthorizationHeader(
                                                                               endpointUrl, "PUT", "s3", regionName);
String authorization = signer.computeSignature(headers,
                                               null, // no query parameters
                                               contentHashString,
                                               awsAccessKey,
                                               awsSecretKey);
*/
// express authorization for this as a header
// Source: http://stackoverflow.com/questions/476455/is-there-a-library-for-iphone-to-work-with-hmac-sha-1-encoding

- (NSData *)HMACSHA1withKey:(NSString *)key forString:(NSString *)string
{
    NSData *clearTextData = [string dataUsingEncoding:NSUTF8StringEncoding];
    NSData *keyData = [key dataUsingEncoding:NSUTF8StringEncoding];
    
    uint8_t digest[CC_SHA1_DIGEST_LENGTH] = {0};
    
    CCHmacContext hmacContext;
    CCHmacInit(&hmacContext, kCCHmacAlgSHA1, keyData.bytes, keyData.length);
    CCHmacUpdate(&hmacContext, clearTextData.bytes, clearTextData.length);
    CCHmacFinal(&hmacContext, digest);
    
    return [NSData dataWithBytes:digest length:CC_SHA1_DIGEST_LENGTH];
}

//Source http://www.cocoadev.com/index.pl?BaseSixtyFour

- (NSString *)base64forData:(NSData *)data
{
    static const char encodingTable[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
    
    if ([data length] == 0)
        return @"";
    
    char *characters = malloc((([data length] + 2) / 3) * 4);
    if (characters == NULL)
        return nil;
    NSUInteger length = 0;
    
    NSUInteger i = 0;
    while (i < [data length])
    {
        char buffer[3] = {0,0,0};
        short bufferLength = 0;
        while (bufferLength < 3 && i < [data length])
            buffer[bufferLength++] = ((char *)[data bytes])[i++];
        
        //  Encode the bytes in the buffer to four characters, including padding "=" characters if necessary.
        characters[length++] = encodingTable[(buffer[0] & 0xFC) >> 2];
        characters[length++] = encodingTable[((buffer[0] & 0x03) << 4) | ((buffer[1] & 0xF0) >> 4)];
        if (bufferLength > 1)
            characters[length++] = encodingTable[((buffer[1] & 0x0F) << 2) | ((buffer[2] & 0xC0) >> 6)];
        else characters[length++] = '=';
        if (bufferLength > 2)
            characters[length++] = encodingTable[buffer[2] & 0x3F];
        else characters[length++] = '=';    
    }
    
     return [[NSString alloc] initWithBytesNoCopy:characters length:length encoding:NSASCIIStringEncoding freeWhenDone:YES] ;
 }
- (IBAction)next:(id)sender {
//    [wView addWaitinViewToView:self.view];
    if ([_phone isEqualToString:@""]){
    [sModel registrationDataEmail:self.email  password:self.password.text   surname:self.name.text name:self.username  completion:^(BOOL success, NSString *error) {
        NSLog(success ? @"Yes" : @"No");
        if ( success ) {
            
            [self performSegueWithIdentifier:@"home" sender:self];
            
        }
        else {
            
        }
        [wView removeWaitActivity];
    }];
    }
    else {
    
        [sModel registrationDataPhone:self.phone  password:self.password.text   surname:self.name.text name:self.username  completion:^(BOOL success, NSString *error) {
            NSLog(success ? @"Yes" : @"No");
            if ( success ) {
                
                 [self performSegueWithIdentifier:@"home" sender:self];
                
            }
            else {
                
            }
            [wView removeWaitActivity];
        }];
    }
}
@end
