//
//  Select_Autch.h
//  My Life
//
//  Created by Admin on 28.09.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Select_Autch : UIViewController
- (IBAction)Back:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *Back;
@property (strong, nonatomic) IBOutlet UIButton *facebook_btn;
- (IBAction)facebook_btn:(id)sender;
- (IBAction)vk_btn:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *vk_btn;
@end
