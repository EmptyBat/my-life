//
//  Constant.h
//  My Life
//
//  Created by Admin on 05.12.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#ifndef Constant_h
#define Constant_h


#endif /* Constant_h */
#define serverErrorString @"Ошибка при получении данных с сервера"



#define RGBA(r,g,b,a)   [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]
#define WA(w,a)         [UIColor colorWithWhite:w alpha:a]
#define Scn_Width       [[UIScreen mainScreen] bounds].size.width
#define Scn_Height      [[UIScreen mainScreen] bounds].size.height
#define APP             (AppDelegate *)[[UIApplication sharedApplication] delegate]
#define Scn_Center      CGPointMake(Scn_Width/2,Scn_Height/2)



// Colors
#define tabBarColor             RGBA(31,170,240,1)
#define tGreenColor             RGBA(17,173,111,1)
#define linesColor              RGBA(209,209,209,1)
#define darkPrimaryColor        RGBA(22,137,204,1)
#define primaryColor            RGBA(31,170,240,1)
#define lightPrimaryColor       RGBA(182,230,250,1)
#define mainTextColor           RGBA(255,255,255,1)
#define accentColor             RGBA(250,67,128,1)
#define primaryTextColor        RGBA(85,85,85,1)
#define secondaryTextColor      RGBA(179,179,179,1)
#define dividerColor            RGBA(181,181,181,1)

// Fonts
#define robotoFont(a) [UIFont fontWithName:@"Roboto" size:a]
#define mainFontBold(a) [UIFont fontWithName:@"MyriadPro-Bold" size:a]
#define mainFontSemibold(a) [UIFont fontWithName:@"MyriadPro-Semibold" size:a]

#define mainURL     @"http://mylistory.com:8080/"



#define str(a)              [NSString stringWithFormat:@"%@",a]
#define strFormat(a,b)      [NSString stringWithFormat:a,b]



// Threads
#define BG_THREAD dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^
#define FG_THREAD dispatch_async(dispatch_get_main_queue(), ^


#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)
