//
//  User_list.h
//  My Life
//
//  Created by Admin on 13.10.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface User_list : UIViewController
@property (strong, nonatomic) IBOutlet UITableView *tableVIew;
@property (strong, nonatomic) IBOutlet UIView *share;
@property (strong, nonatomic) IBOutlet UICollectionView *collection;
- (IBAction)back:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *back;
- (IBAction)share:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *title_btn;
- (IBAction)like_btn:(id)sender;
- (IBAction)actionSheet_show:(id)sender;
- (IBAction)action_btn_1:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *action_btn_1;
@property (strong, nonatomic) IBOutlet UIButton *action_btn_2;
- (IBAction)action_btn_2:(id)sender;
- (IBAction)action_btn_3:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *action_btn_3;
@property (strong, nonatomic) IBOutlet UIButton *action_btn_4;
- (IBAction)action_btn_4:(id)sender;
- (IBAction)action_btn_5:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *actionsheet;
@property (strong, nonatomic) IBOutlet UIButton *action_btn_5;
@end
