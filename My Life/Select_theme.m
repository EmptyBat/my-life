//
//  Select_theme.m
//  My Life
//
//  Created by Admin on 05.10.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "Select_theme.h"

@interface Select_theme ()
{
    UIColor*tableCellColor;
}
@end

@implementation Select_theme

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.tableView.bounds.size.width, 0.01f)];
    NSString *theme = [[NSUserDefaults standardUserDefaults] stringForKey:@"theme"];
    if ([theme isEqualToString:@"black_diamond"]){
        [self black_diamond_style];
    }
    else  if ([theme isEqualToString:@"sea_blue"]){
        [self sea_blue_style];
    }
    else  if ([theme isEqualToString:@"soft_pink"]){
        [self soft_pink_style];
    }
    else  if ([theme isEqualToString:@"green"]){
         [self green_style];
    }
    else {
        [self black_pearl_style];
    }
}
- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    if([view isKindOfClass:[UITableViewHeaderFooterView class]]){
        UITableViewHeaderFooterView *tableViewHeaderFooterView = (UITableViewHeaderFooterView *) view;
        tableViewHeaderFooterView.textLabel.text = [tableViewHeaderFooterView.textLabel.text capitalizedString];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    [cell setBackgroundColor:tableCellColor];
    [cell.contentView setBackgroundColor:tableCellColor];
    if (tableCellColor==nil){
        [cell setBackgroundColor:[UIColor whiteColor]];
        [cell.contentView setBackgroundColor:[UIColor whiteColor]];
    }
}
- (IBAction)back:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)black_pearl:(id)sender {
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    [user setObject:@"black_pearl" forKey:@"theme"];
    [user synchronize];
    [self.tableView reloadData];
    [self black_pearl_style];
}
- (IBAction)black_diamond:(id)sender {
     self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    [user setObject:@"black_diamond" forKey:@"theme"];
    [user synchronize];
    [self.tableView reloadData];
    [self black_diamond_style];

}
- (IBAction)sea_blue:(id)sender {
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    [user setObject:@"sea_blue" forKey:@"theme"];
    [user synchronize];
    [self.tableView reloadData];
    [self sea_blue_style];
}

- (IBAction)soft_pink:(id)sender {
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    [user setObject:@"soft_pink" forKey:@"theme"];
    [user synchronize];
    [self.tableView reloadData];
    [self soft_pink_style];
}
- (IBAction)green:(id)sender {
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    [user setObject:@"green" forKey:@"theme"];
    [user synchronize];
    [self.tableView reloadData];
    [self green_style];
}
-(void)black_pearl_style{
    [_title_btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    tableCellColor=[UIColor whiteColor];
      self.navigationController.navigationBar.barStyle = UIStatusBarStyleDefault;
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1.0];;
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor blackColor]};
    self.tableView.backgroundColor =[UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1.0];
    _black_pearl.tintColor =[UIColor whiteColor];
    _black_diamond.tintColor =[UIColor whiteColor];
    _sea_blue.tintColor =[UIColor whiteColor];
    _soft_pink.tintColor =[UIColor whiteColor];
    _green.tintColor =[UIColor whiteColor];
    [self.done setTitleColor:[UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1] forState:UIControlStateNormal];
    UIImage *image = [[UIImage imageNamed:@"YellowBack"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.back setImage:image forState:UIControlStateNormal];
    self.back.tintColor = [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    _black_pearl.onTintColor =[UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    _black_diamond.onTintColor =[UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    _sea_blue.onTintColor =[UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    _soft_pink.onTintColor =[UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    _green.onTintColor =[UIColor blackColor];
    _black_pearl_label.textColor=[UIColor blackColor];
    _black_diamon_label.textColor=[UIColor blackColor];
    _sea_blue_label.textColor=[UIColor blackColor];
    _soft_pink_label.textColor=[UIColor blackColor];
    _green_label.textColor=[UIColor blackColor];
    self.tabBarController.tabBar.barTintColor = [UIColor whiteColor];
    self.tabBarController.tabBar.tintColor = [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    _black_pearl.on=true;
    _black_diamond.on=false;
    _sea_blue.on=false;
    _soft_pink.on=false;
    _green.on=false;
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"THEME"
     object:nil];
}
-(void)black_diamond_style{
    [_title_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleBlackOpaque;
    tableCellColor=[UIColor blackColor];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor =[UIColor blackColor];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    self.tableView.backgroundColor =[UIColor colorWithRed:23.0/255 green:23.0/255 blue:23.0/255 alpha:1.0];
    _black_pearl.tintColor =[UIColor blackColor];
    _black_diamond.tintColor =[UIColor blackColor];
    _sea_blue.tintColor =[UIColor blackColor];
    _soft_pink.tintColor =[UIColor blackColor];
    _green.tintColor =[UIColor blackColor];
      [self.done setTitleColor:[UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1] forState:UIControlStateNormal];
    UIImage *image = [[UIImage imageNamed:@"YellowBack"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.back setImage:image forState:UIControlStateNormal];
    self.back.tintColor = [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    _black_pearl.onTintColor =[UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    _black_diamond.onTintColor =[UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    _sea_blue.onTintColor =[UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    _soft_pink.onTintColor =[UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    _green.onTintColor =[UIColor blackColor];
    _black_pearl_label.textColor=[UIColor whiteColor];
    _black_diamon_label.textColor=[UIColor whiteColor];
    _sea_blue_label.textColor=[UIColor whiteColor];
    _soft_pink_label.textColor=[UIColor whiteColor];
    _green_label.textColor=[UIColor whiteColor];
    self.tabBarController.tabBar.barTintColor = [UIColor blackColor];
    self.tabBarController.tabBar.tintColor = [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    _black_pearl.on=false;
    _black_diamond.on=true;
    _sea_blue.on=false;
    _soft_pink.on=false;
    _green.on=false;
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"THEME"
     object:nil];
    
}
-(void)sea_blue_style{
    [_title_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:2.0/255 green:35.0/255 blue:143.0/255 alpha:1.0];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    self.tableView.backgroundColor =[UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1.0];
      [self.done setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    UIImage *image = [[UIImage imageNamed:@"YellowBack"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.back setImage:image forState:UIControlStateNormal];
    self.back.tintColor = [UIColor whiteColor];
    _black_pearl.tintColor =[UIColor whiteColor];
    _black_diamond.tintColor =[UIColor whiteColor];
    _sea_blue.tintColor =[UIColor whiteColor];
    _soft_pink.tintColor =[UIColor whiteColor];
    _green.tintColor =[UIColor whiteColor];
    tableCellColor=[UIColor whiteColor];
    self.tabBarController.tabBar.barTintColor =[UIColor whiteColor];
    self.tabBarController.tabBar.tintColor = [UIColor colorWithRed:2.0/255 green:35.0/255 blue:143.0/255 alpha:1.0];
    _black_pearl.onTintColor =[UIColor colorWithRed:2.0/255 green:35.0/255 blue:143.0/255 alpha:1.0];
    _black_diamond.onTintColor =[UIColor colorWithRed:2.0/255 green:35.0/255 blue:143.0/255 alpha:1.0];
    _sea_blue.onTintColor =[UIColor colorWithRed:2.0/255 green:35.0/255 blue:143.0/255 alpha:1.0];
    _soft_pink.onTintColor =[UIColor colorWithRed:2.0/255 green:35.0/255 blue:143.0/255 alpha:1.0];
    _green.onTintColor =[UIColor colorWithRed:2.0/255 green:35.0/255 blue:143.0/255 alpha:1.0];
    _black_pearl_label.textColor=[UIColor blackColor];
    _black_diamon_label.textColor=[UIColor blackColor];
    _sea_blue_label.textColor=[UIColor blackColor];
    _soft_pink_label.textColor=[UIColor blackColor];
    _green_label.textColor=[UIColor blackColor];
    _black_pearl.on=false;
    _black_diamond.on=false;
    _sea_blue.on=true;
    _soft_pink.on=false;
    _green.on=false;
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"THEME"
     object:nil];
}
-(void)soft_pink_style{
    [_title_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:231.0/255 green:194.0/255 blue:202.0/255 alpha:1.0];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    self.tableView.backgroundColor =[UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1.0];
      [self.done setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    UIImage *image = [[UIImage imageNamed:@"YellowBack"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.back setImage:image forState:UIControlStateNormal];
    self.back.tintColor = [UIColor whiteColor];
    _black_pearl.tintColor =[UIColor whiteColor];
    _black_diamond.tintColor =[UIColor whiteColor];
    _sea_blue.tintColor =[UIColor whiteColor];
    _soft_pink.tintColor =[UIColor whiteColor];
    _green.tintColor =[UIColor whiteColor];
    tableCellColor=[UIColor whiteColor];
    self.tabBarController.tabBar.barTintColor =[UIColor whiteColor];
    self.tabBarController.tabBar.tintColor =[UIColor colorWithRed:231.0/255 green:194.0/255 blue:202.0/255 alpha:1.0];
    _black_pearl.onTintColor =[UIColor colorWithRed:231.0/255 green:194.0/255 blue:202.0/255 alpha:1.0];
    _black_diamond.onTintColor =[UIColor colorWithRed:231.0/255 green:194.0/255 blue:202.0/255 alpha:1.0];
    _sea_blue.onTintColor =[UIColor colorWithRed:231.0/255 green:194.0/255 blue:202.0/255 alpha:1.0];
    _soft_pink.onTintColor =[UIColor colorWithRed:231.0/255 green:194.0/255 blue:202.0/255 alpha:1.0];
    _green.onTintColor =[UIColor colorWithRed:231.0/255 green:194.0/255 blue:202.0/255 alpha:1.0];
    _black_pearl_label.textColor=[UIColor blackColor];
    _black_diamon_label.textColor=[UIColor blackColor];
    _sea_blue_label.textColor=[UIColor blackColor];
    _soft_pink_label.textColor=[UIColor blackColor];
    _green_label.textColor=[UIColor blackColor];
    _black_pearl.on=false;
    _black_diamond.on=false;
    _sea_blue.on=false;
    _soft_pink.on=true;
    _green.on=false;
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"THEME"
     object:nil];
    
}
-(void)green_style{
    [_title_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:23.0/255 green:125.0/255 blue:46.0/255 alpha:1.0];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    self.tableView.backgroundColor =[UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1.0];
    [self.done setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];;
    UIImage *image = [[UIImage imageNamed:@"YellowBack"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.back setImage:image forState:UIControlStateNormal];
    self.back.tintColor = [UIColor whiteColor];
    _black_pearl.tintColor =[UIColor whiteColor];
    _black_diamond.tintColor =[UIColor whiteColor];
    _sea_blue.tintColor =[UIColor whiteColor];
    _soft_pink.tintColor =[UIColor whiteColor];
    _green.tintColor =[UIColor whiteColor];
    tableCellColor=[UIColor whiteColor];
    self.tabBarController.tabBar.barTintColor =[UIColor whiteColor];
    self.tabBarController.tabBar.tintColor = [UIColor colorWithRed:23.0/255 green:125.0/255 blue:46.0/255 alpha:1.0];
    _black_pearl.onTintColor =[UIColor colorWithRed:23.0/255 green:125.0/255 blue:46.0/255 alpha:1.0];
    _black_diamond.onTintColor =[UIColor colorWithRed:23.0/255 green:125.0/255 blue:46.0/255 alpha:1.0];
    _sea_blue.onTintColor =[UIColor colorWithRed:23.0/255 green:125.0/255 blue:46.0/255 alpha:1.0];
    _soft_pink.onTintColor =[UIColor colorWithRed:23.0/255 green:125.0/255 blue:46.0/255 alpha:1.0];
    _green.onTintColor =[UIColor colorWithRed:23.0/255 green:125.0/255 blue:46.0/255 alpha:1.0];;
    _black_pearl_label.textColor=[UIColor blackColor];
    _black_diamon_label.textColor=[UIColor blackColor];
    _sea_blue_label.textColor=[UIColor blackColor];
    _soft_pink_label.textColor=[UIColor blackColor];
    _green_label.textColor=[UIColor blackColor];
    _black_pearl.on=false;
    _black_diamond.on=false;
    _sea_blue.on=false;
    _soft_pink.on=false;
    _green.on=true;
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"THEME"
     object:nil];

}
@end
