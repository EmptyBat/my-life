//
//  FoundFriends.h
//  My Life
//
//  Created by Admin on 28.09.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FoundFriends : UIViewController
@property (strong, nonatomic) IBOutlet UIButton *back;
- (IBAction)back:(id)sender;
- (IBAction)sub:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *sub;
- (IBAction)next:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *next;

@end
