//
//  NewMessage.h
//  My Life
//
//  Created by Admin on 12.10.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MessageComposerView.h"
@interface NewMessage : UIViewController<MessageComposerViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)back:(id)sender;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *next;
@property (strong, nonatomic) IBOutlet UIButton *back;
@property (strong, nonatomic) IBOutlet UILabel *whom;
@property (strong, nonatomic) IBOutlet UIButton *search;
@property (strong, nonatomic) IBOutlet UIButton *fake_btn_1;
@property (strong, nonatomic) IBOutlet UIButton *fake_btn_2;
@property (strong, nonatomic) IBOutlet UIButton *title_btn;
@property (strong, nonatomic) IBOutlet UICollectionView *Collection;
@property (nonatomic, strong) MessageComposerView *messageComposerView;
@end
