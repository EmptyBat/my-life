//
//  Collection_View_tab.h
//  My Life
//
//  Created by Admin on 08.11.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Collection_View_tab : UIView
{NSString*str;
    BOOL edit;
    UIImage*img;}
- (void)setCollectionData:(NSArray *)collectionData;
@end
@interface UIImage (PhoenixMaster)
- (UIImage *) makeThumbnailOfSize:(CGSize)size;
@end
