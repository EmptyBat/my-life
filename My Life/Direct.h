//
//  Direct.h
//  My Life
//
//  Created by Admin on 12.10.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Direct : UIViewController
@property (strong, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)back:(id)sender;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *add;
@property (strong, nonatomic) IBOutlet UIButton *back;
@property (strong, nonatomic) IBOutlet UIButton *title_btn;

@end
