//
//  change_password.m
//  My Life
//
//  Created by Admin on 05.10.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "change_password.h"

@interface change_password ()
{
    UIColor*tableCellColor;
    UIColor*textColor;
    UIColor*imageColor;
    UIColor*textColor_main;
}
@end

@implementation change_password

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSString *theme = [[NSUserDefaults standardUserDefaults] stringForKey:@"theme"];
    if ([theme isEqualToString:@"black_diamond"]){
        [self black_diamond_style];
    }
    else  if ([theme isEqualToString:@"sea_blue"]){
        [self sea_blue_style];
    }
    else  if ([theme isEqualToString:@"soft_pink"]){
        [self soft_pink_style];
    }
    else  if ([theme isEqualToString:@"green"]){
        [self green_style];
    }
    else {
        [self black_pearl_style];
    }
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(THEME:)
                                                 name:@"THEME" object:nil];
    
}
- (void)THEME:(NSNotification *)notice{
    NSString *theme = [[NSUserDefaults standardUserDefaults] stringForKey:@"theme"];
    if ([theme isEqualToString:@"black_diamond"]){
        [self black_diamond_style];
    }
    else  if ([theme isEqualToString:@"sea_blue"]){
        [self sea_blue_style];
    }
    else  if ([theme isEqualToString:@"soft_pink"]){
        [self soft_pink_style];
    }
    else  if ([theme isEqualToString:@"green"]){
        [self green_style];
    }
    else {
        [self black_pearl_style];
    }
    
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    [cell setBackgroundColor:tableCellColor];
    [cell.contentView setBackgroundColor:tableCellColor];
    if (tableCellColor==nil){
        [cell setBackgroundColor:[UIColor whiteColor]];
        [cell.contentView setBackgroundColor:[UIColor whiteColor]];
    }
}
-(void)black_pearl_style{
    tableCellColor=[UIColor whiteColor];
    textColor = [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    textColor_main=[UIColor blackColor];
    
    tableCellColor=[UIColor whiteColor];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleDefault;
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1.0];;
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor blackColor]};
    self.tableView.backgroundColor =[UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1.0];
   [_title_btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    UIImage *image = [[UIImage imageNamed:@"YellowBack"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.back setImage:image forState:UIControlStateNormal];
    self.back.tintColor = textColor;
    [self.done setTitleColor:textColor forState:UIControlStateNormal];
    UIImage *image1 = [[UIImage imageNamed:@"lock"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.lock_1.image=image1;
    self.lock_1.tintColor = textColor;
    self.lock_2.image=image1;
    self.lock_2.tintColor = textColor;
    UIImage *image2 = [[UIImage imageNamed:@"2_locker"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.lock_3.image=image2;
    self.lock_3.tintColor = textColor;
    self.current_password.textColor=textColor_main;
    self.field_new_password.textColor=textColor_main;
    self.return_new_password.textColor=textColor_main;
    [self.current_password setValue:textColor_main forKeyPath:@"_placeholderLabel.textColor"];
    [self.field_new_password setValue:textColor_main forKeyPath:@"_placeholderLabel.textColor"];
    [self.return_new_password setValue:textColor_main forKeyPath:@"_placeholderLabel.textColor"];
    [self.tableView reloadData];
}
-(void)black_diamond_style{
    tableCellColor=[UIColor blackColor];
    textColor = [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
    textColor_main=[UIColor whiteColor];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleBlackOpaque;
    tableCellColor=[UIColor blackColor];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor =[UIColor blackColor];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    self.tableView.backgroundColor =[UIColor colorWithRed:23.0/255 green:23.0/255 blue:23.0/255 alpha:1.0];
    self.tabBarController.tabBar.barTintColor = [UIColor blackColor];
    self.tabBarController.tabBar.tintColor = [UIColor colorWithRed:190.0/255.0 green:156.0/255.0 blue:87.0/255.0 alpha:1];
     [_title_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    UIImage *image = [[UIImage imageNamed:@"YellowBack"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.back setImage:image forState:UIControlStateNormal];
    self.back.tintColor = textColor;
    [self.done setTitleColor:textColor forState:UIControlStateNormal];
    UIImage *image1 = [[UIImage imageNamed:@"lock"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.lock_1.image=image1;
    self.lock_1.tintColor = textColor;
    self.lock_2.image=image1;
    self.lock_2.tintColor = textColor;
    UIImage *image2 = [[UIImage imageNamed:@"2_locker"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.lock_3.image=image2;
    self.lock_3.tintColor = textColor;
    self.current_password.textColor=textColor_main;
    self.field_new_password.textColor=textColor_main;
    self.return_new_password.textColor=textColor_main;
    [self.current_password setValue:textColor_main forKeyPath:@"_placeholderLabel.textColor"];
    [self.field_new_password setValue:textColor_main forKeyPath:@"_placeholderLabel.textColor"];
    [self.return_new_password setValue:textColor_main forKeyPath:@"_placeholderLabel.textColor"];
    [self.tableView reloadData];
    
}
-(void)sea_blue_style{
    tableCellColor=[UIColor whiteColor];
    textColor = [UIColor colorWithRed:2.0/255 green:35.0/255 blue:143.0/255 alpha:1.0];
    textColor_main=[UIColor blackColor];
    
    tableCellColor=[UIColor whiteColor];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:2.0/255 green:35.0/255 blue:143.0/255 alpha:1.0];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    self.tableView.backgroundColor =[UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1.0];
    tableCellColor=[UIColor whiteColor];
    self.tabBarController.tabBar.barTintColor =[UIColor whiteColor];
    self.tabBarController.tabBar.tintColor = [UIColor colorWithRed:2.0/255 green:35.0/255 blue:143.0/255 alpha:1.0];
     [_title_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    UIImage *image = [[UIImage imageNamed:@"YellowBack"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.back setImage:image forState:UIControlStateNormal];
    self.back.tintColor = [UIColor whiteColor];
    [self.done setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    UIImage *image1 = [[UIImage imageNamed:@"lock"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.lock_1.image=image1;
    self.lock_1.tintColor = textColor;
    self.lock_2.image=image1;
    self.lock_2.tintColor = textColor;
    UIImage *image2 = [[UIImage imageNamed:@"2_locker"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.lock_3.image=image2;
    self.lock_3.tintColor = textColor;
    self.current_password.textColor=textColor_main;
    self.field_new_password.textColor=textColor_main;
    self.return_new_password.textColor=textColor_main;
    [self.current_password setValue:textColor_main forKeyPath:@"_placeholderLabel.textColor"];
    [self.field_new_password setValue:textColor_main forKeyPath:@"_placeholderLabel.textColor"];
    [self.return_new_password setValue:textColor_main forKeyPath:@"_placeholderLabel.textColor"];
    [self.tableView reloadData];
}
-(void)soft_pink_style{
    tableCellColor=[UIColor whiteColor];
    textColor = [UIColor colorWithRed:231.0/255 green:194.0/255 blue:202.0/255 alpha:1.0];
    textColor_main=[UIColor blackColor];
    
    tableCellColor=[UIColor whiteColor];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:231.0/255 green:194.0/255 blue:202.0/255 alpha:1.0];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    self.tableView.backgroundColor =[UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1.0];
    tableCellColor=[UIColor whiteColor];
    self.tabBarController.tabBar.barTintColor =[UIColor whiteColor];
    self.tabBarController.tabBar.tintColor =[UIColor colorWithRed:231.0/255 green:194.0/255 blue:202.0/255 alpha:1.0];
    [_title_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    UIImage *image = [[UIImage imageNamed:@"YellowBack"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.back setImage:image forState:UIControlStateNormal];
    self.back.tintColor = [UIColor whiteColor];
    [self.done setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    UIImage *image1 = [[UIImage imageNamed:@"lock"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.lock_1.image=image1;
    self.lock_1.tintColor = textColor;
    self.lock_2.image=image1;
    self.lock_2.tintColor = textColor;
    UIImage *image2 = [[UIImage imageNamed:@"2_locker"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.lock_3.image=image2;
    self.lock_3.tintColor = textColor;
    self.current_password.textColor=textColor_main;
    self.field_new_password.textColor=textColor_main;
    self.return_new_password.textColor=textColor_main;
    [self.current_password setValue:textColor_main forKeyPath:@"_placeholderLabel.textColor"];
    [self.field_new_password setValue:textColor_main forKeyPath:@"_placeholderLabel.textColor"];
    [self.return_new_password setValue:textColor_main forKeyPath:@"_placeholderLabel.textColor"];
    [self.tableView reloadData];
    
}
-(void)green_style{
    tableCellColor=[UIColor whiteColor];
    textColor = [UIColor colorWithRed:23.0/255 green:125.0/255 blue:46.0/255 alpha:1.0];
    textColor_main=[UIColor blackColor];
    
    tableCellColor=[UIColor whiteColor];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:23.0/255 green:125.0/255 blue:46.0/255 alpha:1.0];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    self.tableView.backgroundColor =[UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1.0];
    tableCellColor=[UIColor whiteColor];
    self.tabBarController.tabBar.barTintColor =[UIColor whiteColor];
    self.tabBarController.tabBar.tintColor = [UIColor colorWithRed:23.0/255 green:125.0/255 blue:46.0/255 alpha:1.0];
    [_title_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    UIImage *image = [[UIImage imageNamed:@"YellowBack"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.back setImage:image forState:UIControlStateNormal];
    self.back.tintColor = [UIColor whiteColor];
    [self.done setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    UIImage *image1 = [[UIImage imageNamed:@"lock"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.lock_1.image=image1;
    self.lock_1.tintColor = textColor;
    self.lock_2.image=image1;
    self.lock_2.tintColor = textColor;
    UIImage *image2 = [[UIImage imageNamed:@"2_locker"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.lock_3.image=image2;
    self.lock_3.tintColor = textColor;
    self.current_password.textColor=textColor_main;
    self.field_new_password.textColor=textColor_main;
    self.return_new_password.textColor=textColor_main;
    [self.current_password setValue:textColor_main forKeyPath:@"_placeholderLabel.textColor"];
    [self.field_new_password setValue:textColor_main forKeyPath:@"_placeholderLabel.textColor"];
    [self.return_new_password setValue:textColor_main forKeyPath:@"_placeholderLabel.textColor"];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)back:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)done:(id)sender {
}
@end
