//
//  ResetPassword.h
//  My Life
//
//  Created by Admin on 28.09.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResetPassword : UIViewController
@property (strong, nonatomic) IBOutlet UIButton *back;
- (IBAction)back:(id)sender;
- (IBAction)resetFacebook:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *resetFacebook;
@property (strong, nonatomic) IBOutlet UIButton *name_or_email;
- (IBAction)name_or_email:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *MyLife;
- (IBAction)MyLife:(id)sender;

@end
